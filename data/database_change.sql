
--  2019-10-06 
ALTER TABLE public.analyse_chimique ADD COLUMN station uuid;
--  2019-10-07
ALTER TABLE public.analyse_isotope ADD COLUMN station uuid;
ALTER TABLE public.analyse_isotope DROP COLUMN date_prelevement;
ALTER TABLE public.analyse_isotope DROP COLUMN date_analyse;
ALTER TABLE public.analyse_isotope ADD COLUMN date_prelevement character varying
(15);
ALTER TABLE public.analyse_isotope ADD COLUMN date_analyse character varying
(15);

ALTER TABLE public.analyse_chimique DROP COLUMN date_prelevement;
ALTER TABLE public.analyse_chimique DROP COLUMN date_analyse;
ALTER TABLE public.analyse_chimique ADD COLUMN date_prelevement character varying
(15);
ALTER TABLE public.analyse_chimique ADD COLUMN date_analyse character varying
(15);

ALTER TABLE public.analyse_pesticide DROP COLUMN date_prelevement;
ALTER TABLE public.analyse_pesticide DROP COLUMN date_analyse;
ALTER TABLE public.analyse_pesticide ADD COLUMN date_prelevement character varying
(15);
ALTER TABLE public.analyse_pesticide ADD COLUMN date_analyse character varying
(15);
ALTER TABLE public.analyse_pesticide ADD COLUMN station uuid;

ALTER TABLE public.analyse_bacteriologie DROP COLUMN date_prelevement;
ALTER TABLE public.analyse_bacteriologie DROP COLUMN date_analyse;
ALTER TABLE public.analyse_bacteriologie ADD COLUMN date_prelevement character varying
(15);
ALTER TABLE public.analyse_bacteriologie ADD COLUMN date_analyse character varying
(15);
ALTER TABLE public.analyse_bacteriologie ADD COLUMN station uuid;

-- 2019-10-19
ALTER TABLE public.analyse_chimique ADD COLUMN status character varying
(20);
ALTER TABLE public.analyse_bacteriologie ADD COLUMN status character varying
(20);
ALTER TABLE public.analyse_pesticide ADD COLUMN status character varying
(20);
ALTER TABLE public.analyse_isotope ADD COLUMN status character varying
(20);

-- 2019-11-11
ALTER TABLE public.utilisateur DROP COLUMN role;
ALTER TABLE public.utilisateur ADD COLUMN role smallint;
ALTER TABLE public.utilisateur ADD COLUMN niveau_entree smallint;
ALTER TABLE public.utilisateur ADD COLUMN region_id uuid;
ALTER TABLE public.utilisateur ADD COLUMN departement_id uuid;

ALTER TABLE public.pluviometrie ADD COLUMN status character varying
(20);
ALTER TABLE public.piezometrie ADD COLUMN status character varying
(20);
ALTER TABLE public.hydrometrie ADD COLUMN status character varying
(20);

-- 2019-11-12
ALTER TABLE public.carte DROP COLUMN path;
ALTER TABLE public.carte ADD COLUMN image bytea;

-- 2019-11-14
ALTER TABLE type_station ADD CONSTRAINT constraint_code UNIQUE (code);
ALTER TABLE type_ouvrage ADD CONSTRAINT constraint_code_ouvrage UNIQUE (code);
ALTER TABLE type_enregistrement ADD CONSTRAINT constraint_code_enregistrement UNIQUE (code);
ALTER TABLE type_analyse ADD CONSTRAINT constraint_code_typeanalyse UNIQUE (code);
ALTER TABLE station ADD CONSTRAINT constraint_code_station UNIQUE (code);
ALTER TABLE region ADD CONSTRAINT constraint_code_region UNIQUE (code);
ALTER TABLE departement ADD CONSTRAINT constraint_code_departement UNIQUE (code);
ALTER TABLE commune ADD CONSTRAINT constraint_code_commune UNIQUE (code);
ALTER TABLE nappe ADD CONSTRAINT constraint_code_nappe UNIQUE (code);
ALTER TABLE laboratoire ADD CONSTRAINT constraint_code_laboratoire UNIQUE (code);
ALTER TABLE element_chimique ADD CONSTRAINT constraint_code_chimique UNIQUE (code);
ALTER TABLE utilisateur ADD CONSTRAINT constraint_email_utilisateur UNIQUE (email);

ALTER TABLE public.analyse_chimique ADD COLUMN longitude numeric;
ALTER TABLE public.analyse_chimique ADD COLUMN latitude numeric;
ALTER TABLE public.analyse_chimique ADD COLUMN altitude numeric;
ALTER TABLE public.analyse_chimique ADD COLUMN nom_localite character varying
(250);

ALTER TABLE public.analyse_isotope ADD COLUMN longitude numeric;
ALTER TABLE public.analyse_isotope ADD COLUMN latitude numeric;
ALTER TABLE public.analyse_isotope ADD COLUMN altitude numeric;
ALTER TABLE public.analyse_isotope ADD COLUMN nom_localite character varying
(250);

ALTER TABLE public.analyse_bacteriologie ADD COLUMN longitude numeric;
ALTER TABLE public.analyse_bacteriologie ADD COLUMN latitude numeric;
ALTER TABLE public.analyse_bacteriologie ADD COLUMN altitude numeric;
ALTER TABLE public.analyse_bacteriologie ADD COLUMN nom_localite character varying
(250);

ALTER TABLE public.analyse_pesticide ADD COLUMN longitude numeric;
ALTER TABLE public.analyse_pesticide ADD COLUMN latitude numeric;
ALTER TABLE public.analyse_pesticide ADD COLUMN altitude numeric;
ALTER TABLE public.analyse_pesticide ADD COLUMN nom_localite character varying
(250);

ALTER TABLE public.analyse_bacteriologie DROP COLUMN heure_analyse;

ALTER TABLE public.analyse_chimique ADD COLUMN date_creation timestamp
(4) without time zone DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE public.analyse_pesticide ADD COLUMN date_creation timestamp
(4) without time zone DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE public.analyse_isotope ADD COLUMN date_creation timestamp
(4) without time zone DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE public.piezometrie ADD COLUMN date_creation timestamp
(4) without time zone DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE public.pluviometrie ADD COLUMN date_creation timestamp
(4) without time zone DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE public.hydrometrie ADD COLUMN date_creation timestamp
(4) without time zone DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE public.analyse_chimique ADD COLUMN ajouter_par uuid ;
ALTER TABLE public.analyse_pesticide ADD COLUMN ajouter_par uuid ;
ALTER TABLE public.analyse_isotope ADD COLUMN ajouter_par uuid ;
ALTER TABLE public.analyse_bacteriologie ADD COLUMN ajouter_par uuid ;
ALTER TABLE public.piezometrie ADD COLUMN ajouter_par uuid ;
ALTER TABLE public.pluviometrie ADD COLUMN ajouter_par uuid ;
ALTER TABLE public.hydrometrie ADD COLUMN ajouter_par uuid ;

ALTER TABLE public.analyse_chimique ADD COLUMN region_id uuid ;
ALTER TABLE public.analyse_pesticide ADD COLUMN region_id uuid ;
ALTER TABLE public.analyse_isotope ADD COLUMN region_id uuid ;
ALTER TABLE public.analyse_bacteriologie ADD COLUMN region_id uuid ;
ALTER TABLE public.piezometrie ADD COLUMN region_id uuid ;
ALTER TABLE public.pluviometrie ADD COLUMN region_id uuid ;
ALTER TABLE public.hydrometrie ADD COLUMN region_id uuid ;

ALTER TABLE public.hydrometrie ADD COLUMN hauteur_moyenne numeric ;
ALTER TABLE public.hydrometrie ADD COLUMN debit numeric ;
ALTER TABLE public.hydrometrie ADD COLUMN nom_ouvrage character varying
(250) ;

ALTER TABLE public.station ADD COLUMN status character varying
(50);
ALTER TABLE public.station ADD COLUMN observateur character varying
(250);
ALTER TABLE public.station ADD COLUMN courbe_etalonnage boolean;

ALTER TABLE public.nappe ADD COLUMN type_nappe character varying
(25) ;


ALTER TABLE public.piezometrie ADD COLUMN niveau_dynamique_sol boolean;

-- 2019-11-18
ALTER TABLE public.analyse_chimique ADD COLUMN modifier_par uuid ;
ALTER TABLE public.analyse_pesticide ADD COLUMN modifier_par uuid ;
ALTER TABLE public.analyse_isotope ADD COLUMN modifier_par uuid ;
ALTER TABLE public.analyse_bacteriologie ADD COLUMN modifier_par uuid ;
ALTER TABLE public.piezometrie ADD COLUMN modifier_par uuid ;
ALTER TABLE public.pluviometrie ADD COLUMN modifier_par uuid ;
ALTER TABLE public.hydrometrie ADD COLUMN modifier_par uuid ;

ALTER TABLE public.analyse_chimique ADD COLUMN modifier_le timestamp
(4) without time zone ;
ALTER TABLE public.analyse_pesticide ADD COLUMN modifier_le timestamp
(4) without time zone ;
ALTER TABLE public.analyse_isotope ADD COLUMN modifier_le timestamp
(4) without time zone ;
ALTER TABLE public.analyse_bacteriologie ADD COLUMN modifier_le timestamp
(4) without time zone ;
ALTER TABLE public.piezometrie ADD COLUMN modifier_le timestamp
(4) without time zone ;
ALTER TABLE public.pluviometrie ADD COLUMN modifier_le timestamp
(4) without time zone ;
ALTER TABLE public.hydrometrie ADD COLUMN modifier_le timestamp
(4) without time zone ;

ALTER TABLE public.nappe ADD COLUMN profondeur numeric;

-- 2019-11-19
ALTER TABLE public.enquete ADD COLUMN titre character varying
(200) ;
ALTER TABLE public.enquete ADD COLUMN type_enquete character varying
(50) ;
ALTER TABLE public.analyse_chimique ADD COLUMN enquete_id uuid ;
ALTER TABLE public.analyse_pesticide ADD COLUMN enquete_id uuid ;
ALTER TABLE public.analyse_isotope ADD COLUMN enquete_id uuid ;
ALTER TABLE public.analyse_bacteriologie ADD COLUMN enquete_id uuid ;
ALTER TABLE public.piezometrie ADD COLUMN enquete_id uuid ;
ALTER TABLE public.pluviometrie ADD COLUMN enquete_id uuid ;
ALTER TABLE public.hydrometrie ADD COLUMN enquete_id uuid ;

-- 2019-11-20
ALTER TABLE public.coupe_lithologique DROP COLUMN code_lithologique;
ALTER TABLE public.coupe_lithologique ADD COLUMN code_lithologique_id uuid ;

ALTER TABLE public.piezometrie ADD COLUMN pression numeric;
ALTER TABLE public.piezometrie ADD COLUMN pulses numeric DEFAULT 0;

-- 2019-11-23
ALTER TABLE public.hydrometrie ADD COLUMN charge numeric;
ALTER TABLE public.hydrometrie ADD COLUMN voltage numeric;

-- 2019-11-24
ALTER TABLE public.code_lithologique RENAME COLUMN libelle TO titre;

-- 2019-11-26
ALTER TABLE public.element_chimique ADD COLUMN min_norme numeric DEFAULT 0;
ALTER TABLE public.element_chimique ADD COLUMN max_norme numeric;

-- 2019-12-04
ALTER TABLE public.analyse_chimique ADD COLUMN departement_id uuid ;
ALTER TABLE public.analyse_pesticide ADD COLUMN departement_id uuid ;
ALTER TABLE public.analyse_isotope ADD COLUMN departement_id uuid ;
ALTER TABLE public.analyse_bacteriologie ADD COLUMN departement_id uuid ;
ALTER TABLE public.piezometrie ADD COLUMN departement_id uuid ;
ALTER TABLE public.pluviometrie ADD COLUMN departement_id uuid ;
ALTER TABLE public.hydrometrie ADD COLUMN departement_id uuid ;

-- 2020-01-03
ALTER TABLE public.element_chimique ADD COLUMN unite character varying(25) ;