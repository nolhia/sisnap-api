const path = require('path');

module.exports = {
    openapi: '3.0.0',
    info: {
        // API informations (required)
        title: 'SISNAP - API', // Title (required)
        version: '1.0.0', // Version (required)
        description: 'SISNAP API', // Description (optional)
    },
    servers: [
        { url: 'http://localhost:4002' }
    ],
    apis: [path.join(__dirname, './src/**/**/*.ts')]
};
