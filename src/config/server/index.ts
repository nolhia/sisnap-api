import * as http from 'http';
import * as serverHandlers from './serverHandlers';
import server from './server';

const packageJson:any = require(`../../../package.json`);
const Server: http.Server = http.createServer(server);

/**
 * Binds and listens for connections on the specified host
 */
Server.listen(server.get('port'));

/**
 * Server Events
 */
Server.on('error',
    (error: Error) => serverHandlers.onError(error, server.get('port')));
Server.on('listening',
    serverHandlers.onListening.bind(Server));

console.log(`\n==========================================`);
console.log(`~~~ ${packageJson.description || packageJson.name} ~~~\n`);
console.log(`Server port : ${server.get('port')}`);
console.log(`Environment type : ${process.env.ENVIRONMENT}`);
console.log(`==========================================\n`);
