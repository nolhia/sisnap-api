import * as express from 'express'; 
import * as Middleware from '../middleware/middleware';
import * as Routes from '../../routes'; 

const expressWinston = require('express-winston');
const winston = require('winston'); 
 
/**
 * @constant {express.Application}
 */
const app: express.Application = express();

app.use(expressWinston.logger({
    transports: [
        new winston.transports.File({
            filename: 'logs/combined.log',
            level: 'info',
            handleExceptions: true,
            json: true,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false
        }),
        new winston.transports.File({
            filename: 'logs/errors.log',
            level: 'error',
            handleExceptions: true,
            json: true,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false
        }),
        new winston.transports.Console({
            level: 'error',
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ], 
    exceptionHandlers: [
        new winston.transports.File({ filename: 'logs/exceptions.log' })
    ]
}));

/** 
 * @constructs express.Application Middleware
 */
Middleware.configure(app);

/**
 * @constructs express.Application Routes
 */
Routes.init(app);

// Error logger
/*app.use(expressWinston.errorLogger({
    transports: [
      new winston.transports.Console({
        json: true,
        colorize: true
      })
    ]
  }));
*/
/**
 * @constructs express.Application Error Handler
 */
Middleware.initErrorHandler(app);

/**
 * sets port 4000 to default or unless otherwise specified in the environment
 */
app.set('port', process.env.PORT || 4001);

/**
 * sets secret to 'superSecret', otherwise specified in the environment
 */
app.set('secret', process.env.SECRET || 'superSecret');

/**
 * @exports {express.Application}
 */
export default app;
