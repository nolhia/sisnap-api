class Connection {
    public development: any = {
        host: '127.0.0.1',
        port: 5432,
        dbname: 'sisnap',
        username: 'postgres',
        password: ''
    };

    public test: any = {
        host: '127.0.0.1',
        port: 5432,
        dbname: 'sisnap1',
        username: 'postgres',
        password: ''
    };

    public production: any = {
        host: '167.114.2.200',
        port: 5432,
        dbname: 'sisnap',
        username: 'postgres',
        password: 'gestion'
    };

}

export default new Connection;
