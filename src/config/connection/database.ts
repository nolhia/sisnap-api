
import * as knex from 'knex';
import * as dotenv from 'dotenv';
import Connection from '../env/connection';
dotenv.config();
const environment: string = process.env.ENVIRONMENT;
let con: any = Connection.development;

if (environment === 'test') {
    con = Connection.test;
} else if (environment === 'production') {
    con = Connection.production;
}
export default knex({
    client: 'pg',
    connection: `postgres://${con.username}:${con.password}@${con.host}:${con.port}/${con.dbname}`,
    searchPath: ['public', 'private'],
    pool: { min: 2, max: 10 },
    debug: false
});
