const xlstojson = require("xls-to-json-lc");
const xlsxtojson = require("xlsx-to-json-lc"); 

class UtilsHelper { 
 
    passwordResetMessages() {
        return {
            titre: "Réinitialisation de mot de passe",
            body_mail: "Cher/Chère %s <br> Avez-vous oublié votre mot de passe? <br> <a href='%s'> Réinitialiser le mot de passe </a> <br> Ce lien expire dans 24 heures et ne peut être utilisé qu'une seule fois.<br> Si vous ne souhaitez pas modifier votre mot de passe ou n'êtes pas à l'origine de cette demande, ignorez ce message et supprimer-le. <br><br> Merci,<br> L'équipe de gestion des comptes Nekamoto",
            body_sms: "Cher/Chère %s, utilisez ce lien pour réinitialiser votre compte <a href='%s'> Réinitialiser le mot de passe </a>. Ce lien expire dans 24 heures. Si vous ne souhaitez pas modifier votre mot de passe ou n'êtes pas à l'origine de cette demande, ignorez ce message et supprimer-le."
        }
    }

    async convertExcelToJson (req){
 
        /** Multer gives us file info in req.file object */
        if(!req.file){ 
            return {error_code:1,err_desc:"Le fichier est requis."};
        }
        //start convert process
        /** Check the extension of the incoming file and
         *  use the appropriate module
         */
        if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx'){
           return xlsxtojson;
        }  
        return xlstojson;
         
    }

     
}

export default new UtilsHelper();