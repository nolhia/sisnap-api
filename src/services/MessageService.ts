import * as nodemailer from 'nodemailer';
import config from '../config/connection/message';
 
class MessageService {
    constructor(
        public from?: string,
        public toEmail?: string,
        public toSms?: string,
        public subject?: string,
        public body?: string
    ) { } 

    async sendMail(): Promise<any> {
        try {
            let transport = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: config.mail.username,
                    pass: config.mail.password
                }
            }); 
            const message = {
                from: config.mail.username,
                to: this.toEmail,
                subject: this.subject,
                html: this.body
            };

            let info = await transport.sendMail(message);
            console.log("Message sent: %s", info.messageId);

        } catch (error) {
            throw new Error(error.message);
        }
    }
}

export default new MessageService;
