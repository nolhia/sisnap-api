import knex from '../../config/connection/database';

/**
 * @export
 * @interface IHydrometrieModel
 */
export interface IHydrometrieModel {
    id?: number;
}

const tableName: string = 'hydrometrie';

class HydrometrieModel {
    async create(objet: IHydrometrieModel): Promise<IHydrometrieModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IHydrometrieModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[IHydrometrieModel]> {
        return await knex.select('hydrometrie.*', 'station.titre as station').from(tableName)
            .leftJoin('station', 'hydrometrie.station_suivi_id', 'station.id');
    }

    async export(): Promise<[IHydrometrieModel]> {
        return await knex.select(
            'hydrometrie.date_releve', 'hydrometrie.heure_releve', 'hydrometrie.hauteur_matin', 'hydrometrie.hauteur_soir',
            'hydrometrie.hauteur_moyenne', 'hydrometrie.debit', 'hydrometrie.nom_ouvrage as nom ouvrage',
            'hydrometrie.charge', 'hydrometrie.voltage', 'hydrometrie.status', 'hydrometrie.note',
            'station.titre as station').from(tableName)
            .leftJoin('station', 'hydrometrie.station_suivi_id', 'station.id')
            .leftJoin('region', 'hydrometrie.region_id', 'region.id')
            .leftJoin('enquete', 'hydrometrie.enquete_id', 'enquete.id');
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id).del();
    }

    async update(Hydrometrie: IHydrometrieModel): Promise<IHydrometrieModel> {
        return await knex(tableName).where({ id: Hydrometrie.id }).update(Hydrometrie);
    }

    async upload(data: any): Promise<any> {
        return await knex(tableName).insert(data);
    }
}

export default new HydrometrieModel();
