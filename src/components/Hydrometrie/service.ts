
import HydrometrieModel, { IHydrometrieModel } from './model';
import { IHydrometrieService } from './interface';

/**
 * @export
 * @implements {IHydrometrieService}
 */
const HydrometrieService: IHydrometrieService = {

    /**
     * @param {IHydrometrieModel} Hydrometrie
     * @returns {Promise < IHydrometrieModel >}
     * @memberof HydrometrieService
     */
    async insert(body: IHydrometrieModel): Promise<IHydrometrieModel> {
        try {
            const Hydrometrie: IHydrometrieModel = await HydrometrieModel.create(body);

            return Hydrometrie;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IHydrometrieModel} Hydrometrie
     * @returns {Promise < IHydrometrieModel >}
     * @memberof HydrometrieService
     */
    async update(body: IHydrometrieModel): Promise<IHydrometrieModel> {
        try {
            const Hydrometrie: IHydrometrieModel = await HydrometrieModel.update(body);

            return Hydrometrie;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IHydrometrieModel >}
     * @memberof HydrometrieService
     */
    async findOne(id: string): Promise<IHydrometrieModel> {
        try {
            return await HydrometrieModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < IHydrometrieModel[] >}
     * @memberof HydrometrieService
     */
    async findAll(): Promise<IHydrometrieModel[]> {
        try {
            return await HydrometrieModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IHydrometrieModel >}
     * @memberof HydrometrieService
     */
    async remove(id: string): Promise<IHydrometrieModel> {
        try {
            return await HydrometrieModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {any} data
     * @returns {Promise < IHydrometrieModel >}
     * @memberof HydrometrieService
     */
    async upload(data: any): Promise<IHydrometrieModel> {
        try {
            return await HydrometrieModel.upload(data);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
    * @returns {Promise < any[] >}
    * @memberof HydrometrieService
    */
    async export(): Promise<any[]> {
        try {
            const data: any[] = await HydrometrieModel.export();

            return data;

        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default HydrometrieService;
