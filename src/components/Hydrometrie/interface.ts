import { IHydrometrieModel } from './model';

/**
 * @export
 * @interface IHydrometrieService
 */
export interface IHydrometrieService {

    /**
     * @param {IHydrometrieModel} IHydrometrieModel
     * @returns {Promise<IHydrometrieModel>}
     * @memberof IHydrometrieService
     */
    insert(IHydrometrieModel: IHydrometrieModel): Promise<IHydrometrieModel>;

    /**
     * @param {IHydrometrieModel} IHydrometrieModel
     * @returns {Promise<IHydrometrieModel>}
     * @memberof IHydrometrieService
     */
    update(IHydrometrieModel: IHydrometrieModel): Promise<IHydrometrieModel>;

    /**
     * @param {string} id
     * @returns {Promise<IHydrometrieModel>}
     * @memberof IHydrometrieService
     */
    findOne(id: string): Promise<IHydrometrieModel>;

    /**
     * @returns {Promise<IHydrometrieModel[]>}
     * @memberof IHydrometrieService
     */
    findAll(): Promise<IHydrometrieModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<IHydrometrieModel>}
     * @memberof IHydrometrieService
     */
    remove(id: string): Promise<any>;

    /**
     * @param {any} data
     * @returns {Promise<any>}
     * @memberof IHydrometrieService
     */
    upload(data: any): Promise<any>;

    /**
     * @returns {Promise<any[]>}
     * @memberof IHydrometrieService
     */
    export(): Promise<any[]>;

}
