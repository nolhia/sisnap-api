import { IAnalysepesticideModel } from './model';

/**
 * @export
 * @interface IAnalysepesticideService
 */
export interface IAnalysepesticideService {

    /**
     * @param {IAnalysepesticideModel} IAnalysepesticideModel
     * @returns {Promise<IAnalysepesticideModel>}
     * @memberof IAnalysepesticideService
     */
    insert(IAnalysepesticideModel: IAnalysepesticideModel): Promise<IAnalysepesticideModel>;

    /**
     * @param {IAnalysepesticideModel} IAnalysepesticideModel
     * @returns {Promise<IAnalysepesticideModel>}
     * @memberof IAnalysepesticideService
     */
    update(IAnalysepesticideModel: IAnalysepesticideModel): Promise<IAnalysepesticideModel>;

    /**
     * @param {string} id
     * @returns {Promise<IAnalysepesticideModel>}
     * @memberof IAnalysepesticideService
     */
    findOne(id: string): Promise<IAnalysepesticideModel>;

    /**
     * @returns {Promise<IAnalysepesticideModel[]>}
     * @memberof IAnalysepesticideService
     */
    findAll(): Promise<IAnalysepesticideModel[]>;

    /**
     * @returns {Promise<any[]>}
     * @memberof IAnalysepesticideService
     */
    export(): Promise<any[]>;

    /**
     * @param {string} id
     * @returns {Promise<IAnalysepesticideModel>}
     * @memberof IAnalysepesticideService
     */
    remove(id: string): Promise<any>;

    /**
     * @param {any} data
     * @returns {Promise<any>}
     * @memberof IAnalysepesticideService
     */
    upload(data: any): Promise<any>;

}
