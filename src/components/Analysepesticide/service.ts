import * as Joi from 'joi';
import AnalysepesticideModel, { IAnalysepesticideModel } from './model';
import AnalysepesticideValidation from './validation';
import { IAnalysepesticideService } from './interface';

/**
 * @export
 * @implements {IAnalysepesticideService}
 */
const AnalysepesticideService: IAnalysepesticideService = {
    /**
     * @param {IAnalysepesticideModel} Analysepesticide
     * @returns {Promise < IAnalysepesticideModel >}
     * @memberof AnalysepesticideService
     */
    async insert(body: IAnalysepesticideModel): Promise<IAnalysepesticideModel> {
        try {
            const validate: Joi.ValidationResult<IAnalysepesticideModel> = AnalysepesticideValidation.createAnalysepesticide(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const Analysepesticide: IAnalysepesticideModel = await AnalysepesticideModel.create(body);

            return Analysepesticide;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IAnalysepesticideModel} Analysepesticide
     * @returns {Promise < IAnalysepesticideModel >}
     * @memberof AnalysepesticideService
     */
    async update(body: IAnalysepesticideModel): Promise<IAnalysepesticideModel> {
        try {
            const validate: Joi.ValidationResult<IAnalysepesticideModel> = AnalysepesticideValidation.updateAnalysepesticide(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const Analysepesticide: IAnalysepesticideModel = await AnalysepesticideModel.update(body);

            return Analysepesticide;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IAnalysepesticideModel >}
     * @memberof AnalysepesticideService
     */
    async findOne(id: string): Promise<IAnalysepesticideModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = AnalysepesticideValidation.getAnalysepesticide({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await AnalysepesticideModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < IAnalysepesticideModel[] >}
     * @memberof AnalysepesticideService
     */
    async findAll(): Promise<IAnalysepesticideModel[]> {
        try {
            return await AnalysepesticideModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < any[] >}
     * @memberof AnalysechimiqueService
     */
    async export(): Promise<any[]> {
        try {
            const data: any[] = await AnalysepesticideModel.export();
            data.map((element) => {
                element.element_chimique = JSON.stringify(element.element_chimique[0])
                    .replace('{', '')
                    .replace('}', '')
                    .replace(/,/g, ' | ');
            });

            return data;

        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IAnalysepesticideModel >}
     * @memberof AnalysepesticideService
     */
    async remove(id: string): Promise<IAnalysepesticideModel> {
        try {
            return await AnalysepesticideModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {any} data
     * @returns {Promise < IAnalysepesticideModel >}
     * @memberof AnalysepesticideService
     */
    async upload(data: any): Promise<IAnalysepesticideModel> {
        try {
            return await AnalysepesticideModel.upload(data);
        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default AnalysepesticideService;
