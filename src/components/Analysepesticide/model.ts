import knex from '../../config/connection/database';

/**
 * @export
 * @interface IAnalysepesticideModel
 */
export interface IAnalysepesticideModel {
    id?: number;
}

const tableName: string = 'analyse_pesticide';

class AnalysepesticideModel {
    async create(objet: IAnalysepesticideModel): Promise<IAnalysepesticideModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IAnalysepesticideModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[IAnalysepesticideModel]> {
        return await knex.select('analyse_pesticide.*', 'laboratoire.titre as laboratoire_titre')
            .from(tableName)
            .leftJoin('laboratoire', 'analyse_pesticide.laboratoire', 'laboratoire.id');
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id).del();
    }

    async update(Analysepesticide: IAnalysepesticideModel): Promise<IAnalysepesticideModel> {
        return await knex(tableName).where({ id: Analysepesticide.id }).update(Analysepesticide);
    }

    async upload(data: any): Promise<any> {
        return await knex(tableName).insert(data);
    }

    async export(): Promise<[any]> {
        return await knex(tableName)
            .join('station', 'station.id', '=', tableName + '.station')
            .join('laboratoire', 'laboratoire.id', '=', tableName + '.laboratoire')
            .select(
                tableName + '.date_prelevement',
                tableName + '.heure_prelevement',
                tableName + '.date_analyse',
                tableName + '.heure_analyse',
                tableName + '.chimiste',
                tableName + '.element_chimique',
                tableName + '.note',
                'laboratoire.code as laboratoire',
                'station.code as station',
                tableName + '.longitude',
                tableName + '.latitude',
                tableName + '.altitude',
                tableName + '.nom_localite as nom localite',
                tableName + '.status',
            );
    }
}

export default new AnalysepesticideModel();
