import * as Joi from 'joi';
import Validation from '../validation';
import { IAnalysepesticideModel } from './model';

/**
 * Validation des données des analyses pesticides avant enregistrement dans la BD
 * @export
 * @class AnalysepesticideValidation
 * @extends Validation
 */
class AnalysepesticideValidation extends Validation {

    /**
     * Creates an instance of AnalysepesticideValidation.
     * @memberof AnalysepesticideValidation
     */
    constructor() {
        super();
    }

    /**
 * @param {IAnalysepesticideModel} params
 * @returns {Joi.ValidationResult<IAnalysepesticideModel >}
 * @memberof AnalysepesticideValidation
 */
    createAnalysepesticide(
        params: IAnalysepesticideModel
    ): Joi.ValidationResult<IAnalysepesticideModel> {
        const schema: Joi.Schema = Joi.object().keys({
            date_prelevement: Joi.string().allow(null).optional(),
            heure_prelevement: Joi.string().allow(null).optional(),
            date_analyse: Joi.string().required(),
            heure_analyse: Joi.string().allow(null).optional(),
            element_chimique: Joi.any().allow('').optional(),
            note: Joi.string().allow('').optional(),
            laboratoire: Joi.string().required(),
            chimiste: Joi.string().allow('').optional(),
            station: Joi.string().allow('').optional(),
            status: Joi.string().allow('').optional(),
            longitude: Joi.number().allow(null).optional(),
            latitude: Joi.number().allow(null).optional(),
            altitude: Joi.number().allow(null).optional(),
            nom_localite: Joi.string().allow('').optional(),
            ajouter_par: Joi.string().optional(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {IAnalysepesticideModel} params
     * @returns {Joi.ValidationResult<IAnalysepesticideModel >}
     * @memberof AnalysepesticideValidation
     */
    updateAnalysepesticide(
        params: IAnalysepesticideModel
    ): Joi.ValidationResult<IAnalysepesticideModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string().required(),
            date_prelevement: Joi.string().allow(null).optional(),
            heure_prelevement: Joi.string().allow(null).optional(),
            date_analyse: Joi.string().required(),
            heure_analyse: Joi.string().allow(null).optional(),
            element_chimique: Joi.any().allow('').optional(),
            note: Joi.string().allow('').optional(),
            laboratoire: Joi.string().required(),
            chimiste: Joi.string().allow('').optional(),
            station: Joi.string().allow('').optional(),
            status: Joi.string().allow('').optional(),
            longitude: Joi.number().allow(null).optional(),
            latitude: Joi.number().allow(null).optional(),
            altitude: Joi.number().allow(null).optional(),
            nom_localite: Joi.string().allow('').optional(),
            modifier_par: Joi.string().optional(),
            modifier_le: Joi.string().optional(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof AnalysepesticideValidation
     */
    getAnalysepesticide(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof AnalysepesticideValidation
     */
    removeAnalysepesticide(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

}

export default new AnalysepesticideValidation();
