import * as Joi from 'joi';
import Validation from '../validation';
import { IElementchimiqueModel } from './model';

/**
 * Validation des données des parametres d'analyses (elements chimiques) avant enregistrement dans la BD
 * @export
 * @class ElementchimiqueValidation
 * @extends Validation
 */
class ElementchimiqueValidation extends Validation {

    /**
     * Creates an instance of ElementchimiqueValidation.
     * @memberof ElementchimiqueValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {IElementchimiqueModel} params
     * @returns {Joi.ValidationResult<IElementchimiqueModel >}
     * @memberof ElementchimiqueValidation
     */
    createElementchimique(
        params: IElementchimiqueModel
    ): Joi.ValidationResult<IElementchimiqueModel> {
        const schema: Joi.Schema = Joi.object().keys({
            code: Joi.string().required(),
            titre: Joi.string(),
            analyse_chimique: Joi.string(),
            bacteriologie: Joi.string(),
            isotopie: Joi.string(),
            pesticide: Joi.string(),
            min_norme: Joi.number().allow(null).optional(),
            max_norme: Joi.number().allow(null).optional()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {IElementchimiqueModel} params
     * @returns {Joi.ValidationResult<IElementchimiqueModel >}
     * @memberof ElementchimiqueValidation
     */
    updateElementchimique(
        params: IElementchimiqueModel
    ): Joi.ValidationResult<IElementchimiqueModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string(),
            code: Joi.string(),
            titre: Joi.string(),
            analyse_chimique: Joi.any(),
            bacteriologie: Joi.any(),
            isotopie: Joi.any(),
            pesticide: Joi.any(),
            type_analyse: Joi.optional(),
            min_norme: Joi.number().allow(null).optional(),
            max_norme: Joi.number().allow(null).optional()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof ElementchimiqueValidation
     */
    getElementchimique(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ code: string }} body
     * @returns {Joi.ValidationResult<{ code: string }>}
     * @memberof ElementchimiqueValidation
     */
    getElementchimiqueByCode(
        body: {
            code: string
        }
    ): Joi.ValidationResult<{
        code: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            code: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ analyseType: string }} body
     * @returns {Joi.ValidationResult<{ analyseType: string }>}
     * @memberof ElementchimiqueValidation
     */
    getElementchimiqueByAnalyseType(
        body: {
            analyseType: string
        }
    ): Joi.ValidationResult<{
        analyseType: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            analyseType: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof ElementchimiqueValidation
     */
    removeElementchimique(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

}

export default new ElementchimiqueValidation();
