import { IElementchimiqueModel } from './model';

/**
 * @export
 * @interface IElementchimiqueService
 */
export interface IElementchimiqueService {

    /**
     * @param {IElementchimiqueModel} IElementchimiqueModel
     * @returns {Promise<IElementchimiqueModel>}
     * @memberof IElementchimiqueService
     */
    insert(IElementchimiqueModel: IElementchimiqueModel): Promise<IElementchimiqueModel>;

    /**
     * @param {IElementchimiqueModel} IElementchimiqueModel
     * @returns {Promise<IElementchimiqueModel>}
     * @memberof IElementchimiqueService
     */
    update(IElementchimiqueModel: IElementchimiqueModel): Promise<IElementchimiqueModel>;

    /**
     * @param {string} id
     * @returns {Promise<IElementchimiqueModel>}
     * @memberof IElementchimiqueService
     */
    findOne(id: string): Promise<IElementchimiqueModel>;

    /**
     * @param {string} code
     * @returns {Promise<IElementchimiqueModel>}
     * @memberof IElementchimiqueService
     */
    findOneByCode(code: string): Promise<IElementchimiqueModel>;

    /**
     * @param {string} analyseType
     * @returns {Promise<IElementchimiqueModel[]>}
     * @memberof IElementchimiqueService
     */
    findByAnalyse(analyse: string): Promise<IElementchimiqueModel[]>;

    /**
     * @returns {Promise<IElementchimiqueModel[]>}
     * @memberof IElementchimiqueService
     */
    findAll(): Promise<IElementchimiqueModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<IElementchimiqueModel>}
     * @memberof IElementchimiqueService
     */
    remove(id: string): Promise<any>;

}
