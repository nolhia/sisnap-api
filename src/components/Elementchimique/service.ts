import * as Joi from 'joi';
import ElementchimiqueModel, { IElementchimiqueModel } from './model';
import ElementchimiqueValidation from './validation';
import { IElementchimiqueService } from './interface';

/**
 * @export
 * @implements {IElementchimiqueService}
 */
const ElementchimiqueService: IElementchimiqueService = {

    /**
     * @param {IElementchimiqueModel} Elementchimique
     * @returns {Promise < IElementchimiqueModel >}
     * @memberof ElementchimiqueService
     */
    async insert(body: IElementchimiqueModel): Promise<IElementchimiqueModel> {
        try { 
            const Elementchimique: IElementchimiqueModel = await ElementchimiqueModel.create(body);

            return Elementchimique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IElementchimiqueModel} Elementchimique
     * @returns {Promise < IElementchimiqueModel >}
     * @memberof ElementchimiqueService
     */
    async update(body: IElementchimiqueModel): Promise<IElementchimiqueModel> {
        try { 
            const Elementchimique: IElementchimiqueModel = await ElementchimiqueModel.update(body);

            return Elementchimique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IElementchimiqueModel >}
     * @memberof ElementchimiqueService
     */
    async findOne(id: string): Promise<IElementchimiqueModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = ElementchimiqueValidation.getElementchimique({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await ElementchimiqueModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} code
     * @returns {Promise < IElementchimiqueModel >}
     * @memberof ElementchimiqueService
     */
    async findOneByCode(code: string): Promise<IElementchimiqueModel> {
        try {
            const validate: Joi.ValidationResult<{
                code: string
            }> = ElementchimiqueValidation.getElementchimiqueByCode({
                code
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await ElementchimiqueModel.findOne({
                code: code
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < IElementchimiqueModel[] >}
     * @memberof ElementchimiqueService
     */
    async findAll(): Promise<IElementchimiqueModel[]> {
        try {
            return await ElementchimiqueModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IElementchimiqueModel >}
     * @memberof ElementchimiqueService
     */
    async remove(id: string): Promise<IElementchimiqueModel> {
        try {
            return await ElementchimiqueModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} analyseType
     * @returns {Promise < IElementchimiqueModel[] >}
     * @memberof ElementchimiqueService
     */
    async findByAnalyse(analyseType: string): Promise<IElementchimiqueModel[]> {
        try {
            const validate: Joi.ValidationResult<{
                analyseType: string
            }> = ElementchimiqueValidation.getElementchimiqueByAnalyseType({
                analyseType
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await ElementchimiqueModel.findByAnalyse(analyseType);

        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default ElementchimiqueService;
