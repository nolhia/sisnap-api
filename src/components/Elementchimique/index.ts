import ElementchimiqueService from './service';
import { HttpError } from '../../config/error';
import { IElementchimiqueModel } from './model';
import { NextFunction, Request, Response } from 'express';

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Elementchimique: IElementchimiqueModel = await ElementchimiqueService.insert(req.body);

        res.status(201).json(Elementchimique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Elementchimique: IElementchimiqueModel = await ElementchimiqueService.update(req.body);

        res.status(200).json(Elementchimique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Elementchimique: IElementchimiqueModel = await ElementchimiqueService.findOne(req.params.id);

        res.status(200).json(Elementchimique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Elementchimiques: IElementchimiqueModel[] = await ElementchimiqueService.findAll();

        res.status(200).json(Elementchimiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Elementchimique: IElementchimiqueModel = await ElementchimiqueService.remove(req.params.id);

        res.status(200).json(Elementchimique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findByAnalyse(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Elementchimiques: IElementchimiqueModel[] = await ElementchimiqueService.findByAnalyse(req.params.analyseType);

        res.status(200).json(Elementchimiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
