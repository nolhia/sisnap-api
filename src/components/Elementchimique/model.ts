import knex from '../../config/connection/database';

/**
 * @export
 * @interface IElementchimiqueModel
 */
export interface IElementchimiqueModel {
    id?: number;
}

const tableName: string = 'element_chimique';

class ElementchimiqueModel {
    async create(objet: IElementchimiqueModel): Promise<IElementchimiqueModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IElementchimiqueModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findByAnalyse(analyseType: String): Promise<[IElementchimiqueModel]> {

        if (analyseType === 'analyse_chimique') {
            return await knex.select().from(tableName).where('analyse_chimique', true);
        } else if (analyseType === 'bacteriologie') {
            return await knex.select().from(tableName).where('bacteriologie', true);
        } else if (analyseType === 'isotopie') {
            return await knex.select().from(tableName).where('isotopie', true);
        } else if (analyseType === 'pesticide') {
            return await knex.select().from(tableName).where('pesticide', true);
        } else {
            return await knex.select().from(tableName).where('analyse_chimique', true)
                .andWhere('bacteriologie', true)
                .andWhere('isotopie', true)
                .andWhere('pesticide', true);
        }
    }

    async findAll(): Promise<[IElementchimiqueModel]> {
        return await knex.select().from(tableName).orderBy('code');
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id).del();
    }

    async update(Elementchimique: IElementchimiqueModel): Promise<IElementchimiqueModel> {
        return await knex(tableName).where({ id: Elementchimique.id }).update(Elementchimique);
    }
}

export default new ElementchimiqueModel();
