import AnalysechimiqueService from './service';
import LaboratoireService from '../Laboratoire/service';
import StationService from '../Station/service';
import ElementchimiqueService from '../Elementchimique/service';
import { HttpError } from '../../config/error';
import { IAnalysechimiqueModel } from './model';
import { NextFunction, Request, Response } from 'express';
import UtilsHelper from '../../helpers/UtilsHelper';
const fs: any = require('fs');

/**
 * Créer une nouvelle analyse chimique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const analysechimique: IAnalysechimiqueModel = await AnalysechimiqueService.insert(req.body);

        res.status(201).json(analysechimique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Mettre à jour une analyse chimique existante
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const analysechimique: IAnalysechimiqueModel = await AnalysechimiqueService.update(req.body);

        res.status(200).json(analysechimique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Trouver une analyse chimique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Analysechimique: IAnalysechimiqueModel = await AnalysechimiqueService.findOne(req.params.id);

        res.status(200).json(Analysechimique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Trouver une analyse chimique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function report(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Analysechimique: IAnalysechimiqueModel = await AnalysechimiqueService.report(req.params.id);

        res.status(200).json(Analysechimique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer la liste de toutes les analyses chimiques
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const analysechimiques: IAnalysechimiqueModel[] = await AnalysechimiqueService.findAll();

        res.status(200).json(analysechimiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Supprimer une analyse chimique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const analysechimique: IAnalysechimiqueModel = await AnalysechimiqueService.remove(req.params.id);

        res.status(200).json(analysechimique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Traiter un fichier csv contenant une liste d'analyses chimiques à insérer dans la BD
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function upload(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const exceltojson: any = await UtilsHelper.convertExcelToJson(req);

        await exceltojson(
            {
                input: req.file.path,
                output: null,
                lowerCaseHeaders: true
            },
            async (err: any, result: any) => {
                if (err) {
                    res.status(400).json({ error_code: 1, err_desc: err, data: null });
                }
                // supprimer le fichier 
                fs.unlinkSync(req.file.path);

                // pour ligne de données on verifie si le labortoire ou la station existe
                // 1 - Si oui on recupère leur id
                // 2 - Si non on les insères dans leur table respective et on récupère l'id de l'enregistrement
                try {
                    const promisedAnalysechimiques: any = result.map(async (element: any) => {
                        const laboratoireCode: any = element.laboratoire;
                        const stationCode: any = element.station;
                        element.status = 'pending';

                        try {
                            // Traitement des laboratoires
                            const laboratoire: any = await LaboratoireService.findOneByCode(laboratoireCode);

                            if (laboratoire && laboratoire.id) {
                                element.laboratoire = laboratoire.id;
                            } else {
                                console.log('le Laboratoire ' + laboratoireCode + ' n\'existe pas pour l\'instant, il sera crée');
                                const laboratoireData: any = {
                                    'code': laboratoireCode,
                                    'titre': laboratoireCode,
                                };
                                const nouveauLaboratoire: any = await LaboratoireService.insert(laboratoireData);

                                if (nouveauLaboratoire && nouveauLaboratoire.id) {
                                    element.laboratoire = nouveauLaboratoire.id;
                                }

                            }
                            // Traitement des stations
                            const station: any = await StationService.findOneByCode(stationCode);
                            if (station && station.id) {
                                element.station = station.id;
                            } else {
                                console.log('la station ' + stationCode + ' n\'existe pas pour l\'instant, elle sera créee');
                                const stationData: any = {
                                    'code': stationCode,
                                    'titre': stationCode,
                                    'latitude': 1,
                                    'longitude': 1,
                                    'altitude': 1,
                                    'type_station_id': "bf269a4e-1950-4c2e-ac1d-a80f51b39d5e",
                                    'type_ouvrage_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                    'nappe_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                };
                                const nouvelleStation: any = await StationService.insert(stationData);
                                if (nouvelleStation && nouvelleStation.id) {
                                    element.station = nouvelleStation.id;
                                }

                            }
                            // Traitement des elements chimiques
                            // 1 - Nous allons d'abord vérifier si chaque élément chimique existe. sil y en a qui n'existe pas nous les créons
                            const elementChimiqueJson: any = JSON.parse('{' + element.element_chimique + '}');
                            const elementChmiquesCodeList: any = Object.keys(elementChimiqueJson);
                            elementChmiquesCodeList.map(async (code: any) => {
                                // on verifie si l'élément chimique existe
                                const elementChimique: any = await ElementchimiqueService.findOneByCode(code);

                                // Si l'élément chimique existe déjà mais n'est pas ajouté a la liste d'éléments d'analyse chimque, on le fait
                                if (elementChimique) {
                                    if (elementChimique.analyse_chimique.toString() === 'false') {
                                        elementChimique.analyse_chimique = 'true';
                                        await ElementchimiqueService.update(elementChimique);
                                    }
                                } else { // Sinon on crée l'élément chimique avec le code
                                    console.log('element chimique avec le code ' + code + ' sera crée');
                                    const nouveauElementChimique: any = {
                                        'code': code,
                                        'titre': code,
                                        'analyse_chimique': 'true',
                                        'bacteriologie': 'false',
                                        'isotopie': 'false',
                                        'pesticide': 'false'
                                    };

                                    await ElementchimiqueService.insert(nouveauElementChimique);
                                }
                            });

                            element.element_chimique = [elementChimiqueJson];

                            if (element.date_analyse === '') {
                                element.date_analyse = null;
                            }

                            if (element.heure_analyse === '') {
                                element.heure_analyse = null;
                            }

                            if (element.date_prelevement === '') {
                                element.date_prelevement = null;
                            }

                            if (element.heure_prelevement === '') {
                                element.heure_prelevement = null;
                            }

                            if (element.temperature === '') {
                                element.temperature = 0;
                            }

                            if (element.ph === '') {
                                element.ph = 0;
                            }

                            if (element.conductivite === '') {
                                element.conductivite = 0;
                            }

                            return element;

                        } catch (error) {
                            console.log(error.message);
                            res.status(500).json(error.message);
                        }

                    });

                    const analysechimiques: any = await Promise.all(promisedAnalysechimiques);

                    await AnalysechimiqueService.upload(result);
                    res.status(200).json(analysechimiques);
                } catch (er) {
                    console.log(er);
                    res.status(400).json('Une erreure inatendue s\'est produite durant l\'opération. Veuillez vérifier votre fichier SVP.');
                }

            }
        );

    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer la liste des analyses chimiques à exporter sous un format csv
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function exportData(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const data: any[] = await AnalysechimiqueService.export();

        res.status(200).json(data);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

