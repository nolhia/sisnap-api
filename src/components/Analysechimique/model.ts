import knex from '../../config/connection/database';

/**
 * @export
 * @interface IAnalysechimiqueModel
 */
export interface IAnalysechimiqueModel {
    id?: number;
}

const tableName: string = 'analyse_chimique';

class AnalysechimiqueModel {
    async create(objet: IAnalysechimiqueModel): Promise<IAnalysechimiqueModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IAnalysechimiqueModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[IAnalysechimiqueModel]> {
        return await knex.select('analyse_chimique.*', 'laboratoire.titre as laboratoire_titre')
            .from(tableName)
            .leftJoin('laboratoire', 'analyse_chimique.laboratoire', 'laboratoire.id');
    }

    async export(): Promise<[any]> {
        return await knex(tableName)
            .join('station', 'station.id', '=', tableName + '.station')
            .join('laboratoire', 'laboratoire.id', '=', tableName + '.laboratoire')
            .select(
                tableName + '.date_prelevement',
                tableName + '.heure_prelevement',
                tableName + '.date_analyse',
                tableName + '.heure_analyse',
                tableName + '.chimiste',
                tableName + '.temperature',
                tableName + '.conductivite',
                tableName + '.ph',
                tableName + '.element_chimique',
                tableName + '.note',
                'laboratoire.code as laboratoire',
                'station.code as station',
                tableName + '.longitude',
                tableName + '.latitude',
                tableName + '.altitude',
                tableName + '.nom_localite as nom localite',
                tableName + '.note',
                tableName + '.status',
            );
    }

    async report(id: any): Promise<[any]> {
        return await knex(tableName)
            .join('station', 'station.id', '=', tableName + '.station')
            .join('laboratoire', 'laboratoire.id', '=', tableName + '.laboratoire')
            //.join('region', 'region.id', '=', 'station.region_id')
            //.join('departement', 'departement.id', '=', 'station.departement_id')
            //.join('commune', 'commune.id', '=', 'station.commune_id')
            .select(
                tableName + '.date_prelevement',
                tableName + '.heure_prelevement',
                tableName + '.date_analyse',
                tableName + '.heure_analyse',
                tableName + '.chimiste',
                tableName + '.temperature',
                tableName + '.conductivite',
                tableName + '.ph',
                tableName + '.element_chimique',
                tableName + '.note',
                'laboratoire.titre as laboratoire_titre',
                'station.code as station_titre',
                'station.longitude as station_longitude',
                'station.latitude as station_latitude', 
                'station.region_id as station_region_id', 
                'station.departement_id as station_departement_id', 
                'station.commune_id as station_commune_id', 
                tableName + '.longitude',
                tableName + '.latitude',
                tableName + '.altitude',
                tableName + '.nom_localite as nom_localite',
                tableName + '.note',
                tableName + '.status',
            )
            .where(tableName +'.id', id).first();
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id).del();
    }

    async update(Analysechimique: IAnalysechimiqueModel): Promise<IAnalysechimiqueModel> {
        return await knex(tableName).where({ id: Analysechimique.id }).update(Analysechimique);
    }

    async upload(data: any): Promise<any> {
        return await knex(tableName).insert(data);
    }
}

export default new AnalysechimiqueModel();
