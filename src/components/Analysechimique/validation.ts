import * as Joi from 'joi';
import Validation from '../validation';
import { IAnalysechimiqueModel } from './model';

/**
 * Validation des données des analyses chimiques avant enregistrement dans la BD
 * @export
 * @class AnalysechimiqueValidation
 * @extends Validation
 */
class AnalysechimiqueValidation extends Validation {

    /**
     * Creates an instance of AnalysechimiqueValidation.
     * @memberof AnalysechimiqueValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {IAnalysechimiqueModel} params
     * @returns {Joi.ValidationResult<IAnalysechimiqueModel >}
     * @memberof AnalysechimiqueValidation
     */
    createAnalysechimique(
        params: IAnalysechimiqueModel
    ): Joi.ValidationResult<IAnalysechimiqueModel> {
        const schema: Joi.Schema = Joi.object().keys({
            date_prelevement: Joi.string().allow('').optional(),
            heure_prelevement: Joi.string().allow(null).optional(),
            date_analyse: Joi.string().required(),
            heure_analyse: Joi.string().allow(null).optional(),
            temperature: Joi.string().required(),
            conductivite: Joi.string().required(),
            ph: Joi.string().required(),
            element_chimique: Joi.any().allow('').optional(),
            note: Joi.string().allow('').optional(),
            laboratoire: Joi.string().required(),
            chimiste: Joi.string().allow('').optional(),
            station: Joi.string().optional(),
            status: Joi.string().optional(),
            longitude: Joi.number().allow(null).optional(),
            latitude: Joi.number().allow(null).optional(),
            altitude: Joi.number().allow(null).optional(),
            nom_localite: Joi.string().allow('').optional(),
            ajouter_par: Joi.string().optional(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {IAnalysechimiqueModel} params
     * @returns {Joi.ValidationResult<IAnalysechimiqueModel >}
     * @memberof AnalysechimiqueValidation
     */
    updateAnalysechimique(
        params: IAnalysechimiqueModel
    ): Joi.ValidationResult<IAnalysechimiqueModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string().required(),
            date_prelevement: Joi.string().allow('').optional(),
            heure_prelevement: Joi.string().allow(null).optional(),
            date_analyse: Joi.string().required(),
            heure_analyse: Joi.string().allow(null).optional(),
            temperature: Joi.string().required(),
            conductivite: Joi.string().required(),
            ph: Joi.string().required(),
            element_chimique: Joi.any().allow('').optional(),
            note: Joi.string().allow('').optional(),
            laboratoire: Joi.string().required(),
            chimiste: Joi.string().allow('').optional(),
            station: Joi.string().optional(),
            status: Joi.string().optional(),
            longitude: Joi.number().allow(null).optional(),
            latitude: Joi.number().allow(null).optional(),
            altitude: Joi.number().allow(null).optional(),
            nom_localite: Joi.string().allow('').optional(),
            modifier_par: Joi.string().optional(),
            modifier_le: Joi.string().optional(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof AnalysechimiqueValidation
     */
    getAnalysechimique(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof AnalysechimiqueValidation
     */
    removeAnalysechimique(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

}

export default new AnalysechimiqueValidation();
