import * as Joi from 'joi';
import AnalysechimiqueModel, { IAnalysechimiqueModel } from './model';
import AnalysechimiqueValidation from './validation';
import { IAnalysechimiqueService } from './interface';

/**
 * @export
 * @implements {IAnalysechimiqueService}
 */
const AnalysechimiqueService: IAnalysechimiqueService = {
    /**
     * @param {IAnalysechimiqueModel} Analysechimique
     * @returns {Promise < IAnalysechimiqueModel >}
     * @memberof AnalysechimiqueService
     */
    async insert(body: IAnalysechimiqueModel): Promise<IAnalysechimiqueModel> {
        try {
            const validate: Joi.ValidationResult<IAnalysechimiqueModel> = AnalysechimiqueValidation.createAnalysechimique(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const analysechimique: IAnalysechimiqueModel = await AnalysechimiqueModel.create(body);

            return analysechimique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IAnalysechimiqueModel} Analysechimique
     * @returns {Promise < IAnalysechimiqueModel >}
     * @memberof AnalysechimiqueService
     */
    async update(body: IAnalysechimiqueModel): Promise<IAnalysechimiqueModel> {
        try {
            const validate: Joi.ValidationResult<IAnalysechimiqueModel> = AnalysechimiqueValidation.updateAnalysechimique(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const analysechimique: IAnalysechimiqueModel = await AnalysechimiqueModel.update(body);

            return analysechimique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IAnalysechimiqueModel >}
     * @memberof AnalysechimiqueService
     */
    async findOne(id: string): Promise<IAnalysechimiqueModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = AnalysechimiqueValidation.getAnalysechimique({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await AnalysechimiqueModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < IAnalysechimiqueModel[] >}
     * @memberof AnalysechimiqueService
     */
    async findAll(): Promise<IAnalysechimiqueModel[]> {
        try {
            return await AnalysechimiqueModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < any[] >}
     * @memberof AnalysechimiqueService
     */
    async export(): Promise<any[]> {
        try {
            const data: any[] = await AnalysechimiqueModel.export();
            data.map((element) => {
                element.element_chimique = JSON.stringify(element.element_chimique[0])
                    .replace('{', '')
                    .replace('}', '')
                    .replace(/,/g, ' | ');
            });

            return data;

        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < any[] >}
     * @memberof AnalysechimiqueService
     */
    async report(id: string): Promise<IAnalysechimiqueModel> {
        try {
            const data: any = await AnalysechimiqueModel.report(id);
            /*data.map((element) => {
                element.element_chimique = JSON.stringify(element.element_chimique[0])
                    .replace('{', '')
                    .replace('}', '')
                    .replace(/,/g, ' | ');
            });
            */
            return data;

        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IAnalysechimiqueModel >}
     * @memberof AnalysechimiqueService
     */
    async remove(id: string): Promise<IAnalysechimiqueModel> {
        try {
            return await AnalysechimiqueModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {any} data
     * @returns {Promise < IAnalysechimiqueModel >}
     * @memberof AnalysechimiqueService
     */
    async upload(data: any): Promise<IAnalysechimiqueModel> {
        try {
            return await AnalysechimiqueModel.upload(data);
        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default AnalysechimiqueService;
