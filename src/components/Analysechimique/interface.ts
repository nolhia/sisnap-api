import { IAnalysechimiqueModel } from './model';

/**
 * @export
 * @interface IAnalysechimiqueService
 */
export interface IAnalysechimiqueService {

    /**
     * @param {IAnalysechimiqueModel} IAnalysechimiqueModel
     * @returns {Promise<IAnalysechimiqueModel>}
     * @memberof IAnalysechimiqueService
     */
    insert(IAnalysechimiqueModel: IAnalysechimiqueModel): Promise<IAnalysechimiqueModel>;

    /**
     * @param {IAnalysechimiqueModel} IAnalysechimiqueModel
     * @returns {Promise<IAnalysechimiqueModel>}
     * @memberof IAnalysechimiqueService
     */
    update(IAnalysechimiqueModel: IAnalysechimiqueModel): Promise<IAnalysechimiqueModel>;

    /**
     * @param {string} id
     * @returns {Promise<IAnalysechimiqueModel>}
     * @memberof IAnalysechimiqueService
     */
    findOne(id: string): Promise<IAnalysechimiqueModel>;

    /**
     * @returns {Promise<IAnalysechimiqueModel[]>}
     * @memberof IAnalysechimiqueService
     */
    findAll(): Promise<IAnalysechimiqueModel[]>;

    /**
     * @returns {Promise<any[]>}
     * @memberof IAnalysechimiqueService
     */
    export(): Promise<any[]>;

    /**
     * @returns {Promise<any[]>}
     * @memberof IAnalysechimiqueService
     */
    report(id: string): Promise<any>;

    /**
     * @param {string} id
     * @returns {Promise<IAnalysechimiqueModel>}
     * @memberof IAnalysechimiqueService
     */
    remove(id: string): Promise<any>;

    /**
     * @param {any} data
     * @returns {Promise<any>}
     * @memberof IAnalysechimiqueService
     */
    upload(data: any): Promise<any>;

}
