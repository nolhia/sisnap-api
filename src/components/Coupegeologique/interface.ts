import { ICoupegeologiqueModel } from './model';

/**
 * @export
 * @interface ICoupegeologiqueService
 */
export interface ICoupegeologiqueService {

    /**
     * @param {ICoupegeologiqueModel} ICoupegeologiqueModel
     * @returns {Promise<ICoupegeologiqueModel>}
     * @memberof ICoupegeologiqueService
     */
    insert(ICoupegeologiqueModel: ICoupegeologiqueModel): Promise<ICoupegeologiqueModel>;

    /**
     * @param {ICoupegeologiqueModel} ICoupegeologiqueModel
     * @returns {Promise<ICoupegeologiqueModel>}
     * @memberof ICoupegeologiqueService
     */
    update(ICoupegeologiqueModel: ICoupegeologiqueModel): Promise<ICoupegeologiqueModel>;

    /**
     * @param {string} id
     * @returns {Promise<ICoupegeologiqueModel>}
     * @memberof ICoupegeologiqueService
     */
    findOne(id: string): Promise<ICoupegeologiqueModel>;

    /**
     * @returns {Promise<ICoupegeologiqueModel[]>}
     * @memberof ICoupegeologiqueService
     */
    findAll(): Promise<ICoupegeologiqueModel[]>;

    /**
     * @returns {Promise<ICoupegeologiqueModel[]>}
     * @memberof ICoupegeologiqueService
     */
    getCodesLithologiques(): Promise<ICoupegeologiqueModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<ICoupegeologiqueModel>}
     * @memberof ICoupegeologiqueService
     */
    remove(id: string): Promise<any>;

}
