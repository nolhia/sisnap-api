import CoupegeologiqueService from './service';
import { HttpError } from '../../config/error';
import { ICoupegeologiqueModel } from './model';
import { NextFunction, Request, Response } from 'express';

/**
 * Créer une nouvelle coupe geologique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Coupegeologique: ICoupegeologiqueModel = await CoupegeologiqueService.insert(req.body);

        res.status(201).json(Coupegeologique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Mettre à jour une coupe geologique existante
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Coupegeologique: ICoupegeologiqueModel = await CoupegeologiqueService.update(req.body);

        res.status(200).json(Coupegeologique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Retrouver une coupe geologique à l'aide de son id
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Coupegeologique: ICoupegeologiqueModel = await CoupegeologiqueService.findOne(req.params.id);

        res.status(200).json(Coupegeologique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer la liste de toutes les coupe geologiques
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Coupegeologiques: ICoupegeologiqueModel[] = await CoupegeologiqueService.findAll();

        res.status(200).json(Coupegeologiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Supprimer une coupe geologique à l'aide de son id
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Coupegeologique: ICoupegeologiqueModel = await CoupegeologiqueService.remove(req.params.id);

        res.status(200).json(Coupegeologique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}


/**
 * Récupérer la liste des codes lithologiques d'une coupe géologique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function getCodesLithologiques(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const codeslithologiques: ICoupegeologiqueModel[] = await CoupegeologiqueService.getCodesLithologiques();

        res.status(200).json(codeslithologiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
