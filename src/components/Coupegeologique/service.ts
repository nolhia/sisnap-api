import * as Joi from 'joi';
import CoupegeologiqueModel, { ICoupegeologiqueModel } from './model'; 
import { ICoupegeologiqueService } from './interface';

/**
 * @export
 * @implements {ICoupegeologiqueService}
 */
const CoupegeologiqueService: ICoupegeologiqueService = {

    /**
     * @param {ICoupegeologiqueModel} Coupegeologique
     * @returns {Promise < ICoupegeologiqueModel >}
     * @memberof CoupegeologiqueService
     */
    async insert(body: ICoupegeologiqueModel): Promise<ICoupegeologiqueModel> {
        try { 
            const Coupegeologique: ICoupegeologiqueModel = await CoupegeologiqueModel.create(body);

            return Coupegeologique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {ICoupegeologiqueModel} Coupegeologique
     * @returns {Promise < ICoupegeologiqueModel >}
     * @memberof CoupegeologiqueService
     */
    async update(body: ICoupegeologiqueModel): Promise<ICoupegeologiqueModel> {
        try {
             
            const Coupegeologique: ICoupegeologiqueModel = await CoupegeologiqueModel.update(body);

            return Coupegeologique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ICoupegeologiqueModel >}
     * @memberof CoupegeologiqueService
     */
    async findOne(id: string): Promise<ICoupegeologiqueModel> {
        try {  
            return await CoupegeologiqueModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < ICoupegeologiqueModel[] >}
     * @memberof CoupegeologiqueService
     */
    async findAll(): Promise<ICoupegeologiqueModel[]> {
        try {
            return await CoupegeologiqueModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < ICoupegeologiqueModel[] >}
     * @memberof CoupegeologiqueService
     */
    async getCodesLithologiques(): Promise<ICoupegeologiqueModel[]> {
        try {
            return await CoupegeologiqueModel.getCodesLithologiques();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ICoupegeologiqueModel >}
     * @memberof CoupegeologiqueService
     */
    async remove(id: string): Promise<ICoupegeologiqueModel> {
        try { 
            return await CoupegeologiqueModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default CoupegeologiqueService;
