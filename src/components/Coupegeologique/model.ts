import knex  from '../../config/connection/database';

/**
 * @export
 * @interface ICoupegeologiqueModel
 */
export interface ICoupegeologiqueModel {
    id?: number; 
}
  
const tableName:string = 'coupe_lithologique';

class CoupegeologiqueModel{ 
    async create(objet:ICoupegeologiqueModel): Promise<ICoupegeologiqueModel> { 
        const id:number[] = await knex(tableName).insert(objet); 

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<ICoupegeologiqueModel> {  
        return await knex.select().from(tableName).where(objet).first(); 
    }
 
    async findAll(): Promise<[ICoupegeologiqueModel]> {
        return await knex.select('coupe_lithologique.*', 'station.titre as station'
            , 'code_lithologique.code as code_lithologique').from(tableName)
        .leftJoin('station', 'coupe_lithologique.station_id', 'station.id')
        .leftJoin('code_lithologique', 'coupe_lithologique.code_lithologique_id', 'code_lithologique.id')
        .orderBy('code');
    }

    async getCodesLithologiques(): Promise<[ICoupegeologiqueModel]> {
        return await knex.select().from('code_lithologique');
    }

    async findOneAndRemove(id: any): Promise<any> { 
        return await knex(tableName).where('id', id).del();
    }

    async update(Coupegeologique:ICoupegeologiqueModel): Promise<ICoupegeologiqueModel> {
        return await knex(tableName).where({ id: Coupegeologique.id }).update(Coupegeologique);
    }
}

export default new CoupegeologiqueModel();
