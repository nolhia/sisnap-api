import ZonegeographiqueService from './service';
import { HttpError } from '../../config/error';
import { IZonegeographiqueModel } from './model';
import { NextFunction, Request, Response } from 'express';

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try { 
        const Zonegeographiques: IZonegeographiqueModel[] = await ZonegeographiqueService.findAll(req.params.type, req.params.id);

        res.status(200).json(Zonegeographiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

export async function findAllZone(req: Request, res: Response, next: NextFunction): Promise<void> {
    try { 
        const Zonegeographiques: IZonegeographiqueModel[] = await ZonegeographiqueService.findAllZone();

        res.status(200).json(Zonegeographiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

export async function findRegions(req: Request, res: Response, next: NextFunction): Promise<void> {
    try { 
        const Zonegeographiques: IZonegeographiqueModel[] = await ZonegeographiqueService.findRegions();

        res.status(200).json(Zonegeographiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

export async function findDepartements(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {  
        const Zonegeographiques: IZonegeographiqueModel[] = await ZonegeographiqueService.findDepartements(req.params.id);

        res.status(200).json(Zonegeographiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function findCommunes(req: Request, res: Response, next: NextFunction): Promise<void> {
    try { 
        const Zonegeographiques: IZonegeographiqueModel[] = await ZonegeographiqueService.findCommunes(req.params.id);

        res.status(200).json(Zonegeographiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOneRegion(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueService.findOneRegion(req.params.id);

        res.status(200).json(Zonegeographique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function findOneDepartement(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueService.findOneDepartement(req.params.id);

        res.status(200).json(Zonegeographique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function findOneCommune(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueService.findOneCommune(req.params.id);

        res.status(200).json(Zonegeographique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function createRegion(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueService.insertRegion(req.body);

        res.status(201).json(Zonegeographique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function createDepartement(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueService.insertDepartement(req.body);

        res.status(201).json(Zonegeographique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function createCommune(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueService.insertCommune(req.body);

        res.status(201).json(Zonegeographique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function updateRegion(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueService.updateRegion(req.body);

        res.status(200).json(Zonegeographique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function updateDepartement(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueService.updateDepartement(req.body);

        res.status(200).json(Zonegeographique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function updateCommune(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueService.updateCommune(req.body);

        res.status(200).json(Zonegeographique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function removeRegion(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const objet: IZonegeographiqueModel = await ZonegeographiqueService.removeRegion(req.params.id);

        res.status(200).json(objet);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function removeDepartement(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const objet: IZonegeographiqueModel = await ZonegeographiqueService.removeDepartement(req.params.id);

        res.status(200).json(objet);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function removeCommune(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const objet: IZonegeographiqueModel = await ZonegeographiqueService.removeCommune(req.params.id);

        res.status(200).json(objet);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

