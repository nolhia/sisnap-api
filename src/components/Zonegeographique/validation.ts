import * as Joi from 'joi';
import Validation from '../validation';
import { IZonegeographiqueModel } from './model';

/**
 * Validation des données des zones geographiques avant enregistrement dans la BD
 * @export
 * @class ZonegeographiqueValidation
 * @extends Validation
 */
class ZonegeographiqueValidation extends Validation {

    /**
     * Creates an instance of ZonegeographiqueValidation.
     * @memberof ZonegeographiqueValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {IZonegeographiqueModel} params
     * @returns {Joi.ValidationResult<IZonegeographiqueModel >}
     * @memberof ZonegeographiqueValidation
     */
    createZonegeographique(
        params: IZonegeographiqueModel
    ): Joi.ValidationResult<IZonegeographiqueModel> {
        const schema: Joi.Schema = Joi.object().keys({
            code: Joi.string().required(),
            titre: Joi.string().required(),
            parent_code: Joi.string()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {IZonegeographiqueModel} params
     * @returns {Joi.ValidationResult<IZonegeographiqueModel >}
     * @memberof ZonegeographiqueValidation
     */
    updateZonegeographique(
        params: IZonegeographiqueModel
    ): Joi.ValidationResult<IZonegeographiqueModel> {
        const schema: Joi.Schema = Joi.object().keys({
            code: Joi.string().required(),
            titre: Joi.string().required(),
            parent_code: Joi.string()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof ZonegeographiqueValidation
     */
    getZonegeographique(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof ZonegeographiqueValidation
     */
    removeZonegeographique(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }
}

export default new ZonegeographiqueValidation();
