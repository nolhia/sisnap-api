import ZonegeographiqueModel, { IZonegeographiqueModel } from './model';
import { IZonegeographiqueService } from './interface';

/**
 * @export
 * @implements {IZonegeographiqueModelService}
 */
const ZonegeographiqueService: IZonegeographiqueService = {
    /**
     * @returns {Promise < IZonegeographiqueModel[] >}
     * @memberof ZonegeographiqueService
     */
    async findAll(): Promise<IZonegeographiqueModel[]> {
        try {
            return await ZonegeographiqueModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async findAllZone(): Promise<any> {
        try {
            return await ZonegeographiqueModel.findAllZone();
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async findRegions(): Promise<IZonegeographiqueModel[]> {
        try {
            ZonegeographiqueModel.tableName = 'region';
            return await ZonegeographiqueModel.findRegions();
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async findDepartements(id_region: string): Promise<IZonegeographiqueModel[]> {
        try {
            ZonegeographiqueModel.tableName = 'departement';
            return await ZonegeographiqueModel.findDepartements(id_region);
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async findCommunes(id_departement: string): Promise<IZonegeographiqueModel[]> {
        try {
            ZonegeographiqueModel.tableName = 'commune';
            return await ZonegeographiqueModel.findCommunes(id_departement);
        } catch (error) {
            throw new Error(error.message);
        }
    },
    /**
     * @param {string} id
     * @returns {Promise < IZonegeographiqueModel >}
     * @memberof ZonegeographiqueService
     */
    async findOneRegion(id: string): Promise<IZonegeographiqueModel> {
        try {
            ZonegeographiqueModel.tableName = 'region';
            return await ZonegeographiqueModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async findOneDepartement(id: string): Promise<IZonegeographiqueModel> {
        try {
            ZonegeographiqueModel.tableName = 'departement';
            return await ZonegeographiqueModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async findOneCommune(id: string): Promise<IZonegeographiqueModel> {
        try {
            ZonegeographiqueModel.tableName = 'commune';
            return await ZonegeographiqueModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} code
     * @returns {Promise < IZonegeographiqueModel >}
     * @memberof ZonegeographiqueService
     */
    async findOneRegionByCode(code: string): Promise<IZonegeographiqueModel> {
        try {
            ZonegeographiqueModel.tableName = 'region';
            return await ZonegeographiqueModel.findOne({
                code: code
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    async findOneDepartementByCode(code: string): Promise<IZonegeographiqueModel> {
        try {
            ZonegeographiqueModel.tableName = 'departement';
            return await ZonegeographiqueModel.findOne({
                code: code
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    async findOneCommuneByCode(code: string): Promise<IZonegeographiqueModel> {
        try {
            ZonegeographiqueModel.tableName = 'commune';
            return await ZonegeographiqueModel.findOne({
                code: code
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IZonegeographiqueModel} Zonegeographique
     * @returns {Promise < IZonegeographiqueModel >}
     * @memberof ZonegeographiqueService
     */
    async insertRegion(body: IZonegeographiqueModel): Promise<IZonegeographiqueModel> {
        try {
            // throw new Error(validate.error.message); 
            ZonegeographiqueModel.tableName = 'region';
            const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueModel.createRegion(body);

            return Zonegeographique;
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async insertDepartement(body: IZonegeographiqueModel): Promise<IZonegeographiqueModel> {
        try {
            // throw new Error(validate.error.message);
            ZonegeographiqueModel.tableName = 'departement';
            const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueModel.createDepartement(body);

            return Zonegeographique;
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async insertCommune(body: IZonegeographiqueModel): Promise<IZonegeographiqueModel> {
        try {
            // throw new Error(validate.error.message);
            ZonegeographiqueModel.tableName = 'commune';
            const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueModel.createCommune(body);

            return Zonegeographique;
        } catch (error) {
            throw new Error(error.message);
        }
    },
    /**
    * @param {IZonegeographiqueModel} Zonegeographique
    * @returns {Promise < IZonegeographiqueModel >}
    * @memberof ZonegeographiqueService
    */
    async updateRegion(body: IZonegeographiqueModel): Promise<IZonegeographiqueModel> {
        try {
            // throw new Error(validate.error.message);
            ZonegeographiqueModel.tableName = 'region';
            const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueModel.updateRegion(body);

            return Zonegeographique;
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async updateDepartement(body: IZonegeographiqueModel): Promise<IZonegeographiqueModel> {
        try {
            // throw new Error(validate.error.message);
            ZonegeographiqueModel.tableName = 'departement';
            const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueModel.updateDepartement(body);

            return Zonegeographique;
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async updateCommune(body: IZonegeographiqueModel): Promise<IZonegeographiqueModel> {
        try {
            // throw new Error(validate.error.message);
            ZonegeographiqueModel.tableName = 'commune';
            const Zonegeographique: IZonegeographiqueModel = await ZonegeographiqueModel.updateCommune(body);

            return Zonegeographique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < INappeModel >}
     * @memberof NappeService
     */
    async removeRegion(id: string): Promise<IZonegeographiqueModel> {
        try {
            ZonegeographiqueModel.tableName = 'region';
            return await ZonegeographiqueModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async removeDepartement(id: string): Promise<IZonegeographiqueModel> {
        try {
            ZonegeographiqueModel.tableName = 'departement';
            return await ZonegeographiqueModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },
    async removeCommune(id: string): Promise<IZonegeographiqueModel> {
        try {
            ZonegeographiqueModel.tableName = 'commune';
            return await ZonegeographiqueModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }

};

export default ZonegeographiqueService;
