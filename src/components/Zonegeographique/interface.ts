import { IZonegeographiqueModel } from './model';

/**
 * @export
 * @interface IZonegeographiqueService
 */
export interface IZonegeographiqueService {

    /**
     * @returns {Promise<IZonegeographiqueModel[]>}
     * @memberof IZonegeographiqueService
     */
    findAll(type: string, id: string): Promise<IZonegeographiqueModel[]>;
    findAllZone(): Promise<any>;
    findRegions(): Promise<IZonegeographiqueModel[]>;
    findDepartements(id: string): Promise<IZonegeographiqueModel[]>;
    findCommunes(id: string): Promise<IZonegeographiqueModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<IZonegeographiqueModel>}
     * @memberof IZonegeographiqueService
     */
    findOneRegion(id: string): Promise<IZonegeographiqueModel>;
    findOneDepartement(id: string): Promise<IZonegeographiqueModel>;
    findOneCommune(id: string): Promise<IZonegeographiqueModel>;

    /**
     * @param {string} code
     * @returns {Promise<IZonegeographiqueModel>}
     * @memberof IZonegeographiqueService
     */
    findOneRegionByCode(code: string): Promise<IZonegeographiqueModel>;
    findOneDepartementByCode(code: string): Promise<IZonegeographiqueModel>;
    findOneCommuneByCode(code: string): Promise<IZonegeographiqueModel>;

    /**
     * @param {IZonegeographiqueModel} IZonegeographiqueModel
     * @returns {Promise<IZonegeographiqueModel>}
     * @memberof IZonegeographiqueService
     */
    insertRegion(IZonegeographiqueModel: IZonegeographiqueModel): Promise<IZonegeographiqueModel>;
    insertDepartement(IZonegeographiqueModel: IZonegeographiqueModel): Promise<IZonegeographiqueModel>;
    insertCommune(IZonegeographiqueModel: IZonegeographiqueModel): Promise<IZonegeographiqueModel>;

    /**
     * @param {IZonegeographiqueModel} IZonegeographiqueModel
     * @returns {Promise<IZonegeographiqueModel>}
     * @memberof IZonegeographiqueService
     */
    updateRegion(IZonegeographiqueModel: IZonegeographiqueModel): Promise<IZonegeographiqueModel>;
    updateDepartement(IZonegeographiqueModel: IZonegeographiqueModel): Promise<IZonegeographiqueModel>;
    updateCommune(IZonegeographiqueModel: IZonegeographiqueModel): Promise<IZonegeographiqueModel>;

    /**
     * @param {string} id
     * @returns {Promise<IZonegeographiqueModel>}
     * @memberof IZonegeographiqueService
     */
    removeRegion(id: string): Promise<any>;
    removeDepartement(id: string): Promise<any>;
    removeCommune(id: string): Promise<any>;
}
