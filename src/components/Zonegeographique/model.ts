import knex from '../../config/connection/database';
import { NextFunction } from 'express';

/**
 * @export
 * @interface IZonegeographiqueModel
 */
export interface IZonegeographiqueModel {
    id?: number;
}

export type AuthToken = {
    accessToken: string,
    kind: string
};

class ZonegeographiqueModel {
    public tableName: string;

    async findAll(): Promise<[IZonegeographiqueModel]> {
        return await knex.select().from(this.tableName);
    }

    async findAllZone(): Promise<any> {
        const regions = await knex.select().from('region').orderBy('code');
        const departements = await knex.select().from('departement').orderBy('code');
        const communes = await knex.select().from('commune').orderBy('code');
        return {
            regions : regions,
            departements : departements,
            communes: communes
        };
    }
    
    async findRegions(): Promise<[IZonegeographiqueModel]> {
        return await knex.select().from(this.tableName).orderBy('code');
    }
    async findDepartements(id: string): Promise<[IZonegeographiqueModel]> { 
        return await knex.select().from(this.tableName).where({region_id: id}).orderBy('code');
    }
    async findCommunes(id: string): Promise<[IZonegeographiqueModel]> { 
        return await knex.select().from(this.tableName).where({departement_id: id}).orderBy('code');
    }

    async createRegion(objet:IZonegeographiqueModel): Promise<IZonegeographiqueModel> {  
        const id:number[] = await knex(this.tableName).insert(objet); 

        return { ...objet, id: id[0] };
    }
    async createDepartement(objet:IZonegeographiqueModel): Promise<IZonegeographiqueModel> {  
        const id:number[] = await knex(this.tableName).insert(objet); 

        return { ...objet, id: id[0] };
    }
    async createCommune(objet:IZonegeographiqueModel): Promise<IZonegeographiqueModel> {  
        const id:number[] = await knex(this.tableName).insert(objet); 

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IZonegeographiqueModel> {
        return await knex.select().from(this.tableName).where(objet).first();
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(this.tableName).where('id', id).del();
    } 

    async updateRegion(project:IZonegeographiqueModel): Promise<IZonegeographiqueModel> {
        return await knex(this.tableName).where({ id: project.id }).update(project);
    }
    async updateDepartement(project:IZonegeographiqueModel): Promise<IZonegeographiqueModel> {
        return await knex(this.tableName).where({ id: project.id }).update(project);
    }
    async updateCommune(project:IZonegeographiqueModel): Promise<IZonegeographiqueModel> {
        return await knex(this.tableName).where({ id: project.id }).update(project);
    }
 

}

export default new ZonegeographiqueModel();
