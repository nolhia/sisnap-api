import AuthService from './service';
import MessageService from '../../services/MessageService';
import UtilsHelper from '../../helpers/UtilsHelper';
import HttpError from '../../config/error';
import { IUserModel } from '../User/model';
import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import app from '../../config/server/server';
const format: any = require('format-util');

/**
 * Créer un compte utilisateur
 * @export
 * @param {Request} req 
 * @param {Response} res 
 * @param {NextFunction} next 
 * @returns {Promise < void >}
 */
export async function signup(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const user: IUserModel = await AuthService.createUser(req.body);
        const token: string = jwt.sign({ email: user.email }, app.get('secret'), {
            expiresIn: '1440m'
        });

        res.json({
            status: 200,
            logged: true,
            token: token,
            message: 'Sign in successfull'
        });
    } catch (error) {
        if (error.code === 500) {
            return next(new HttpError(error.message.status, error.message));
        }
        res.json({
            status: 400,
            message: error.message
        });
    }
}

/**
 * Verifier les informations de connexion d'un utilisateur et autoriser son accès ou non
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function login(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const user: IUserModel = await AuthService.getUser(req.body);

        const token: string = jwt.sign({ email: user.email }, app.get('secret'), {
            expiresIn: '1440m'
        });

        res.json({
            status: 200,
            logged: true,
            token: token,
            message: 'Sign in successfull',
            user_info: user
        });

    } catch (error) {
        if (error.code === 500) {
            return next(new HttpError(error.message.status, error.message));
        }

        res.json({
            status: 400,
            message: error.message
        });
    }
}


/**
 * Récupérer son mot de passe
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function forgotpassword(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const user: IUserModel = await AuthService.findUser(req.body);

        const token: string = jwt.sign({ email: user.email }, app.get('secret'), {
            expiresIn: '1440m'
        });
        const url: string = `http://localhost/passwordreset/?id=${user.id}&token=${token}`;

        MessageService.subject = UtilsHelper.passwordResetMessages().titre;

        // Mail
        if (user.email) {
            MessageService.toEmail = user.email;
            MessageService.body = format(UtilsHelper.passwordResetMessages().body_mail, user.prenoms, url);
            await MessageService.sendMail();
        }

        res.json({
            status: 200
        });

    } catch (error) {
        if (error.code === 500) {
            return next(new HttpError(error.message.status, error.message));
        }
        res.status(400);
        res.json({
            status: 400,
            message: error.message
        });
    }
}

/**
 * Initialiser le mot de passe d'un utilisateur
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function passwordreset(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const data = req.body;

        if (data && data.id && data.token && jwt.verify(data.token, app.get('secret'))) {
            const user: IUserModel = await AuthService.resetPassword(data);
            res.json({
                status: 200,
                message: 'Password changed successfull'
            });
        }
        else {
            const message = "Invalid data";
            return next(new HttpError(400, message));
        }
    } catch (error) {
        if (error.code === 500) {
            return next(new HttpError(error.message.status, error.message));
        }

        res.status(400);
        res.json({
            status: 400,
            message: error.message
        });
    }
}

