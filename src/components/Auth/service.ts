import * as Joi from 'joi';
import * as bcrypt from 'bcryptjs';
import AuthValidation from './validation';
import UserModel, { IUserModel } from '../User/model';
import { IAuthService } from './interface';

/**
 * @export
 * @implements {IAuthService}
 */
const AuthService: IAuthService = {

    /**
     * @param {IUserModel} body
     * @returns {Promise <IUserModel>}
     * @memberof AuthService
     */
    async createUser(body: IUserModel): Promise < IUserModel > {
        try {
            const validate: Joi.ValidationResult < IUserModel > = AuthValidation.createUser(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const user: IUserModel = {
                email: body.email,
                password: body.password
            };

            const query: IUserModel = await UserModel.findOne({
                email: body.email
            });

            if (query) {
                throw new Error('Cet email existe déjà.');
            }

            const saved: IUserModel = await UserModel.create(user);

            return saved;
        } catch (error) {
            throw new Error(error);
        }
    },
    /**
     * @param {IUserModel} body 
     * @returns {Promise <IUserModel>}
     * @memberof AuthService
     */
    async getUser(body: IUserModel): Promise < IUserModel > {
        try { 
            const validate: Joi.ValidationResult < IUserModel > = AuthValidation.getUser(body);
            
            if (validate.error) { 
                throw new Error(validate.error.message);
            }
            
            const user: IUserModel = await UserModel.findOne({ email: body.email });   
                  
            const isMatched: boolean = await UserModel.comparePassword(user, body.password);
           
            if (isMatched) {
                return user;
            }

            throw new Error("Le login ou le mot de passe est incorrect.");
            
        } catch (error) {
            throw new Error(error);
        }
    },

    /**
     * @param {IUserModel} body 
     * @returns {Promise <IUserModel>}
     * @memberof AuthService
     */
    async findUser(body: IUserModel): Promise < IUserModel > {
        try {    
            const user: IUserModel = await UserModel.findOne({ email: body.email });   
                   
            if (user) {
                return user;
            }

            throw new Error("L'adresse email fournie est incorrecte.");
            
        } catch (error) {
            throw new Error(error);
        }
    },

    /**
     * @param {IUserModel} body 
     * @returns {Promise <IUserModel>}
     * @memberof AuthService
     */
    async resetPassword(body: any): Promise < IUserModel > {
        try {  
            const data = {
                id: body.id,
                password: bcrypt.hashSync(body.password, 10)
            }   
            const user: IUserModel = await UserModel.update(data);
            if (user) {
                return user;
            } 
            
        } catch (error) {
            throw new Error(error);
        }
    }
};

export default AuthService;
