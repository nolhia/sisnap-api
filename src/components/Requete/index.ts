import RequeteService from './service';
import { HttpError } from '../../config/error'; 
import { NextFunction, Request, Response } from 'express';

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function mesure(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const response: any = await RequeteService.mesure(req.body);

        res.status(200).json(response);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function graphePiezometries(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const response: any = await RequeteService.graphePiezometries(req.body);

        res.status(200).json(response);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function graphePluviometries(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const response: any = await RequeteService.graphePluviometries(req.body);

        res.status(200).json(response);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
export async function grapheHydrometries(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const response: any = await RequeteService.grapheHydrometries(req.body);

        res.status(200).json(response);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function analyse(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const response: any = await RequeteService.analyse(req.body);

        res.status(200).json(response);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
 