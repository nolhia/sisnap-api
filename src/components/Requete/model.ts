import knex from '../../config/connection/database';

/**
 * @export
 * @interface IAnalysechimiqueModel
 */
export interface IRequeteModel {
    id?: number;
    element_chimique?: JSON;
}

class RequeteModel {

    async mesure(objet: any): Promise<IRequeteModel> {
        console.log(objet)
        if(objet.type_requete_id == 1){
            return await knex.select('piezometrie.*', 'station.titre as station_titre')
                            .from('piezometrie')
                            .leftJoin('station', 'piezometrie.station_suivi_id', 'station.id')
                            .where('date_mesure', '>=', objet.date_debut)
                            .where('date_mesure', '<=', objet.date_fin)
                            .where('station_suivi_id', objet.station_id)
                            .orderBy('date_mesure', 'asc');
        }
        if(objet.type_requete_id == 2){
            return await knex.select('pluviometrie.*', 'enquete.titre as enquete_titre')
                            .from('pluviometrie')
                            .leftJoin('enquete', 'pluviometrie.enquete_id', 'enquete.id')
                            .where('date_pluie', '>=', objet.date_debut)
                            .where('date_pluie', '<=', objet.date_fin)
                            //.whereIn('enquete_id', objet.enquete)
                            .orderBy('date_pluie', 'asc');
        }
        if(objet.type_requete_id == 3){
            return await knex.select('hydrometrie.*', 'enquete.titre as enquete_titre')
                                .from('hydrometrie')
                                .leftJoin('enquete', 'hydrometrie.enquete_id', 'enquete.id')
                                .where('date_releve', '>=', objet.date_debut)
                                .where('date_releve', '<=', objet.date_fin)
                                //.whereIn('enquete_id', objet.enquete)
                                .orderBy('date_releve', 'asc');
        }
    }

    async graphePiezometries(objet: any): Promise<IRequeteModel> {

        return await knex.select('piezometrie.*', 'station.titre as station_titre')
                        .from('piezometrie')
                        .leftJoin('station', 'piezometrie.station_suivi_id', 'station.id')
                        .where('date_mesure', '>=', objet.debut)
                        .where('date_mesure', '<=', objet.fin)
                        .whereIn('station_suivi_id', objet.station)
                        .orderBy('date_mesure', 'asc');
    }

    async grapheHydrometries(objet: any): Promise<IRequeteModel> {
        return await knex.select('hydrometrie.*', 'enquete.titre as enquete_titre')
                        .from('hydrometrie')
                        .leftJoin('enquete', 'hydrometrie.enquete_id', 'enquete.id')
                        .whereIn('enquete_id', objet.enquete)
                        .orderBy('date_releve', 'asc');
    }

    async graphePluviometries(objet: any): Promise<IRequeteModel> {
        return await knex.select('pluviometrie.*', 'enquete.titre as enquete_titre')
                        .from('pluviometrie')
                        .leftJoin('enquete', 'pluviometrie.enquete_id', 'enquete.id')
                        .whereIn('enquete_id', objet.enquete)
                        .orderBy('date_pluie', 'asc');
    }

    async analyse(objet: any): Promise<[IRequeteModel]> {
        if(objet.type_requete_id == 1){
            return await knex.select('analyse_chimique.*', 'laboratoire.titre as laboratoire_titre',
            'station.titre as station_titre')
            .from('analyse_chimique')
            .leftJoin('laboratoire', 'analyse_chimique.laboratoire', 'laboratoire.id')
            .leftJoin('station', 'analyse_chimique.station', 'station.id');
        }
        if(objet.type_requete_id == 2){
            return await knex.select('analyse_bacteriologie.*', 'laboratoire.titre as laboratoire_titre',
            'station.titre as station_titre')
            .from('analyse_bacteriologie')
            .leftJoin('laboratoire', 'analyse_bacteriologie.laboratoire', 'laboratoire.id')
            .leftJoin('station', 'analyse_bacteriologie.station', 'station.id');
        }
        if(objet.type_requete_id == 3){
            return await knex.select('analyse_isotope.*', 'laboratoire.titre as laboratoire_titre',
            'station.titre as station_titre')
                .from('analyse_isotope')
                .leftJoin('laboratoire', 'analyse_isotope.laboratoire', 'laboratoire.id')
                .leftJoin('station', 'analyse_isotope.station', 'station.id');
        }
        if(objet.type_requete_id == 4){
            return await knex.select('analyse_pesticide.*', 'laboratoire.titre as laboratoire_titre',
            'station.titre as station_titre')
            .from('analyse_pesticide')
            .leftJoin('laboratoire', 'analyse_pesticide.laboratoire', 'laboratoire.id')
            .leftJoin('station', 'analyse_pesticide.station', 'station.id');
        }
    }

}

export default new RequeteModel();
