import { IRequeteModel } from './model';

/**
 * @export
 * @interface IRequeteService
 */
export interface IRequeteService {
 
    /**
     * @param {string} id
     * @returns {Promise<any>}
     * @memberof IRequeteService
     */
    mesure(data: any): Promise<any>;
    graphePiezometries(data: any): Promise<any>;
    graphePluviometries(data: any): Promise<any>;
    grapheHydrometries(data: any): Promise<any>;

    /**
     * @returns {Promise<any>}
     * @memberof IRequeteService
     */
    analyse(data: any): Promise<any>; 
}
