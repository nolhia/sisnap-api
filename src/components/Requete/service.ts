import RequeteModel, { IRequeteModel } from './model'; 
import { IRequeteService } from './interface';

/**
 * @export
 * @implements {IRequeteService}
 */
const RequeteService: IRequeteService = {
    /**
     * @param {IRequeteModel} Requete
     * @returns {Promise < IRequeteModel >}
     * @memberof RequeteService
     */
    async mesure(body: any): Promise<IRequeteModel> {
        try {  
            const response: any = await RequeteModel.mesure(body);

            return response;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IRequeteModel} Requete
     * @returns {Promise < IRequeteModel >}
     * @memberof RequeteService
     */
    async graphePiezometries(body: any): Promise<IRequeteModel> {
        try {  
            const response: any = await RequeteModel.graphePiezometries(body);

            return response;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IRequeteModel} Requete
     * @returns {Promise < IRequeteModel >}
     * @memberof RequeteService
     */
    async graphePluviometries(body: any): Promise<IRequeteModel> {
        try {  
            const response: any = await RequeteModel.graphePluviometries(body);

            return response;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IRequeteModel} Requete
     * @returns {Promise < IRequeteModel >}
     * @memberof RequeteService
     */
    async grapheHydrometries(body: any): Promise<IRequeteModel> {
        try {  
            const response: any = await RequeteModel.grapheHydrometries(body);

            return response;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IAnalysechimiqueModel} Analysechimique
     * @returns {Promise < IAnalysechimiqueModel >}
     * @memberof AnalysechimiqueService
     */
    async analyse(body: any): Promise<IRequeteModel> {
        try { 
            //body.element_chimique = JSON.parse(body.element_chimique);
            const response: any = await RequeteModel.analyse(body);

            return response;
        } catch (error) {
            throw new Error(error.message);
        }
    },

     
};

export default RequeteService;
