import * as AuthComponent from './Auth';
import * as UserComponent from './User';
import * as ZonegeographiqueComponent from './Zonegeographique';
import * as TypeStationComponent from './TypeStation';
import * as LaboratoireComponent from './Laboratoire';
import * as StationComponent from './Station';
import * as CoupegeologiqueComponent from './Coupegeologique';
import * as NappeComponent from './Nappe';
import * as ElementchimiqueComponent from './Elementchimique';
import * as TypeOuvrageComponent from './TypeOuvrage';
import * as EnqueteComponent from './Enquete';
import * as PiezometrieComponent from './Piezometrie';
import * as HydrometrieComponent from './Hydrometrie';
import * as PluviometrieComponent from './Pluviometrie';
import * as AnalysechimiqueComponent from './Analysechimique';
import * as AnalyseisotopeComponent from './Analyseisotope';
import * as AnalysepesticideComponent from './Analysepesticide';
import * as RequeteComponent from './Requete';
import * as AnalysebacteriologieComponent from './Analysebacteriologie';
import * as CarteStatiqueComponent from './CarteStatique';
import * as CodelithologiqueComponent from './Codelithologique';

export {
    AuthComponent,
    UserComponent,
    ZonegeographiqueComponent,
    TypeStationComponent,
    LaboratoireComponent,
    StationComponent,
    CoupegeologiqueComponent,
    NappeComponent,
    ElementchimiqueComponent,
    TypeOuvrageComponent,
    EnqueteComponent,
    PiezometrieComponent,
    PluviometrieComponent,
    HydrometrieComponent,
    AnalysechimiqueComponent,
    RequeteComponent,
    AnalyseisotopeComponent,
    AnalysepesticideComponent,
    AnalysebacteriologieComponent,
    CarteStatiqueComponent,
    CodelithologiqueComponent
};
