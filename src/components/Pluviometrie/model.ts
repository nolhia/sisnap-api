import knex from '../../config/connection/database';

/**
 * @export
 * @interface IPluviometrieModel
 */
export interface IPluviometrieModel {
    id?: number;
}

const tableName: string = 'pluviometrie';

class PluviometrieModel {
    async create(objet: IPluviometrieModel): Promise<IPluviometrieModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IPluviometrieModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[IPluviometrieModel]> {
        return await knex.select('pluviometrie.*', 'station.titre as station').from(tableName)
            .leftJoin('station', 'pluviometrie.station_suivi_id', 'station.id');
    }

    async export(): Promise<[IPluviometrieModel]> {
        return await knex.select(
            'pluviometrie.date_pluie', 'pluviometrie.heure_pluie',
            'pluviometrie.quantite', 'pluviometrie.note', 'pluviometrie.status',
            'station.titre as station',
            'region.titre as region',
            'enquete.titre as campagne').from(tableName)
            .leftJoin('station', 'pluviometrie.station_suivi_id', 'station.id')
            .leftJoin('region', 'pluviometrie.region_id', 'region.id')
            .leftJoin('enquete', 'pluviometrie.enquete_id', 'enquete.id');
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id).del();
    }

    async update(Pluviometrie: IPluviometrieModel): Promise<IPluviometrieModel> {
        return await knex(tableName).where({ id: Pluviometrie.id }).update(Pluviometrie);
    }

    async upload(data: any): Promise<any> {
        return await knex(tableName).insert(data);
    }
}

export default new PluviometrieModel();
