
import PluviometrieModel, { IPluviometrieModel } from './model';
import { IPluviometrieService } from './interface';

/**
 * @export
 * @implements {IPluviometrieService}
 */
const PluviometrieService: IPluviometrieService = {

    /**
     * @param {IPluviometrieModel} Pluviometrie
     * @returns {Promise < IPluviometrieModel >}
     * @memberof PluviometrieService
     */
    async insert(body: IPluviometrieModel): Promise<IPluviometrieModel> {
        try {
            const Pluviometrie: IPluviometrieModel = await PluviometrieModel.create(body);

            return Pluviometrie;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IPluviometrieModel} Pluviometrie
     * @returns {Promise < IPluviometrieModel >}
     * @memberof PluviometrieService
     */
    async update(body: IPluviometrieModel): Promise<IPluviometrieModel> {
        try {
            const Pluviometrie: IPluviometrieModel = await PluviometrieModel.update(body);

            return Pluviometrie;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IPluviometrieModel >}
     * @memberof PluviometrieService
     */
    async findOne(id: string): Promise<IPluviometrieModel> {
        try {
            return await PluviometrieModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < IPluviometrieModel[] >}
     * @memberof PluviometrieService
     */
    async findAll(): Promise<IPluviometrieModel[]> {
        try {
            return await PluviometrieModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IPluviometrieModel >}
     * @memberof PluviometrieService
     */
    async remove(id: string): Promise<IPluviometrieModel> {
        try {
            return await PluviometrieModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {any} data
     * @returns {Promise < IPiezometrieModel >}
     * @memberof PluviometrieService
     */
    async upload(data: any): Promise<IPluviometrieModel> {
        try {
            return await PluviometrieModel.upload(data);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
    * @returns {Promise < any[] >}
    * @memberof PluviometrieService
    */
    async export(): Promise<any[]> {
        try {
            const data: any[] = await PluviometrieModel.export();

            return data;

        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default PluviometrieService;
