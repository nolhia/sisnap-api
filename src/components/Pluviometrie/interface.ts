import { IPluviometrieModel } from './model';

/**
 * @export
 * @interface IPluviometrieService
 */
export interface IPluviometrieService {

    /**
     * @param {IPluviometrieModel} IPluviometrieModel
     * @returns {Promise<IPluviometrieModel>}
     * @memberof IPluviometrieService
     */
    insert(IPluviometrieModel: IPluviometrieModel): Promise<IPluviometrieModel>;

    /**
     * @param {IPluviometrieModel} IPluviometrieModel
     * @returns {Promise<IPluviometrieModel>}
     * @memberof IPluviometrieService
     */
    update(IPluviometrieModel: IPluviometrieModel): Promise<IPluviometrieModel>;

    /**
     * @param {string} id
     * @returns {Promise<IPluviometrieModel>}
     * @memberof IPluviometrieService
     */
    findOne(id: string): Promise<IPluviometrieModel>;

    /**
     * @returns {Promise<IPluviometrieModel[]>}
     * @memberof IPluviometrieService
     */
    findAll(): Promise<IPluviometrieModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<IPluviometrieModel>}
     * @memberof IPluviometrieService
     */
    remove(id: string): Promise<any>;

    /**
     * @param {any} data
     * @returns {Promise<any>}
     * @memberof IPluviometrieService
     */
    upload(data: any): Promise<any>;

    /**
     * @returns {Promise<any[]>}
     * @memberof IPluviometrieService
     */
    export(): Promise<any[]>;

}
