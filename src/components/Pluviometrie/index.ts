import PluviometrieService from './service';
import StationService from '../Station/service';
import { HttpError } from '../../config/error';
import { IPluviometrieModel } from './model';
import { NextFunction, Request, Response } from 'express';
import UtilsHelper from '../../helpers/UtilsHelper';
const fs: any = require('fs');

/**
 * Créer une nouvelle pluviométrie
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Pluviometrie: IPluviometrieModel = await PluviometrieService.insert(req.body);

        res.status(201).json(Pluviometrie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Mettre à jour une pluviométrie existante
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Pluviometrie: IPluviometrieModel = await PluviometrieService.update(req.body);

        res.status(200).json(Pluviometrie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer une pluviometrie à l'aide de son id
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Pluviometrie: IPluviometrieModel = await PluviometrieService.findOne(req.params.id);

        res.status(200).json(Pluviometrie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer la liste de toutes les pluviométries
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Pluviometries: IPluviometrieModel[] = await PluviometrieService.findAll();

        res.status(200).json(Pluviometries);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Supprimer une pluviométrie à l'aide de son id
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Pluviometrie: IPluviometrieModel = await PluviometrieService.remove(req.params.id);

        res.status(200).json(Pluviometrie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Traiter un fichier csv contenant une liste de pluviométrie à insérer dans la BD
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function upload(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const exceltojson: any = await UtilsHelper.convertExcelToJson(req);

        await exceltojson(
            {
                input: req.file.path,
                output: null,
                lowerCaseHeaders: true
            },
            async (err: any, result: any) => {
                if (err) {
                    res.status(400).json({ error_code: 1, err_desc: err, data: null });
                }
                // supprimer le fichier 
                fs.unlinkSync(req.file.path);

                // pour ligne de données on verifie si la station existe
                // 1 - Si oui on recupère leur id
                // 2 - Si non on les insères dans leur table respective et on récupère l'id de l'enregistrement
                try {
                    const promisedPluviometries: any = result.map(async (element: any) => {
                        const stationCode: any = element.station;
                        element.status = 'pending';

                        try {
                            // Traitement des stations
                            const station: any = await StationService.findOneByCode(stationCode);
                            if (station && station.id) {
                                element.station_suivi_id = station.id;
                            } else {
                                console.log('la station ' + stationCode + ' n\'existe pas pour l\'instant, elle sera créee');
                                const stationData: any = {
                                    'code': stationCode,
                                    'titre': stationCode,
                                    'latitude': 1,
                                    'longitude': 1,
                                    'altitude': 1,
                                    'type_station_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                    'type_ouvrage_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                    'nappe_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                };
                                const nouvelleStation: any = await StationService.insert(stationData);
                                if (nouvelleStation && nouvelleStation.id) {
                                    element.station_suivi_id = nouvelleStation.id;
                                }

                            }

                            delete element['station'];

                            return element;

                        } catch (error) {
                            console.log(error.message);
                            res.status(500).json(error.message);
                        }

                    });

                    const pluviometries: any = await Promise.all(promisedPluviometries);

                    await PluviometrieService.upload(result);
                    res.status(200).json(pluviometries);
                } catch (er) {
                    console.log(er);
                    res.status(400).json('Une erreure inatendue s\'est produite durant l\'opération. Veuillez vérifier votre fichier SVP.');
                }

            }
        );

    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * préparer les données des pluviométries existantes pour un export
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function exportData(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const data: any[] = await PluviometrieService.export();

        res.status(200).json(data);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
