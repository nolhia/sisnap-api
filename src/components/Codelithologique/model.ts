import knex from '../../config/connection/database';

/**
 * @export
 * @interface ICodelithologiqueModel
 */
export interface ICodelithologiqueModel {
    id?: number;
    code?: string;
    titre?: string;
}

export type AuthToken = {
    accessToken: string,
    kind: string
};

const tableName: string = 'code_lithologique';

class CodelithologiqueModel {
    async create(objet: ICodelithologiqueModel): Promise<ICodelithologiqueModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<ICodelithologiqueModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[ICodelithologiqueModel]> {
        return await knex.select().from(tableName).orderBy('code');
    }

    async findOneAndRemove(id: any): Promise<any> { 
        return await knex(tableName).where('id', id).del();
    }

    async update(Codelithologique: ICodelithologiqueModel): Promise<ICodelithologiqueModel> {
        return await knex(tableName).where({ id: Codelithologique.id }).update(Codelithologique);
    }

    async upload(data:any): Promise<any> { 
        return await knex(tableName).insert(data);  
    }
}

export default new CodelithologiqueModel();
