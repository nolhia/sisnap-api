import CodelithologiqueService from './service';
import { HttpError } from '../../config/error';
import { ICodelithologiqueModel } from './model';
import { NextFunction, Request, Response } from 'express';
import UtilsHelper from '../../helpers/UtilsHelper';
const fs: any = require('fs');

/**
 * Créer un code lithologique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Codelithologique: ICodelithologiqueModel = await CodelithologiqueService.insert(req.body);

        res.status(201).json(Codelithologique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Mettre à jour un code lithologique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Codelithologique: ICodelithologiqueModel = await CodelithologiqueService.update(req.body);

        res.status(200).json(Codelithologique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Traiter un fichier csv contenant une liste de code lithologique à insérer dans la BD
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function upload(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const exceltojson: any = await UtilsHelper.convertExcelToJson(req);

        await exceltojson({
            input: req.file.path,
            output: null,
            lowerCaseHeaders: true
        }, async function (err, result) {

            if (err) {
                res.status(400).json({ error_code: 1, err_desc: err, data: null })
            }
            //supprimer le fichier 
            fs.unlinkSync(req.file.path);
            await CodelithologiqueService.upload(result);
            res.status(200).json(result);
        })

    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer un code lithologique à l'aide de son id
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Codelithologique: ICodelithologiqueModel = await CodelithologiqueService.findOne(req.params.id);

        res.status(200).json(Codelithologique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer la liste de tous les codes lithologiques de la BD
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Codelithologiques: ICodelithologiqueModel[] = await CodelithologiqueService.findAll();

        res.status(200).json(Codelithologiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Supprimer un code lithologique à l'aide de son id
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Codelithologique: ICodelithologiqueModel = await CodelithologiqueService.remove(req.params.id);

        res.status(200).json(Codelithologique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
