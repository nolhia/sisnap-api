import { ICodelithologiqueModel } from './model';

/**
 * @export
 * @interface ICodelithologiqueService
 */
export interface ICodelithologiqueService {

    /**
     * @param {ICodelithologiqueModel} ICodelithologiqueModel
     * @returns {Promise<ICodelithologiqueModel>}
     * @memberof ICodelithologiqueService
     */
    insert(ICodelithologiqueModel: ICodelithologiqueModel): Promise<ICodelithologiqueModel>;

    /**
     * @param {ICodelithologiqueModel} ICodelithologiqueModel
     * @returns {Promise<ICodelithologiqueModel>}
     * @memberof ICodelithologiqueService
     */
    update(ICodelithologiqueModel: ICodelithologiqueModel): Promise<ICodelithologiqueModel>;

    /**
     * @param {string} id
     * @returns {Promise<ICodelithologiqueModel>}
     * @memberof ICodelithologiqueService
     */
    findOne(id: string): Promise<ICodelithologiqueModel>;

    /**
     * @param {string} code
     * @returns {Promise<ICodelithologiqueModel>}
     * @memberof ICodelithologiqueService
     */
    findOneByCode(code: string): Promise<ICodelithologiqueModel>;

    /**
     * @returns {Promise<ICodelithologiqueModel[]>}
     * @memberof ICodelithologiqueService
     */
    findAll(): Promise<ICodelithologiqueModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<ICodelithologiqueModel>}
     * @memberof ICodelithologiqueService
     */
    remove(id: string): Promise<any>;

    /**
     * @param {any} data
     * @returns {Promise<any>}
     * @memberof ICodelithologiqueService
     */
    upload(data: any): Promise<any>;

}
