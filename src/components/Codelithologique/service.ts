import * as Joi from 'joi';
import CodelithologiqueModel, { ICodelithologiqueModel } from './model'; 
import { ICodelithologiqueService } from './interface';

/**
 * @export
 * @implements {ICodelithologiqueService}
 */
const CodelithologiqueService: ICodelithologiqueService = {

    /**
     * @param {ICodelithologiqueModel} Codelithologique
     * @returns {Promise < ICodelithologiqueModel >}
     * @memberof CodelithologiqueService
     */
    async insert(body: ICodelithologiqueModel): Promise<ICodelithologiqueModel> {
        try { 
            const Codelithologique: ICodelithologiqueModel = await CodelithologiqueModel.create(body);

            return Codelithologique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {ICodelithologiqueModel} Codelithologique
     * @returns {Promise < ICodelithologiqueModel >}
     * @memberof CodelithologiqueService
     */
    async update(body: ICodelithologiqueModel): Promise<ICodelithologiqueModel> {
        try { 
            const Codelithologique: ICodelithologiqueModel = await CodelithologiqueModel.update(body);

            return Codelithologique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ICodelithologiqueModel >}
     * @memberof CodelithologiqueService
     */
    async findOne(id: string): Promise<ICodelithologiqueModel> {
        try {
            return await CodelithologiqueModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },


    /**
     * @param {string} code
     * @returns {Promise < ICodelithologiqueModel >}
     * @memberof CodelithologiqueService
     */
    async findOneByCode(code: string): Promise<ICodelithologiqueModel> {
        try {
            return await CodelithologiqueModel.findOne({
                code: code
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < ICodelithologiqueModel[] >}
     * @memberof CodelithologiqueService
     */
    async findAll(): Promise<ICodelithologiqueModel[]> {
        try {
            return await CodelithologiqueModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ICodelithologiqueModel >}
     * @memberof CodelithologiqueService
     */
    async remove(id: string): Promise<ICodelithologiqueModel> {
        try {
            return await CodelithologiqueModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {any} data
     * @returns {Promise < ICodelithologiqueModel >}
     * @memberof CodelithologiqueService
     */
    async upload(data: any): Promise<ICodelithologiqueModel> {
        try {
            return await CodelithologiqueModel.upload(data);
        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default CodelithologiqueService;
