import * as Joi from 'joi';
import EnqueteModel, { IEnqueteModel } from './model';
import EnqueteValidation from './validation';
import { IEnqueteService } from './interface';

/**
 * @export
 * @implements {IEnqueteService}
 */
const EnqueteService: IEnqueteService = {

    /**
     * @param {IEnqueteModel} Enquete
     * @returns {Promise < IEnqueteModel >}
     * @memberof EnqueteService
     */
    async insert(body: IEnqueteModel): Promise<IEnqueteModel> {
        try { 
            // throw new Error(validate.error.message);
            const Enquete: IEnqueteModel = await EnqueteModel.create(body);

            return Enquete;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IEnqueteModel} Enquete
     * @returns {Promise < IEnqueteModel >}
     * @memberof EnqueteService
     */
    async update(body: IEnqueteModel): Promise<IEnqueteModel> {
        try { 
            const Enquete: IEnqueteModel = await EnqueteModel.update(body);

            return Enquete;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IEnqueteModel} Enquete
     * @returns {Promise < IEnqueteModel >}
     * @memberof EnqueteService
     */
    async updateStatus(body: IEnqueteModel): Promise<IEnqueteModel> {
        try { 
            const Enquete: IEnqueteModel = await EnqueteModel.update(body);
            if(body.status === 'validated' || body.status === 'active'){
                await EnqueteModel.updateCollecteStatus(body);
            }
            return Enquete;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IEnqueteModel >}
     * @memberof EnqueteService
     */
    async findOne(id: string): Promise<IEnqueteModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = EnqueteValidation.getEnquete({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await EnqueteModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < IEnqueteModel[] >}
     * @memberof EnqueteService
     */
    async findAll(): Promise<IEnqueteModel[]> {
        try {
            return await EnqueteModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IEnqueteModel >}
     * @memberof EnqueteService
     */
    async remove(id: string): Promise<IEnqueteModel> {
        try { 
            return await EnqueteModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default EnqueteService;
