import * as Joi from 'joi';
import Validation from '../validation';
import { IEnqueteModel } from './model';

/**
 * Validation des données des enquêtes avant enregistrement dans la BD
 * @export
 * @class EnqueteValidation
 * @extends Validation
 */
class EnqueteValidation extends Validation {

    /**
     * Creates an instance of EnqueteValidation.
     * @memberof EnqueteValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {IEnqueteModel} params
     * @returns {Joi.ValidationResult<IEnqueteModel >}
     * @memberof EnqueteValidation
     */
    createEnquete(
        params: IEnqueteModel
    ): Joi.ValidationResult<IEnqueteModel> {
        const schema: Joi.Schema = Joi.object().keys({
            code: Joi.string().required(),
            debut: Joi.string(),
            fin: Joi.string(),
            status: Joi.string()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {IEnqueteModel} params
     * @returns {Joi.ValidationResult<IEnqueteModel >}
     * @memberof EnqueteValidation
     */
    updateEnquete(
        params: IEnqueteModel
    ): Joi.ValidationResult<IEnqueteModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string(),
            code: Joi.string().required(),
            titre: Joi.string().required(),
            email: Joi.string(),
            telephone: Joi.string(),
            adresse: Joi.string(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof EnqueteValidation
     */
    getEnquete(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof EnqueteValidation
     */
    removeEnquete(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

}

export default new EnqueteValidation();
