import EnqueteService from './service';
import { HttpError } from '../../config/error';
import { IEnqueteModel } from './model';
import { NextFunction, Request, Response } from 'express';

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Enquete: IEnqueteModel = await EnqueteService.insert(req.body);

        res.status(201).json(Enquete);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Enquete: IEnqueteModel = await EnqueteService.update(req.body);

        res.status(200).json(Enquete);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function updateStatus(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Enquete: IEnqueteModel = await EnqueteService.updateStatus(req.body);
        
        res.status(200).json(Enquete);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Enquete: IEnqueteModel = await EnqueteService.findOne(req.params.id);

        res.status(200).json(Enquete);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Enquetes: IEnqueteModel[] = await EnqueteService.findAll();

        res.status(200).json(Enquetes);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Enquete: IEnqueteModel = await EnqueteService.remove(req.params.id);

        res.status(200).json(Enquete);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
