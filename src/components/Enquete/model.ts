import knex  from '../../config/connection/database';

/**
 * @export
 * @interface IEnqueteModel
 */
export interface IEnqueteModel {
    id?: number; 
    status?: string;
    type_enquete?: string;
}
  
const tableName:string = 'enquete';

class EnqueteModel{ 
    async create(objet:IEnqueteModel): Promise<IEnqueteModel> { 
        const id:number[] = await knex(tableName).insert(objet); 

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IEnqueteModel> {  
        return await knex.select().from(tableName).where(objet).first(); 
    }

    async findAll(): Promise<[IEnqueteModel]> {
        return await knex.select().from(tableName).orderBy('code');
    }

    async findOneAndRemove(id: any): Promise<any> { 
        return await knex(tableName).where('id', id).del();
    }

    async update(Enquete:IEnqueteModel): Promise<IEnqueteModel> {
        return await knex(tableName).where({ id: Enquete.id }).update(Enquete);
    }

    async updateCollecteStatus(Enquete:IEnqueteModel): Promise<IEnqueteModel> {
        const data = {
            status: Enquete.status
        } 
        switch(Enquete.type_enquete){
            case 'analyse-physico-chimique':  
                return knex('analyse_chimique').where({ enquete_id: Enquete.id }).update(data);
            case 'analyse-bacteriologique':
                return knex('analyse_bacteriologie').where({ enquete_id: Enquete.id }).update(data);
            case 'analyse-pesticide':
                return knex('analyse_pesticide').where({ enquete_id: Enquete.id }).update(data);
            case 'analyse-isotope':
                return knex('analyse_isotope').where({ enquete_id: Enquete.id }).update(data);
            case 'mesure-piezometrique':
                return knex('piezometrie').where({ enquete_id: Enquete.id }).update(data);
            case 'mesure-pluviometrique':
                return knex('pluviometrie').where({ enquete_id: Enquete.id }).update(data);
            case 'mesure-hydrometrique':
                return knex('hydrometrie').where({ enquete_id: Enquete.id }).update(data); 
        } 
    }
}

export default new EnqueteModel();
