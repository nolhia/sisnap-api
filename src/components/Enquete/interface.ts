import { IEnqueteModel } from './model';

/**
 * @export
 * @interface IEnqueteService
 */
export interface IEnqueteService {

    /**
     * @param {IEnqueteModel} IEnqueteModel
     * @returns {Promise<IEnqueteModel>}
     * @memberof IEnqueteService
     */
    insert(IEnqueteModel: IEnqueteModel): Promise<IEnqueteModel>;

    /**
     * @param {IEnqueteModel} IEnqueteModel
     * @returns {Promise<IEnqueteModel>}
     * @memberof IEnqueteService
     */
    update(IEnqueteModel: IEnqueteModel): Promise<IEnqueteModel>;

    /**
     * @param {IEnqueteModel} IEnqueteModel
     * @returns {Promise<IEnqueteModel>}
     * @memberof IEnqueteService
     */
    updateStatus(IEnqueteModel: IEnqueteModel): Promise<IEnqueteModel>;

    /**
     * @param {string} id
     * @returns {Promise<IEnqueteModel>}
     * @memberof IEnqueteService
     */
    findOne(id: string): Promise<IEnqueteModel>;

    /**
     * @returns {Promise<IEnqueteModel[]>}
     * @memberof IEnqueteService
     */
    findAll(): Promise<IEnqueteModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<IEnqueteModel>}
     * @memberof IEnqueteService
     */
    remove(id: string): Promise<any>;

}
