import knex from '../../config/connection/database';

/**
 * @export
 * @interface IAnalyseisotopeModel
 */
export interface IAnalyseisotopeModel {
    id?: number;
    //element_chimique?: any;
}

const tableName: string = 'analyse_isotope';

class AnalyseisotopeModel {
    async create(objet: IAnalyseisotopeModel): Promise<IAnalyseisotopeModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IAnalyseisotopeModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[IAnalyseisotopeModel]> {
        return await knex.select('analyse_isotope.*', 'laboratoire.titre as laboratoire_titre')
            .from(tableName)
            .leftJoin('laboratoire', 'analyse_isotope.laboratoire', 'laboratoire.id');
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id).del();
    }

    async update(Analyseisotope: IAnalyseisotopeModel): Promise<IAnalyseisotopeModel> {
        return await knex(tableName).where({ id: Analyseisotope.id }).update(Analyseisotope);
    }

    async upload(data: any): Promise<any> {
        return await knex(tableName).insert(data);
    }

    async export(): Promise<[any]> {
        return await knex(tableName)
            .join('station', 'station.id', '=', tableName + '.station')
            .join('laboratoire', 'laboratoire.id', '=', tableName + '.laboratoire')
            .select(
                tableName + '.date_prelevement',
                tableName + '.heure_prelevement',
                tableName + '.date_analyse',
                tableName + '.heure_analyse',
                tableName + '.chimiste',
                tableName + '.element_chimique',
                tableName + '.note',
                'laboratoire.code as laboratoire',
                'station.code as station',
                tableName + '.longitude',
                tableName + '.latitude',
                tableName + '.altitude',
                tableName + '.nom_localite as nom localite',
                tableName + '.status',
            );
    }
}

export default new AnalyseisotopeModel();
