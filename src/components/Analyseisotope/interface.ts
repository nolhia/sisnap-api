import { IAnalyseisotopeModel } from './model';

/**
 * @export
 * @interface IAnalyseisotopeService
 */
export interface IAnalyseisotopeService {

    /**
     * @param {IAnalyseisotopeModel} IAnalyseisotopeModel
     * @returns {Promise<IAnalyseisotopeModel>}
     * @memberof IAnalyseisotopeService
     */
    insert(IAnalyseisotopeModel: IAnalyseisotopeModel): Promise<IAnalyseisotopeModel>;

    /**
     * @param {IAnalyseisotopeModel} IAnalyseisotopeModel
     * @returns {Promise<IAnalyseisotopeModel>}
     * @memberof IAnalyseisotopeService
     */
    update(IAnalyseisotopeModel: IAnalyseisotopeModel): Promise<IAnalyseisotopeModel>;

    /**
     * @param {string} id
     * @returns {Promise<IAnalyseisotopeModel>}
     * @memberof IAnalyseisotopeService
     */
    findOne(id: string): Promise<IAnalyseisotopeModel>;

    /**
     * @returns {Promise<IAnalyseisotopeModel[]>}
     * @memberof IAnalyseisotopeService
     */
    findAll(): Promise<IAnalyseisotopeModel[]>;

    /**
     * @returns {Promise<any[]>}
     * @memberof IAnalyseisotopeService
     */
    export(): Promise<any[]>;

    /**
     * @param {string} id
     * @returns {Promise<IAnalyseisotopeModel>}
     * @memberof IAnalyseisotopeService
     */
    remove(id: string): Promise<any>;

    /**
     * @param {any} data
     * @returns {Promise<any>}
     * @memberof IAnalyseisotopeService
     */
    upload(data: any): Promise<any>;

}
