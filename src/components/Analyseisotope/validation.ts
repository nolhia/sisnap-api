import * as Joi from 'joi';
import Validation from '../validation';
import { IAnalyseisotopeModel } from './model';

/**
 * Validation des données des analyses isotopes avant enregistrement dans la BD
 * @export
 * @class AnalyseisotopeValidation
 * @extends Validation
 */
class AnalyseisotopeValidation extends Validation {

    /**
     * Creates an instance of AnalyseisotopeValidation.
     * @memberof AnalyseisotopeValidation
     */
    constructor() {
        super();
    }

    /**
 * @param {IAnalyseisotopeModel} params
 * @returns {Joi.ValidationResult<IAnalyseisotopeModel >}
 * @memberof AnalyseisotopeValidation
 */
    createAnalyseisotope(
        params: IAnalyseisotopeModel
    ): Joi.ValidationResult<IAnalyseisotopeModel> {
        const schema: Joi.Schema = Joi.object().keys({
            date_prelevement: Joi.string().allow(null).optional(),
            heure_prelevement: Joi.string().allow(null).optional(),
            date_analyse: Joi.string().required(),
            heure_analyse: Joi.string().allow(null).optional(),
            element_chimique: Joi.any().allow('').optional(),
            note: Joi.string().allow('').optional(),
            laboratoire: Joi.string().required(),
            chimiste: Joi.string().allow('').optional(),
            station: Joi.string().allow('').optional(),
            status: Joi.string().allow('').optional(),
            longitude: Joi.number().allow(null).optional(),
            latitude: Joi.number().allow(null).optional(),
            altitude: Joi.number().allow(null).optional(),
            nom_localite: Joi.string().allow('').optional(),
            ajouter_par: Joi.string().optional(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {IAnalyseisotopeModel} params
     * @returns {Joi.ValidationResult<IAnalyseisotopeModel >}
     * @memberof AnalyseisotopeValidation
     */
    updateAnalyseisotope(
        params: IAnalyseisotopeModel
    ): Joi.ValidationResult<IAnalyseisotopeModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string().required(),
            date_prelevement: Joi.string().allow(null).optional(),
            heure_prelevement: Joi.string().allow(null).optional(),
            date_analyse: Joi.string().required(),
            heure_analyse: Joi.string().allow(null).optional(),
            element_chimique: Joi.any().optional(),
            note: Joi.string().allow('').optional(),
            laboratoire: Joi.string().required(),
            chimiste: Joi.string().allow('').optional(),
            station: Joi.string().allow('').optional(),
            status: Joi.string().allow('').optional(),
            longitude: Joi.number().allow(null).optional(),
            latitude: Joi.number().allow(null).optional(),
            altitude: Joi.number().allow(null).optional(),
            nom_localite: Joi.string().allow('').optional(),
            modifier_par: Joi.string().optional(),
            modifier_le: Joi.string().optional(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof AnalyseisotopeValidation
     */
    getAnalyseisotope(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof AnalyseisotopeValidation
     */
    removeAnalyseisotope(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

}

export default new AnalyseisotopeValidation();
