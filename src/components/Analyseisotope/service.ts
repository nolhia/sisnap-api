import * as Joi from 'joi';
import AnalyseisotopeModel, { IAnalyseisotopeModel } from './model';
import AnalyseisotopeValidation from './validation';
import { IAnalyseisotopeService } from './interface';

/**
 * @export
 * @implements {IAnalyseisotopeService}
 */
const AnalyseisotopeService: IAnalyseisotopeService = {
    /**
     * @param {IAnalyseisotopeModel} Analyseisotope
     * @returns {Promise < IAnalyseisotopeModel >}
     * @memberof AnalyseisotopeService
     */
    async insert(body: IAnalyseisotopeModel): Promise<IAnalyseisotopeModel> {
        try {
            const validate: Joi.ValidationResult<IAnalyseisotopeModel> = AnalyseisotopeValidation.createAnalyseisotope(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const Analyseisotope: IAnalyseisotopeModel = await AnalyseisotopeModel.create(body);

            return Analyseisotope;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IAnalyseisotopeModel} Analyseisotope
     * @returns {Promise < IAnalyseisotopeModel >}
     * @memberof AnalyseisotopeService
     */
    async update(body: IAnalyseisotopeModel): Promise<IAnalyseisotopeModel> {
        try {
            const validate: Joi.ValidationResult<IAnalyseisotopeModel> = AnalyseisotopeValidation.updateAnalyseisotope(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const Analyseisotope: IAnalyseisotopeModel = await AnalyseisotopeModel.update(body);

            return Analyseisotope;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IAnalyseisotopeModel >}
     * @memberof AnalyseisotopeService
     */
    async findOne(id: string): Promise<IAnalyseisotopeModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = AnalyseisotopeValidation.getAnalyseisotope({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await AnalyseisotopeModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < IAnalyseisotopeModel[] >}
     * @memberof AnalyseisotopeService
     */
    async findAll(): Promise<IAnalyseisotopeModel[]> {
        try {
            return await AnalyseisotopeModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < any[] >}
     * @memberof AnalyseisotopeService
     */
    async export(): Promise<any[]> {
        try {
            const data: any[] = await AnalyseisotopeModel.export();
            data.map((element) => {
                element.element_chimique = JSON.stringify(element.element_chimique[0])
                    .replace('{', '')
                    .replace('}', '')
                    .replace(/,/g, ' | ');
            });

            return data;

        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IAnalyseisotopeModel >}
     * @memberof AnalyseisotopeService
     */
    async remove(id: string): Promise<IAnalyseisotopeModel> {
        try {
            return await AnalyseisotopeModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {any} data
     * @returns {Promise < IAnalyseisotopeModel >}
     * @memberof AnalyseisotopeService
     */
    async upload(data: any): Promise<IAnalyseisotopeModel> {
        try {
            return await AnalyseisotopeModel.upload(data);
        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default AnalyseisotopeService;
