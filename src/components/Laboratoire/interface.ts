import { ILaboratoireModel } from './model';

/**
 * @export
 * @interface ILaboratoireService
 */
export interface ILaboratoireService {

    /**
     * @param {ILaboratoireModel} ILaboratoireModel
     * @returns {Promise<ILaboratoireModel>}
     * @memberof ILaboratoireService
     */
    insert(ILaboratoireModel: ILaboratoireModel): Promise<ILaboratoireModel>;

    /**
     * @param {ILaboratoireModel} ILaboratoireModel
     * @returns {Promise<ILaboratoireModel>}
     * @memberof ILaboratoireService
     */
    update(ILaboratoireModel: ILaboratoireModel): Promise<ILaboratoireModel>;

    /**
     * @param {string} id
     * @returns {Promise<ILaboratoireModel>}
     * @memberof ILaboratoireService
     */
    findOne(id: string): Promise<ILaboratoireModel>;

    /**
     * @param {string} code
     * @returns {Promise<ILaboratoireModel>}
     * @memberof ILaboratoireService
     */
    findOneByCode(code: string): Promise<ILaboratoireModel>;

    /**
     * @returns {Promise<ILaboratoireModel[]>}
     * @memberof ILaboratoireService
     */
    findAll(): Promise<ILaboratoireModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<ILaboratoireModel>}
     * @memberof ILaboratoireService
     */
    remove(id: string): Promise<any>;

}
