import * as Joi from 'joi';
import LaboratoireModel, { ILaboratoireModel } from './model';
import LaboratoireValidation from './validation';
import { ILaboratoireService } from './interface';

/**
 * @export
 * @implements {ILaboratoireService}
 */
const LaboratoireService: ILaboratoireService = {

    /**
     * @param {ILaboratoireModel} Laboratoire
     * @returns {Promise < ILaboratoireModel >}
     * @memberof LaboratoireService
     */
    async insert(body: ILaboratoireModel): Promise<ILaboratoireModel> {
        try { 
            const Laboratoire: ILaboratoireModel = await LaboratoireModel.create(body);

            return Laboratoire;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {ILaboratoireModel} Laboratoire
     * @returns {Promise < ILaboratoireModel >}
     * @memberof LaboratoireService
     */
    async update(body: ILaboratoireModel): Promise<ILaboratoireModel> {
        try { 
            const Laboratoire: ILaboratoireModel = await LaboratoireModel.update(body);

            return Laboratoire;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ILaboratoireModel >}
     * @memberof LaboratoireService
     */
    async findOne(id: string): Promise<ILaboratoireModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = LaboratoireValidation.getLaboratoire({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await LaboratoireModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} code
     * @returns {Promise < ILaboratoireModel >}
     * @memberof LaboratoireService
     */
    async findOneByCode(code: string): Promise<ILaboratoireModel> {
        try {
            const validate: Joi.ValidationResult<{
                code: string
            }> = LaboratoireValidation.getLaboratoireByCode({
                code
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await LaboratoireModel.findOne({
                code: code
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < ILaboratoireModel[] >}
     * @memberof LaboratoireService
     */
    async findAll(): Promise<ILaboratoireModel[]> {
        try {
            return await LaboratoireModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ILaboratoireModel >}
     * @memberof LaboratoireService
     */
    async remove(id: string): Promise<ILaboratoireModel> {
        try {
            return await LaboratoireModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default LaboratoireService;
