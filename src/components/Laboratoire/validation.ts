import * as Joi from 'joi';
import Validation from '../validation';
import { ILaboratoireModel } from './model';

/**
 * Validation des données des laboratoires avant enregistrement dans la BD
 * @export
 * @class LaboratoireValidation
 * @extends Validation
 */
class LaboratoireValidation extends Validation {

    /**
     * Creates an instance of LaboratoireValidation.
     * @memberof LaboratoireValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {ILaboratoireModel} params
     * @returns {Joi.ValidationResult<ILaboratoireModel >}
     * @memberof LaboratoireValidation
     */
    createLaboratoire(
        params: ILaboratoireModel
    ): Joi.ValidationResult<ILaboratoireModel> {
        const schema: Joi.Schema = Joi.object().keys({
            code: Joi.string().required(),
            titre: Joi.string().required(),
            email: Joi.string(),
            telephone: Joi.string(),
            adresse: Joi.string(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {ILaboratoireModel} params
     * @returns {Joi.ValidationResult<ILaboratoireModel >}
     * @memberof LaboratoireValidation
     */
    updateLaboratoire(
        params: ILaboratoireModel
    ): Joi.ValidationResult<ILaboratoireModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string(),
            code: Joi.string().required(),
            titre: Joi.string().required(),
            email: Joi.string(),
            telephone: Joi.string(),
            adresse: Joi.string(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof LaboratoireValidation
     */
    getLaboratoire(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ code: string }} body
     * @returns {Joi.ValidationResult<{ code: string }>}
     * @memberof LaboratoireValidation
     */
    getLaboratoireByCode(
        body: {
            code: string
        }
    ): Joi.ValidationResult<{
        code: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            code: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof LaboratoireValidation
     */
    removeLaboratoire(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

}

export default new LaboratoireValidation();
