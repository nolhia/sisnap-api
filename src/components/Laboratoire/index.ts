import LaboratoireService from './service';
import { HttpError } from '../../config/error';
import { ILaboratoireModel } from './model';
import { NextFunction, Request, Response } from 'express';

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Laboratoire: ILaboratoireModel = await LaboratoireService.insert(req.body);

        res.status(201).json(Laboratoire);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Laboratoire: ILaboratoireModel = await LaboratoireService.update(req.body);

        res.status(200).json(Laboratoire);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Laboratoire: ILaboratoireModel = await LaboratoireService.findOne(req.params.id);

        res.status(200).json(Laboratoire);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Laboratoires: ILaboratoireModel[] = await LaboratoireService.findAll();

        res.status(200).json(Laboratoires);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Laboratoire: ILaboratoireModel = await LaboratoireService.remove(req.params.id);

        res.status(200).json(Laboratoire);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
