import knex from '../../config/connection/database';

/**
 * @export
 * @interface ILaboratoireModel
 */
export interface ILaboratoireModel {
    id?: number;
}

const tableName: string = 'laboratoire';

class LaboratoireModel {
    async create(objet: ILaboratoireModel): Promise<ILaboratoireModel> {
        const id: number[] = await knex(tableName).insert(objet, 'id');

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<ILaboratoireModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[ILaboratoireModel]> {
        return await knex.select().from(tableName).orderBy('code');
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id).del();
    }

    async update(laboratoire: ILaboratoireModel): Promise<ILaboratoireModel> {
        return await knex(tableName).where({ id: laboratoire.id }).update(laboratoire);
    }
}

export default new LaboratoireModel();
