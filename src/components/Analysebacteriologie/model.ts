import knex from '../../config/connection/database';

/**
 * @export
 * @interface IAnalysebacteriologieModel
 */
export interface IAnalysebacteriologieModel {
    id?: number;
}

const tableName: string = 'analyse_bacteriologie';

class AnalysebacteriologieModel {
    async create(objet: IAnalysebacteriologieModel): Promise<IAnalysebacteriologieModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IAnalysebacteriologieModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[IAnalysebacteriologieModel]> {
        return await knex.select('analyse_bacteriologie.*', 'laboratoire.titre as laboratoire_titre')
            .from(tableName)
            .leftJoin('laboratoire', 'analyse_bacteriologie.laboratoire', 'laboratoire.id');

    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id).del();
    }

    async update(Analysebacteriologie: IAnalysebacteriologieModel): Promise<IAnalysebacteriologieModel> {

        return await knex(tableName).where({ id: Analysebacteriologie.id }).update(Analysebacteriologie);
    }

    async upload(data: any): Promise<any> {
        return await knex(tableName).insert(data);
    }

    async export(): Promise<[any]> {
        return await knex(tableName)
            .join('station', 'station.id', '=', tableName + '.station')
            .join('laboratoire', 'laboratoire.id', '=', tableName + '.laboratoire')
            .select(
                tableName + '.date_prelevement',
                tableName + '.heure_prelevement',
                tableName + '.date_analyse',
                tableName + '.heure_debut_analyse',
                tableName + '.heure_fin_analyse',
                tableName + '.chimiste',
                tableName + '.coliforme_totaux',
                tableName + '.coliforme_fecaux',
                tableName + '.element_chimique',
                tableName + '.note',
                'laboratoire.code as laboratoire',
                'station.code as station',
                tableName + '.longitude',
                tableName + '.latitude',
                tableName + '.altitude',
                tableName + '.nom_localite as nom localite',
                tableName + '.status',
            );
    }

    async report(id: any): Promise<[any]> {
        return await knex(tableName)
            .join('station', 'station.id', '=', tableName + '.station')
            .join('laboratoire', 'laboratoire.id', '=', tableName + '.laboratoire')
            .select(
                tableName + '.date_prelevement',
                tableName + '.heure_prelevement',
                tableName + '.date_analyse',
                tableName + '.heure_debut_analyse',
                tableName + '.heure_fin_analyse',
                tableName + '.chimiste',
                tableName + '.coliforme_totaux',
                tableName + '.coliforme_fecaux',
                tableName + '.element_chimique',
                tableName + '.note',
                'laboratoire.titre as laboratoire',
                'station.code as station',
                tableName + '.longitude',
                tableName + '.latitude',
                tableName + '.altitude',
                tableName + '.nom_localite as nom localite',
                tableName + '.status',
            )
            .where(tableName + '.id', id).first();
    }
}

export default new AnalysebacteriologieModel();
