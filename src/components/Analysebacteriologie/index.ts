import AnalysebacteriologieService from './service';
import LaboratoireService from '../Laboratoire/service';
import StationService from '../Station/service';
import ElementchimiqueService from '../Elementchimique/service';
import { HttpError } from '../../config/error';
import { IAnalysebacteriologieModel } from './model';
import { NextFunction, Request, Response } from 'express';
import UtilsHelper from '../../helpers/UtilsHelper';
const fs: any = require('fs');

/**
 * Créer une nouvelle analyse bactériologique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const analysebacteriologie: IAnalysebacteriologieModel = await AnalysebacteriologieService.insert(req.body);

        res.status(201).json(analysebacteriologie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Mettre à jour une analyse bacteriologique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const analysebacteriologie: IAnalysebacteriologieModel = await AnalysebacteriologieService.update(req.body);

        res.status(200).json(analysebacteriologie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Rechercher une analyse bacteriologique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const analysebacteriologie: IAnalysebacteriologieModel = await AnalysebacteriologieService.findOne(req.params.id);

        res.status(200).json(analysebacteriologie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer la liste de toutes les analyses bactériologiques
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const analysebacteriologies: IAnalysebacteriologieModel[] = await AnalysebacteriologieService.findAll();

        res.status(200).json(analysebacteriologies);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Supprimer une analyse bactériologique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const analysebacteriologies: IAnalysebacteriologieModel = await AnalysebacteriologieService.remove(req.params.id);

        res.status(200).json(analysebacteriologies);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Traiter un fichier CSV contenant une liste d'analyses bactériologiques sous un format prédéfinis
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function upload(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const exceltojson: any = await UtilsHelper.convertExcelToJson(req);

        await exceltojson(
            {
                input: req.file.path,
                output: null,
                lowerCaseHeaders: true
            },
            async (err: any, result: any) => {
                if (err) {
                    res.status(400).json({ error_code: 1, err_desc: err, data: null });
                }
                // supprimer le fichier 
                fs.unlinkSync(req.file.path);

                // pour ligne de données on verifie si le labortoire ou la station existe
                // 1 - Si oui on recupère leur id
                // 2 - Si non on les insères dans leur table respective et on récupère l'id de l'enregistrement
                try {
                    const promisedAnalysebacteriologiques: any = result.map(async (element: any) => {
                        const laboratoireCode: any = element.laboratoire;
                        const stationCode: any = element.station;
                        element.status = 'pending';

                        try {
                            // Traitement des laboratoires
                            const laboratoire: any = await LaboratoireService.findOneByCode(laboratoireCode);

                            if (laboratoire && laboratoire.id) {
                                element.laboratoire = laboratoire.id;
                            } else {
                                console.log('le Laboratoire ' + laboratoireCode + ' n\'existe pas pour l\'instant, il sera crée');
                                const laboratoireData: any = {
                                    'code': laboratoireCode,
                                    'titre': laboratoireCode,
                                };
                                const nouveauLaboratoire: any = await LaboratoireService.insert(laboratoireData);

                                if (nouveauLaboratoire && nouveauLaboratoire.id) {
                                    element.laboratoire = nouveauLaboratoire.id;
                                }

                            }
                            // Traitement des stations
                            const station: any = await StationService.findOneByCode(stationCode);
                            if (station && station.id) {
                                element.station = station.id;
                            } else {
                                console.log('la station ' + stationCode + ' n\'existe pas pour l\'instant, elle sera créee');
                                const stationData: any = {
                                    'code': stationCode,
                                    'titre': stationCode,
                                    'latitude': 1,
                                    'longitude': 1,
                                    'altitude': 1,
                                    'type_station_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                    'type_ouvrage_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                    'nappe_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                };
                                const nouvelleStation: any = await StationService.insert(stationData);
                                if (nouvelleStation && nouvelleStation.id) {
                                    element.station = nouvelleStation.id;
                                }

                            }
                            // Traitement des elements chimiques
                            // 1 - Nous allons d'abord vérifier si chaque élément chimique existe. sil y en a qui n'existe pas nous les créons
                            const elementChimiqueJson: any = JSON.parse('{' + element.element_chimique + '}');
                            const elementChmiquesCodeList: any = Object.keys(elementChimiqueJson);
                            elementChmiquesCodeList.map(async (code: any) => {
                                // on verifie si l'élément chimique existe
                                const elementChimique: any = await ElementchimiqueService.findOneByCode(code);

                                // Si l'élément chimique existe déjà mais n'est pas ajouté a la liste d'éléments d'analyse bacteriologique, on le fait
                                if (elementChimique) {
                                    if (elementChimique.bacteriologie.toString() === 'false') {
                                        elementChimique.bacteriologie = 'true';
                                        await ElementchimiqueService.update(elementChimique);
                                    }
                                } else { // Sinon on crée l'élément chimique avec le code
                                    console.log('element chimique avec le code ' + code + ' sera crée');
                                    const nouveauElementChimique: any = {
                                        'code': code,
                                        'titre': code,
                                        'analyse_chimique': 'false',
                                        'bacteriologie': 'true',
                                        'isotopie': 'false',
                                        'pesticide': 'false'
                                    };

                                    await ElementchimiqueService.insert(nouveauElementChimique);
                                }
                            });

                            element.element_chimique = [elementChimiqueJson];

                            if (element.heure_prelevement === '') {
                                element.heure_prelevement = null;
                            }
                            if (element.heure_debut_analyse === '') {
                                element.heure_debut_analyse = null;
                            }
                            if (element.heure_fin_analyse === '') {
                                element.heure_fin_analyse = null;
                            }

                            if (element.coliforme_totaux === '') {
                                element.coliforme_totaux = 0;
                            }

                            if (element.coliforme_fecaux === '') {
                                element.coliforme_fecaux = 0;
                            }

                            return element;

                        } catch (error) {
                            console.log(error.message);
                            res.status(500).json(error.message);
                        }

                    });

                    const analysebacteriologiques: any = await Promise.all(promisedAnalysebacteriologiques);

                    await AnalysebacteriologieService.upload(result);
                    res.status(200).json(analysebacteriologiques);
                } catch (er) {
                    console.log(er);
                    res.status(400).json('Une erreure inatendue s\'est produite durant l\'opération. Veuillez vérifier votre fichier SVP.');
                }

            }
        );

    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer la liste des analyses bactériologiques à exporter sous un format csv
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function exportData(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const data: any[] = await AnalysebacteriologieService.export();

        res.status(200).json(data);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Trouver une analyse bacteriologique
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function report(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const analysebacteriologie: IAnalysebacteriologieModel = await AnalysebacteriologieService.report(req.params.id);

        res.status(200).json(analysebacteriologie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
