import { IAnalysebacteriologieModel } from './model';

/**
 * @export
 * @interface IAnalysebacteriologieService
 */
export interface IAnalysebacteriologieService {

    /**
     * @param {IAnalysebacteriologieModel} IAnalysebacteriologieModel
     * @returns {Promise<IAnalysebacteriologieModel>}
     * @memberof IAnalysebacteriologieService
     */
    insert(IAnalysebacteriologieModel: IAnalysebacteriologieModel): Promise<IAnalysebacteriologieModel>;

    /**
     * @param {IAnalysebacteriologieModel} IAnalysebacteriologieModel
     * @returns {Promise<IAnalysebacteriologieModel>}
     * @memberof IAnalysebacteriologieService
     */
    update(IAnalysebacteriologieModel: IAnalysebacteriologieModel): Promise<IAnalysebacteriologieModel>;

    /**
     * @param {string} id
     * @returns {Promise<IAnalysebacteriologieModel>}
     * @memberof IAnalysebacteriologieService
     */
    findOne(id: string): Promise<IAnalysebacteriologieModel>;

    /**
     * @returns {Promise<IAnalysebacteriologieModel[]>}
     * @memberof IAnalysebacteriologieService
     */
    findAll(): Promise<IAnalysebacteriologieModel[]>;

    /**
     * @returns {Promise<any[]>}
     * @memberof IAnalysebacteriologieService
     */
    export(): Promise<any[]>;

    /**
     * @param {string} id
     * @returns {Promise<IAnalysebacteriologieModel>}
     * @memberof IAnalysebacteriologieService
     */
    remove(id: string): Promise<any>;

    /**
     * @param {any} data
     * @returns {Promise<any>}
     * @memberof IAnalysebacteriologieService
     */
    upload(data: any): Promise<any>;

    /**
     * @returns {Promise<any[]>}
     * @memberof IAnalysebacteriologieService
     */
    report(id: string): Promise<any>;

}
