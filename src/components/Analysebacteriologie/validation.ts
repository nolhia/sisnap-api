import * as Joi from 'joi';
import Validation from '../validation';
import { IAnalysebacteriologieModel } from './model';
import { join } from 'bluebird';

/**
 * Validation des données des analyses bactériologiques avant enregistrement dans la BD
 * @export
 * @class AnalysebacteriologieValidation
 * @extends Validation
 */

class AnalysebacteriologieValidation extends Validation {
    /**
     * Creates an instance of AnalysebacteriologieValidation.
     * @memberof AnalysebacteriologieValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {IAnalysebacteriologieModel} params
     * @returns {Joi.ValidationResult<IAnalysebacteriologieModel >}
     * @memberof AnalysebacteriologieValidation
     */
    createAnalysebacteriologie(
        params: IAnalysebacteriologieModel
    ): Joi.ValidationResult<IAnalysebacteriologieModel> {
        const schema: Joi.Schema = Joi.object().keys({
            date_prelevement: Joi.string().allow(null).optional(),
            heure_prelevement: Joi.string().allow(null).optional(),
            date_analyse: Joi.string().required(),
            heure_debut_analyse: Joi.string().allow(null).optional(),
            heure_fin_analyse: Joi.string().allow(null).optional(),
            element_chimique: Joi.any().optional(),
            note: Joi.string().allow('').optional(),
            laboratoire: Joi.string().required(),
            chimiste: Joi.string().allow('').optional(),
            station: Joi.string().allow('').optional(),
            coliforme_totaux: Joi.number().required(),
            coliforme_fecaux: Joi.number().required(),
            status: Joi.string().allow('').optional(),
            longitude: Joi.number().allow(null).optional(),
            latitude: Joi.number().allow(null).optional(),
            altitude: Joi.number().allow(null).optional(),
            nom_localite: Joi.string().allow('').optional(),
            ajouter_par: Joi.string().optional(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {IAnalysebacteriologieModel} params
     * @returns {Joi.ValidationResult<IAnalysebacteriologieModel >}
     * @memberof AnalysebacteriologieValidation
     */
    updateAnalysebacteriologie(
        params: IAnalysebacteriologieModel
    ): Joi.ValidationResult<IAnalysebacteriologieModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string().required(),
            date_prelevement: Joi.string().allow(null).optional(),
            heure_prelevement: Joi.string().allow(null).optional(),
            date_analyse: Joi.string().required(),
            heure_debut_analyse: Joi.string().allow(null).optional(),
            heure_fin_analyse: Joi.string().allow(null).optional(),
            element_chimique: Joi.any().allow('').optional(),
            note: Joi.string().allow('').optional(),
            laboratoire: Joi.string().required(),
            chimiste: Joi.string().allow('').optional(),
            station: Joi.string().allow('').optional(),
            coliforme_totaux: Joi.number().required(),
            coliforme_fecaux: Joi.number().required(),
            status: Joi.string().allow('').optional(),
            longitude: Joi.number().allow(null).optional(),
            latitude: Joi.number().allow(null).optional(),
            altitude: Joi.number().allow(null).optional(),
            nom_localite: Joi.string().allow('').optional(),
            modifier_par: Joi.string().optional(),
            modifier_le: Joi.string().optional(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof AnalysebacteriologieValidation
     */
    getAnalysebacteriologie(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof AnalysebacteriologieValidation
     */
    removeAnalysebacteriologie(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

}

export default new AnalysebacteriologieValidation();
