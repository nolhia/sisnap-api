import * as Joi from 'joi';
import AnalysebacteriologieModel, { IAnalysebacteriologieModel } from './model';
import AnalysebacteriologieValidation from './validation';
import { IAnalysebacteriologieService } from './interface';

/**
 * @export
 * @implements {IAnalysebacteriologieService}
 */
const AnalysebacteriologieService: IAnalysebacteriologieService = {

    /**
     * @param {IAnalysebacteriologieModel} Analysebacteriologie
     * @returns {Promise < AnalysebacteriologieModel >}
     * @memberof AnalysebacteriologieService
     */
    async insert(body: IAnalysebacteriologieModel): Promise<IAnalysebacteriologieModel> {
        try {
            const validate: Joi.ValidationResult<IAnalysebacteriologieModel> = AnalysebacteriologieValidation.createAnalysebacteriologie(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const analysebacteriologie: IAnalysebacteriologieModel = await AnalysebacteriologieModel.create(body);

            return analysebacteriologie;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IAnalysebacteriologieModel} Analysebacteriologie
     * @returns {Promise < AnalysebacteriologieModel >}
     * @memberof AnalysebacteriologieService
     */
    async update(body: IAnalysebacteriologieModel): Promise<IAnalysebacteriologieModel> {
        try {
            const validate: Joi.ValidationResult<IAnalysebacteriologieModel> = AnalysebacteriologieValidation.updateAnalysebacteriologie(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const analysebacteriologie: IAnalysebacteriologieModel = await AnalysebacteriologieModel.update(body);

            return analysebacteriologie;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IAnalysebacteriologieModel >}
     * @memberof AnalysebacteriologieService
     */
    async findOne(id: string): Promise<IAnalysebacteriologieModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = AnalysebacteriologieValidation.getAnalysebacteriologie({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await AnalysebacteriologieModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < IAnalysebacteriologieModel[] >}
     * @memberof AnalysebacteriologieService
     */
    async findAll(): Promise<IAnalysebacteriologieModel[]> {
        try {
            return await AnalysebacteriologieModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IAnalysebacteriologieModel >}
     * @memberof AnalysebacteriologieService
     */
    async remove(id: string): Promise<IAnalysebacteriologieModel> {
        try {
            return await AnalysebacteriologieModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {any} data
     * @returns {Promise < IAnalysebacteriologieModel >}
     * @memberof AnalysebacteriologieService
     */
    async upload(data: any): Promise<IAnalysebacteriologieModel> {
        try {
            return await AnalysebacteriologieModel.upload(data);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < any[] >}
     * @memberof AnalysebacteriologieService
     */
    async export(): Promise<any[]> {
        try {
            const data: any[] = await AnalysebacteriologieModel.export();
            data.map((element) => {
                element.element_chimique = JSON.stringify(element.element_chimique[0])
                    .replace('{', '')
                    .replace('}', '')
                    .replace(/,/g, ' | ');
            });

            return data;

        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < any[] >}
     * @memberof AnalysebacteriologieService
     */
    async report(id: string): Promise<IAnalysebacteriologieModel> {
        try {
            const data: any = await AnalysebacteriologieModel.report(id);

            return data;

        } catch (error) {
            throw new Error(error.message);
        }
    },
};

export default AnalysebacteriologieService;
