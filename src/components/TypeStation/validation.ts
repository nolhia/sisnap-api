import * as Joi from 'joi';
import Validation from '../validation';
import { ITypeStationModel } from './model';

/**
 * Validation des données des Types stations avant enregistrement dans la BD
 * @export
 * @class TypeStationValidation
 * @extends Validation
 */
class TypeStationValidation extends Validation {

    /**
     * Creates an instance of TypeStationValidation.
     * @memberof TypeStationValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {ITypeStationModel} params
     * @returns {Joi.ValidationResult<ITypeStationModel >}
     * @memberof TypeStationValidation
     */
    createTypeStation(
        params: ITypeStationModel
    ): Joi.ValidationResult<ITypeStationModel> {
        const schema: Joi.Schema = Joi.object().keys({
            code: Joi.string().required(),
            titre: Joi.string().required()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {ITypeStationModel} params
     * @returns {Joi.ValidationResult<ITypeStationModel >}
     * @memberof TypeStationValidation
     */
    updateTypeStation(
        params: ITypeStationModel
    ): Joi.ValidationResult<ITypeStationModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string(),
            code: Joi.string().required(),
            titre: Joi.string().required()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof TypeStationValidation
     */
    getTypeStation(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ code: string }} body
     * @returns {Joi.ValidationResult<{ code: string }>}
     * @memberof TypeStationValidation
     */
    getTypeStationByCode(
        body: {
            code: string
        }
    ): Joi.ValidationResult<{
        code: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            code: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof TypeStationValidation
     */
    removeTypeStation(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

}

export default new TypeStationValidation();
