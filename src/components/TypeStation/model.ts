import knex from '../../config/connection/database';

/**
 * @export
 * @interface ITypeStationModel
 */
export interface ITypeStationModel {
    id?: number;
    code?: string;
    titre?: string;
}

export type AuthToken = {
    accessToken: string,
    kind: string
};

const tableName: string = 'type_station';

class TypeStationModel {
    async create(objet: ITypeStationModel): Promise<ITypeStationModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<ITypeStationModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[ITypeStationModel]> {
        return await knex.select().from(tableName).orderBy('code');
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where({ id }).del();
    }

    async update(typeStation: ITypeStationModel): Promise<ITypeStationModel> {
        return await knex(tableName).where({ id: typeStation.id }).update(typeStation);
    }
}

export default new TypeStationModel();
