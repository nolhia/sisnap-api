import * as Joi from 'joi';
import TypeStationModel, { ITypeStationModel } from './model';
import TypeStationValidation from './validation';
import { ITypeStationService } from './interface';

/**
 * @export
 * @implements {ITypeStationService}
 */
const TypeStationService: ITypeStationService = {

    /**
     * @param {ITypeStationModel} TypeStation
     * @returns {Promise < ITypeStationModel >}
     * @memberof TypeStationService
     */
    async insert(body: ITypeStationModel): Promise<ITypeStationModel> {
        try {
            const validate: Joi.ValidationResult<ITypeStationModel> = TypeStationValidation.createTypeStation(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const typeStation: ITypeStationModel = await TypeStationModel.create(body);

            return typeStation;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {ITypeStationModel} TypeStation
     * @returns {Promise < ITypeStationModel >}
     * @memberof TypeStationService
     */
    async update(body: ITypeStationModel): Promise<ITypeStationModel> {
        try {
            const validate: Joi.ValidationResult<ITypeStationModel> = TypeStationValidation.updateTypeStation(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const typeStation: ITypeStationModel = await TypeStationModel.update(body);

            return typeStation;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ITypeStationModel >}
     * @memberof TypeStationService
     */
    async findOne(id: string): Promise<ITypeStationModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = TypeStationValidation.getTypeStation({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await TypeStationModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },


    /**
     * @param {string} code
     * @returns {Promise < ITypeStationModel >}
     * @memberof TypeStationService
     */
    async findOneByCode(code: string): Promise<ITypeStationModel> {
        try {
            const validate: Joi.ValidationResult<{
                code: string
            }> = TypeStationValidation.getTypeStationByCode({
                code
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await TypeStationModel.findOne({
                code: code
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < ITypeStationModel[] >}
     * @memberof TypeStationService
     */
    async findAll(): Promise<ITypeStationModel[]> {
        try {
            return await TypeStationModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ITypeStationModel >}
     * @memberof TypeStationService
     */
    async remove(id: string): Promise<ITypeStationModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = TypeStationValidation.getTypeStation({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await TypeStationModel.findOneAndRemove({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default TypeStationService;
