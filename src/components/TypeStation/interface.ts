import { ITypeStationModel } from './model';

/**
 * @export
 * @interface ITypeStationService
 */
export interface ITypeStationService {

    /**
     * @param {ITypeStationModel} ITypeStationModel
     * @returns {Promise<ITypeStationModel>}
     * @memberof ITypeStationService
     */
    insert(ITypeStationModel: ITypeStationModel): Promise<ITypeStationModel>;

    /**
     * @param {ITypeStationModel} ITypeStationModel
     * @returns {Promise<ITypeStationModel>}
     * @memberof ITypeStationService
     */
    update(ITypeStationModel: ITypeStationModel): Promise<ITypeStationModel>;

    /**
     * @param {string} id
     * @returns {Promise<ITypeStationModel>}
     * @memberof ITypeStationService
     */
    findOne(id: string): Promise<ITypeStationModel>;

    /**
     * @param {string} code
     * @returns {Promise<ITypeStationModel>}
     * @memberof ITypeStationService
     */
    findOneByCode(code: string): Promise<ITypeStationModel>;

    /**
     * @returns {Promise<ITypeStationModel[]>}
     * @memberof ITypeStationService
     */
    findAll(): Promise<ITypeStationModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<ITypeStationModel>}
     * @memberof ITypeStationService
     */
    remove(id: string): Promise<any>;

}
