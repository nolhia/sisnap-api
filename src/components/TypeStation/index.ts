import TypeStationService from './service';
import { HttpError } from '../../config/error';
import { ITypeStationModel } from './model';
import { NextFunction, Request, Response } from 'express';

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const typeStation: ITypeStationModel = await TypeStationService.insert(req.body);

        res.status(201).json(typeStation);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const typeStation: ITypeStationModel = await TypeStationService.update(req.body);

        res.status(200).json(typeStation);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const typeStation: ITypeStationModel = await TypeStationService.findOne(req.params.id);

        res.status(200).json(typeStation);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const typeStations: ITypeStationModel[] = await TypeStationService.findAll();

        res.status(200).json(typeStations);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const typeStation: ITypeStationModel = await TypeStationService.remove(req.params.id);

        res.status(200).json(typeStation);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
