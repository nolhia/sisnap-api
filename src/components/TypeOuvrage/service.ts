import * as Joi from 'joi';
import TypeOuvrageModel, { ITypeOuvrageModel } from './model';
import TypeOuvrageValidation from './validation';
import { ITypeOuvrageService } from './interface';

/**
 * @export
 * @implements {ITypeOuvrageService}
 */
const TypeOuvrageService: ITypeOuvrageService = {

    /**
     * @param {ITypeOuvrageModel} TypeOuvrage
     * @returns {Promise < ITypeOuvrageModel >}
     * @memberof TypeOuvrageService
     */
    async insert(body: ITypeOuvrageModel): Promise<ITypeOuvrageModel> {
        try {
            const validate: Joi.ValidationResult<ITypeOuvrageModel> = TypeOuvrageValidation.createTypeOuvrage(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const typeOuvrage: ITypeOuvrageModel = await TypeOuvrageModel.create(body);

            return typeOuvrage;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {ITypeOuvrageModel} TypeOuvrage
     * @returns {Promise < ITypeOuvrageModel >}
     * @memberof TypeOuvrageService
     */
    async update(body: ITypeOuvrageModel): Promise<ITypeOuvrageModel> {
        try {
            const validate: Joi.ValidationResult<ITypeOuvrageModel> = TypeOuvrageValidation.updateTypeOuvrage(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            const typeOuvrage: ITypeOuvrageModel = await TypeOuvrageModel.update(body);

            return typeOuvrage;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ITypeOuvrageModel >}
     * @memberof TypeOuvrageService
     */
    async findOne(id: string): Promise<ITypeOuvrageModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = TypeOuvrageValidation.getTypeOuvrage({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await TypeOuvrageModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },


    /**
     * @param {string} code
     * @returns {Promise < ITypeOuvrageModel >}
     * @memberof TypeOuvrageService
     */
    async findOneByCode(code: string): Promise<ITypeOuvrageModel> {
        try {
            const validate: Joi.ValidationResult<{
                code: string
            }> = TypeOuvrageValidation.getTypeOuvrageByCode({
                code
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await TypeOuvrageModel.findOne({
                code: code
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < ITypeOuvrageModel[] >}
     * @memberof TypeOuvrageService
     */
    async findAll(): Promise<ITypeOuvrageModel[]> {
        try {
            return await TypeOuvrageModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ITypeOuvrageModel >}
     * @memberof TypeOuvrageService
     */
    async remove(id: string): Promise<ITypeOuvrageModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = TypeOuvrageValidation.getTypeOuvrage({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await TypeOuvrageModel.findOneAndRemove({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    }
}

export default TypeOuvrageService;
