import * as Joi from 'joi';
import Validation from '../validation';
import { ITypeOuvrageModel } from './model';

/**
 * Validation des données des types d'ouvrages avant enregistrement dans la BD
 * @export
 * @class TypeOuvrageValidation
 * @extends Validation
 */
class TypeOuvrageValidation extends Validation {
    /**
     * Creates an instance of TypeOuvrageValidation.
     * @memberof TypeOuvrageValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {ITypeOuvrageModel} params
     * @returns {Joi.ValidationResult<ITypeOuvrageModel >}
     * @memberof TypeOuvrageValidation
     */
    createTypeOuvrage(
        params: ITypeOuvrageModel
    ): Joi.ValidationResult<ITypeOuvrageModel> {
        const schema: Joi.Schema = Joi.object().keys({
            code: Joi.string().required(),
            titre: Joi.string().required()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {ITypeOuvrageModel} params
     * @returns {Joi.ValidationResult<ITypeOuvrageModel >}
     * @memberof TypeOuvrageValidation
     */
    updateTypeOuvrage(
        params: ITypeOuvrageModel
    ): Joi.ValidationResult<ITypeOuvrageModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string(),
            code: Joi.string().required(),
            titre: Joi.string().required()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof TypeOuvrageValidation
     */
    getTypeOuvrage(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ code: string }} body
     * @returns {Joi.ValidationResult<{ code: string }>}
     * @memberof TypeOuvrageValidation
     */
    getTypeOuvrageByCode(
        body: {
            code: string
        }
    ): Joi.ValidationResult<{
        code: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            code: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof TypeOuvrageValidation
     */
    removeTypeOuvrage(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }
}

export default new TypeOuvrageValidation();
