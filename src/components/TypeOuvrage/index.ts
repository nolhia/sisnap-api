import TypeOuvrageService from './service';
import { HttpError } from '../../config/error';
import { ITypeOuvrageModel } from './model';
import { NextFunction, Request, Response } from 'express';

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const typeOuvrage: ITypeOuvrageModel = await TypeOuvrageService.insert(req.body);

        res.status(201).json(typeOuvrage);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const typeOuvrage: ITypeOuvrageModel = await TypeOuvrageService.update(req.body);

        res.status(200).json(typeOuvrage);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const typeOuvrage: ITypeOuvrageModel = await TypeOuvrageService.findOne(req.params.id);

        res.status(200).json(typeOuvrage);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const typeOuvrages: ITypeOuvrageModel[] = await TypeOuvrageService.findAll();

        res.status(200).json(typeOuvrages);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const typeOuvrage: ITypeOuvrageModel = await TypeOuvrageService.remove(req.params.id);

        res.status(200).json(typeOuvrage);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}



