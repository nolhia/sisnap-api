import knex from '../../config/connection/database';

/**
 * @export
 * @interface ITypeOuvrageModel
 */
export interface ITypeOuvrageModel {
    id?: number;
    code?: string;
    titre?: string;
}

export type AuthToken = {
    accessToken: string,
    kind: string
};

const tableName: string = 'type_ouvrage';

class TypeOuvrageModel {
    async create(objet: ITypeOuvrageModel): Promise<ITypeOuvrageModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<ITypeOuvrageModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[ITypeOuvrageModel]> {
        return await knex.select().from(tableName).orderBy('code');
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where({ id }).del();
    }

    async update(typeOuvrage: ITypeOuvrageModel): Promise<ITypeOuvrageModel> {
        return await knex(tableName).where({ id: typeOuvrage.id }).update(typeOuvrage);
    }
}

export default new TypeOuvrageModel();
