import { ITypeOuvrageModel } from './model';

/**
 * @export
 * @interface ITypeOuvrageService
 */
export interface ITypeOuvrageService {
    /**
     * @param {ITypeOuvrageModel} ITypeOuvrageModel
     * @returns {Promise<ITypeOuvrageModel>}
     * @memberof ITypeOuvrageService
     */
    insert(ITypeOuvrageModel: ITypeOuvrageModel): Promise<ITypeOuvrageModel>;

    /**
     * @param {ITypeOuvrageModel} ITypeOuvrageModel
     * @returns {Promise<ITypeOuvrageModel>}
     * @memberof ITypeOuvrageService
     */
    update(ITypeOuvrageModel: ITypeOuvrageModel): Promise<ITypeOuvrageModel>;

    /**
     * @param {string} id
     * @returns {Promise<ITypeOuvrageModel>}
     * @memberof ITypeOuvrageService
     */
    findOne(id: string): Promise<ITypeOuvrageModel>;

    /**
     * @param {string} code
     * @returns {Promise<ITypeOuvrageModel>}
     * @memberof ITypeOuvrageService
     */
    findOneByCode(code: string): Promise<ITypeOuvrageModel>;

    /**
     * @returns {Promise<ITypeOuvrageModel[]>}
     * @memberof ITypeOuvrageService
     */
    findAll(): Promise<ITypeOuvrageModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<ITypeOuvrageModel>}
     * @memberof ITypeOuvrageService
     */
    remove(id: string): Promise<any>;
}
