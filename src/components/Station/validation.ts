import * as Joi from 'joi';
import Validation from '../validation';
import { IStationModel } from './model';

/**
 * Validation des données des stations avant enregistrement dans la BD
 * @export
 * @class StationValidation
 * @extends Validation
 */
class StationValidation extends Validation {

    /**
     * Creates an instance of TypeStationValidation.
     * @memberof StationValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {IStationModel} params
     * @returns {Joi.ValidationResult<IStationModel >}
     * @memberof StationValidation
     */
    createStation(
        params: IStationModel
    ): Joi.ValidationResult<IStationModel> {
        const schema: Joi.Schema = Joi.object().keys({
            code: Joi.string().required(),
            titre: Joi.string().required(),
            latitude: Joi.number().required(),
            longitude: Joi.number().required(),
            altitude: Joi.number().required(),
            profondeur_totale: Joi.number().optional(),
            repere_sol: Joi.number().optional(),
            type_enregistrement: Joi.string().optional(),
            type_ouvrage: Joi.string().required(),
            type_station: Joi.string().required(),
            nappe: Joi.string().required(),
            note: Joi.string().optional(),
            region: Joi.string().optional(),
            departement: Joi.string().optional(),
            commune: Joi.string().optional()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {IStationModel} params
     * @returns {Joi.ValidationResult<IStationModel >}
     * @memberof StationValidation
     */
    updateStation(
        params: IStationModel
    ): Joi.ValidationResult<IStationModel> {
        const schema: Joi.Schema = Joi.object().keys({
            code: Joi.string().required(),
            titre: Joi.string().required(),
            latitude: Joi.number().required(),
            longitude: Joi.number().required(),
            altitude: Joi.number().required(),
            profondeur_totale: Joi.number().optional(),
            repere_sol: Joi.number().optional(),
            type_enregistrement: Joi.string().optional(),
            type_ouvrage: Joi.string().required(),
            type_station: Joi.string().required(),
            nappe: Joi.string().required(),
            note: Joi.string().optional(),
            region: Joi.string().optional(),
            departement: Joi.string().optional(),
            commune: Joi.string().optional()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof StationValidation
     */
    getStation(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ code: string }} body
     * @returns {Joi.ValidationResult<{ code: string }>}
     * @memberof StationValidation
     */
    getStationByCode(
        body: {
            code: string
        }
    ): Joi.ValidationResult<{
        code: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            code: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof StationValidation
     */
    removeStation(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

}

export default new StationValidation();
