import knex from '../../config/connection/database';

/**
 * @export
 * @interface IStationModel
 */
export interface IStationModel {
    id?: number;
    code?: string;
    titre?: string;
    latitude?: number;
    longitude?: number;
    altitude?: number;
    profondeur_totale?: number;
    repere_sol?: number;
    type_enregistrement?: string;
    type_ouvrage_id?: string;
    type_station_id?: string;
    nappe_id?: string;
    note?: string;
    region_id?: string;
    departement_id?: string;
    commune_id?: string;
}

export type AuthToken = {
    accessToken: string,
    kind: string
};

const tableName: string = 'station';

class StationModel {
    async create(objet: IStationModel): Promise<IStationModel> {
        const id: number[] = await knex(tableName).insert(objet, 'id');

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IStationModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[IStationModel]> {
        return await knex.select('station.*', 'nappe.titre as nappe').from(tableName)
            .leftJoin('nappe', 'station.nappe_id', 'nappe.id')
            .orderBy('code');
    }

    async export(): Promise<[any]> {
        return await knex(tableName)
            .join('nappe', 'nappe.id', '=', tableName + '.nappe_id')
            .join('type_station', 'type_station.id', '=', tableName + '.type_station_id')
            .join('type_ouvrage', 'type_ouvrage.id', '=', tableName + '.type_ouvrage_id')
            .join('region', 'region.id', '=', tableName + '.region_id')
            .join('departement', 'departement.id', '=', tableName + '.departement_id')
            .join('commune', 'commune.id', '=', tableName + '.commune_id')
            .select(
                tableName + '.code',
                tableName + '.titre',
                tableName + '.latitude',
                tableName + '.longitude',
                tableName + '.altitude',
                tableName + '.profondeur_totale',
                tableName + '.repere_sol',
                tableName + '.note',
                'type_station.titre as type_station',
                'type_ouvrage.titre as type_ouvrage',
                'region.titre as region',
                'departement.titre as departement',
                'commune.titre as commune',
            );
    }

    async update(typeStation: IStationModel): Promise<IStationModel> {
        return await knex(tableName).where({ id: typeStation.id }).update(typeStation);
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id).del();;
    }

    async upload(data: any): Promise<any> {
        return await knex(tableName).insert(data);
    }
}

export default new StationModel();

