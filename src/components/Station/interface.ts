import { IStationModel } from './model';

/**
 * @export
 * @interface ITypeStationService
 */
export interface IStationService {

    /**
     * @param {IStationModel} IStationModel
     * @returns {Promise<ITypeStationModel>}
     * @memberof IStationService
     */
    insert(IStationModel: IStationModel): Promise<IStationModel>;

    /**
     * @param {IStationModel} IStationModel
     * @returns {Promise<IStationModel>}
     * @memberof IStationService
     */
    update(IStationModel: IStationModel): Promise<IStationModel>;

    /**
     * @param {string} id
     * @returns {Promise<IStationModel>}
     * @memberof IStationService
     */
    findOne(id: string): Promise<IStationModel>;

    /**
     * @param {string} code
     * @returns {Promise<IStationModel>}
     * @memberof IStationService
     */
    findOneByCode(code: string): Promise<IStationModel>;

    /**
     * @returns {Promise<IStationModel[]>}
     * @memberof IStationService
     */
    findAll(): Promise<IStationModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<IStationModel>}
     * @memberof IStationService
     */
    remove(id: string): Promise<any>;

    /**
     * @param {any} data
     * @returns {Promise<any>}
     * @memberof IStationService
     */
    upload(data: any): Promise<any>;

    /**
     * @returns {Promise<any[]>}
     * @memberof IStationService
     */
    export(): Promise<any[]>;
}
