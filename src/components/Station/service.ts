import * as Joi from 'joi';
import StationModel, { IStationModel } from './model';
import StationValidation from './validation';
import { IStationService } from './interface';

/**
 * @export
 * @implements {IStationService}
 */
const StationService: IStationService = {
    /**
     * @param {IStationModel} Station
     * @returns {Promise < IStationModel >}
     * @memberof StationService
     */
    async insert(body: IStationModel): Promise<IStationModel> {
        try {
            /*const validate: Joi.ValidationResult<IStationModel> = StationValidation.createStation(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }
            */
            const station: IStationModel = await StationModel.create(body);

            return station;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IStationModel} Station
     * @returns {Promise < IStationModel >}
     * @memberof StationService
     */
    async update(body: IStationModel): Promise<IStationModel> {
        try {
            /*
            const validate: Joi.ValidationResult<IStationModel> = StationValidation.updateStation(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }
            */
            const station: IStationModel = await StationModel.update(body);

            return station;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IStationModel >}
     * @memberof StationService
     */
    async findOne(id: string): Promise<IStationModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = StationValidation.getStation({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await StationModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} code
     * @returns {Promise < IStationModel >}
     * @memberof StationService
     */
    async findOneByCode(code: string): Promise<IStationModel> {
        try {
            const validate: Joi.ValidationResult<{
                code: string
            }> = StationValidation.getStationByCode({
                code
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await StationModel.findOne({
                code: code
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < IStationModel[] >}
     * @memberof StationService
     */
    async findAll(): Promise<IStationModel[]> {
        try {
            return await StationModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IStationModel >}
     * @memberof StationService
     */
    async remove(id: string): Promise<IStationModel> {
        try { 
            return await StationModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {any} data
     * @returns {Promise < IStationModel >}
     * @memberof StationService
     */
    async upload(data: any): Promise<IStationModel> {
        try {
            return await StationModel.upload(data);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < any[] >}
     * @memberof StationService
     */
    async export(): Promise<any[]> {
        try {
            const data: any[] = await StationModel.export();

            return data;

        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default StationService;
