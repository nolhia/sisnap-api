import StationService from './service';
import NappeService from '../Nappe/service';
import TypeStationService from '../TypeStation/service';
import TypeOuvrageService from '../TypeOuvrage/service';
import ZonegeographiqueService from '../Zonegeographique/service';
import { HttpError } from '../../config/error';
import { IStationModel } from './model';
import { NextFunction, Request, Response } from 'express';
import UtilsHelper from '../../helpers/UtilsHelper';
const fs: any = require('fs');
const jsonData: any = require('../../../data/data.json');
const _: any = require('underscore');

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const station: IStationModel = await StationService.insert(req.body);

        res.status(201).json(station);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const station: IStationModel = await StationService.update(req.body);

        res.status(200).json(station);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const station: IStationModel = await StationService.findOne(req.params.id);

        res.status(200).json(station);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const stations: IStationModel[] = await StationService.findAll();

        res.status(200).json(stations);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const station: IStationModel = await StationService.remove(req.params.id);

        res.status(200).json(station);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}


/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function upload(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const exceltojson: any = await UtilsHelper.convertExcelToJson(req);

        await exceltojson(
            {
                input: req.file.path,
                output: null,
                lowerCaseHeaders: true
            },
            async (err: any, result: any) => {
                if (err) {
                    res.status(400).json({ error_code: 1, err_desc: err, data: null });
                }
                // supprimer le fichier 
                fs.unlinkSync(req.file.path);

                // pour ligne de données on verifie si le labortoire ou la station existe
                // 1 - Si oui on recupère leur id
                // 2 - Si non on les insères dans leur table respective et on récupère l'id de l'enregistrement
                try {
                    const promisedStations: any = result.map(async (element: any) => {
                        const typeStationCode: any = element.type_station;
                        const typeOuvrageCode: any = element.type_ouvrage;
                        var nappeCode: any = element.nappe;
                        const regionCode: any = element.region;
                        const departementCode: any = element.departement;
                        const communeCode: any = element.commune;

                        try {
                            // Traitement des type stations
                            const typeStation: any = await TypeStationService.findOneByCode(typeStationCode);
                            if (typeStation && typeStation.id) {
                                element.type_station_id = typeStation.id;
                            } else {
                                console.log('le type station ' + typeStationCode + ' n\'existe pas pour l\'instant, il sera crée');
                                const typeStationData: any = {
                                    'code': typeStationCode,
                                    'titre': typeStationCode,
                                };
                                const nouvelleTypeStation: any = await TypeStationService.insert(typeStationData);
                                if (nouvelleTypeStation && nouvelleTypeStation.id) {
                                    element.type_station_id = nouvelleTypeStation.id;
                                }

                            }

                            // Traitement typeOuvrage
                            const typeOuvrage: any = await TypeOuvrageService.findOneByCode(typeOuvrageCode);
                            if (typeOuvrage && typeOuvrage.id) {
                                element.type_ouvrage_id = typeOuvrage.id;
                            } else {
                                console.log('le type ouvrage ' + typeOuvrageCode + ' n\'existe pas pour l\'instant, il sera crée');
                                const typeOuvrageData: any = {
                                    'code': typeOuvrageCode,
                                    'titre': typeOuvrageCode,
                                };
                                const nouvelleTypeOuvrage: any = await TypeOuvrageService.insert(typeOuvrageData);
                                if (nouvelleTypeOuvrage && nouvelleTypeOuvrage.id) {
                                    element.type_ouvrage_id = nouvelleTypeOuvrage.id;
                                }

                            }

                            // Traitement nappe
                            if (nappeCode === '') {
                                nappeCode = 1
                            }
                            const nappe: any = await NappeService.findOneByCode(nappeCode);
                            if (nappe && nappe.id) {
                                element.nappe_id = nappe.id;
                            } else {
                                console.log('la nappe ' + nappeCode + ' n\'existe pas pour l\'instant, elle sera créee');
                                const nappeData: any = {
                                    'code': typeOuvrageCode,
                                    'titre': typeOuvrageCode,
                                };
                                const nouvelleNappe: any = await NappeService.insert(nappeData);
                                if (nouvelleNappe && nouvelleNappe.id) {
                                    element.nappe_id = nouvelleNappe.id;
                                }

                            }


                            // Type enregistrement
                            const type_enregistrements: any = jsonData.type_enregistreurs;
                            const type_enregistrement: any = _.where(type_enregistrements, { code: element.type_enregistrement });

                            if (type_enregistrement && type_enregistrement.code === element.type_enregistrement) {
                                element.type_enregistrement = type_enregistrement.id;
                            } else {
                                element.type_enregistrement = '018f91d3-8c0f-44d3-85c6-b3e2d0114def'; // id enregistrement manuel
                            }

                            // region
                            if (regionCode !== '') {
                                const region: any = await ZonegeographiqueService.findOneRegionByCode(regionCode);
                                if (region && region.id) {
                                    element.region_id = region.id;
                                } else {
                                    res.status(400).json({ message: 'La région ' + regionCode + ' de la station ' + element.code + ' n\'existe pas. Veuillez corriger votre fichier svp.' });

                                    return;
                                }
                            }

                            // departement
                            if (departementCode !== '') {
                                const departement: any = await ZonegeographiqueService.findOneDepartementByCode(departementCode);
                                if (departement && departement.id) {
                                    element.departement_id = departement.id;
                                }
                            }

                            // commune
                            if (communeCode !== '') {
                                const commune: any = await ZonegeographiqueService.findOneCommuneByCode(communeCode);
                                if (commune && commune.id) {
                                    element.commune_id = commune.id;
                                }
                            }

                            delete element['type_station'];
                            delete element['type_ouvrage'];
                            delete element['nappe'];
                            delete element['region'];
                            delete element['departement'];
                            delete element['commune'];

                            return element;

                        } catch (error) {
                            console.log(error.message);
                            res.status(500).json(error.message);
                        }

                    });

                    const stations: any = await Promise.all(promisedStations);

                    await StationService.upload(result);
                    res.status(200).json(stations);
                } catch (er) {
                    console.log(er);
                    res.status(400).json('Une erreure inatendue s\'est produite durant l\'opération. Veuillez vérifier votre fichier SVP.');
                }

            }
        );

    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function exportData(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const data: any[] = await StationService.export();

        res.status(200).json(data);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
