import * as Joi from 'joi';
import NappeModel, { INappeModel } from './model';
import NappeValidation from './validation';
import { INappeService } from './interface';

/**
 * @export
 * @implements {INappeService}
 */
const NappeService: INappeService = {

    /**
     * @param {INappeModel} Nappe
     * @returns {Promise < INappeModel >}
     * @memberof NappeService
     */
    async insert(body: INappeModel): Promise<INappeModel> {
        try {
            const Nappe: INappeModel = await NappeModel.create(body);

            return Nappe;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {INappeModel} Nappe
     * @returns {Promise < INappeModel >}
     * @memberof NappeService
     */
    async update(body: INappeModel): Promise<INappeModel> {
        try {
            const Nappe: INappeModel = await NappeModel.update(body);

            return Nappe;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < INappeModel >}
     * @memberof NappeService
     */
    async findOne(id: string): Promise<INappeModel> {
        try {
            const validate: Joi.ValidationResult<{
                id: string
            }> = NappeValidation.getNappe({
                id
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await NappeModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} code
     * @returns {Promise < INappeModel >}
     * @memberof NappeService
     */
    async findOneByCode(code: string): Promise<INappeModel> {
        try {
            const validate: Joi.ValidationResult<{
                code: string
            }> = NappeValidation.getNappeByCode({
                code
            });

            if (validate.error) {
                throw new Error(validate.error.message);
            }

            return await NappeModel.findOne({
                code: code
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < INappeModel[] >}
     * @memberof NappeService
     */
    async findAll(): Promise<INappeModel[]> {
        try {
            return await NappeModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < INappeModel >}
     * @memberof NappeService
     */
    async remove(id: string): Promise<INappeModel> {
        try {
            return await NappeModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {any} data
     * @returns {Promise < INappeModel >}
     * @memberof NappeService
     */
    async upload(data: any): Promise<INappeModel> {
        try {
            return await NappeModel.upload(data);
        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default NappeService;
