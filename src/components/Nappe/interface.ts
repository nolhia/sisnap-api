import { INappeModel } from './model';

/**
 * @export
 * @interface INappeService
 */
export interface INappeService {

    /**
     * @param {INappeModel} INappeModel
     * @returns {Promise<INappeModel>}
     * @memberof INappeService
     */
    insert(INappeModel: INappeModel): Promise<INappeModel>;

    /**
     * @param {INappeModel} INappeModel
     * @returns {Promise<INappeModel>}
     * @memberof INappeService
     */
    update(INappeModel: INappeModel): Promise<INappeModel>;

    /**
     * @param {string} id
     * @returns {Promise<INappeModel>}
     * @memberof INappeService
     */
    findOne(id: string): Promise<INappeModel>;

    /**
     * @param {string} code
     * @returns {Promise<INappeModel>}
     * @memberof INappeService
     */
    findOneByCode(code: string): Promise<INappeModel>;

    /**
     * @returns {Promise<INappeModel[]>}
     * @memberof INappeService
     */
    findAll(): Promise<INappeModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<INappeModel>}
     * @memberof INappeService
     */
    remove(id: string): Promise<any>;

    /**
     * @param {any} data
     * @returns {Promise<any>}
     * @memberof INappeService
     */
    upload(data: any): Promise<any>;

}
