import NappeService from './service';
import { HttpError } from '../../config/error';
import { INappeModel } from './model';
import { NextFunction, Request, Response } from 'express';
import UtilsHelper from '../../helpers/UtilsHelper';
const fs: any = require('fs');

/**
 * Ajouter une nouvelle nappe
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Nappe: INappeModel = await NappeService.insert(req.body);

        res.status(201).json(Nappe);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Mettre à jour une nappe existante
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Nappe: INappeModel = await NappeService.update(req.body);

        res.status(200).json(Nappe);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Traiter un fichier csv contenant une liste de nappes à insérer dans la BD
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function upload(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        // console.log(req.file)
        const exceltojson: any = await UtilsHelper.convertExcelToJson(req);

        await exceltojson({
            input: req.file.path, //the same path where we uploaded our file
            output: null, //since we don't need output.json
            lowerCaseHeaders: true
        }, async function (err, result) {

            if (err) {
                res.status(400).json({ error_code: 1, err_desc: err, data: null })
            }
            //supprimer le fichier 
            fs.unlinkSync(req.file.path);
            await NappeService.upload(result);
            res.status(200).json(result);
        })

    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Retrouver une nappe à l'aide de son id
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Nappe: INappeModel = await NappeService.findOne(req.params.id);

        res.status(200).json(Nappe);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer la liste de toutes les nappes
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Nappes: INappeModel[] = await NappeService.findAll();

        res.status(200).json(Nappes);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Supprimer une nappe à l'aide de son id
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Nappe: INappeModel = await NappeService.remove(req.params.id);

        res.status(200).json(Nappe);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
