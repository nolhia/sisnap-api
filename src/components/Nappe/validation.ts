import * as Joi from 'joi';
import Validation from '../validation';
import { INappeModel } from './model';

/**
 * Validation des données des nappes avant enregistrement dans la BD
 * @export
 * @class NappeValidation
 * @extends Validation
 */
class NappeValidation extends Validation {

    /**
     * Creates an instance of NappeValidation.
     * @memberof NappeValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {INappeModel} params
     * @returns {Joi.ValidationResult<INappeModel >}
     * @memberof NappeValidation
     */
    createNappe(
        params: INappeModel
    ): Joi.ValidationResult<INappeModel> {
        const schema: Joi.Schema = Joi.object().keys({
            code: Joi.string().required(),
            cote_toit: Joi.string(),
            cote_mur: Joi.string(),
            note: Joi.string().allow('').optional(),
            titre: Joi.string().required(),
            type_nappe: Joi.string(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {INappeModel} params
     * @returns {Joi.ValidationResult<INappeModel >}
     * @memberof NappeValidation
     */
    updateNappe(
        params: INappeModel
    ): Joi.ValidationResult<INappeModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string(),
            code: Joi.string(),
            cote_toit: Joi.number().allow(null).optional(),
            cote_mur: Joi.number().allow(null).optional(),
            note: Joi.string().allow('').optional(),
            titre: Joi.string(),
            type_nappe: Joi.string(),
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof NappeValidation
     */
    getNappe(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ code: string }} body
     * @returns {Joi.ValidationResult<{ code: string }>}
     * @memberof NappeValidation
     */
    getNappeByCode(
        body: {
            code: string
        }
    ): Joi.ValidationResult<{
        code: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            code: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof NappeValidation
     */
    removeNappe(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

}

export default new NappeValidation();
