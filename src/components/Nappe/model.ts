import knex  from '../../config/connection/database';

/**
 * @export
 * @interface INappeModel
 */
export interface INappeModel {
    id?: number; 
}
  
const tableName:string = 'nappe';

class NappeModel{ 
    async create(objet:INappeModel): Promise<INappeModel> { 
        const id:number[] = await knex(tableName).insert(objet); 

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<INappeModel> {  
        return await knex.select().from(tableName).where(objet).first(); 
    }

    async findAll(): Promise<[INappeModel]> {
        return await knex.select().from(tableName).orderBy('code');
    }

    async findOneAndRemove(id: any): Promise<any> { 
        return await knex(tableName).where('id', id).del();
    }

    async update(Nappe:INappeModel): Promise<INappeModel> {
        return await knex(tableName).where({ id: Nappe.id }).update(Nappe);
    }

    async upload(data:any): Promise<any> {

        return await knex(tableName).insert(data);  
    }
}

export default new NappeModel();
