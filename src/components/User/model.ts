import * as bcrypt from 'bcryptjs';
import knex  from '../../config/connection/database'; 

/**
 * @export
 * @interface IUserModel
 * @extends {Document}
 */
export interface IUserModel {
    id?: number;
    email?: string;
    password?: string;
    nom?: string;
    prenoms?: string; 
    status?: string;
    matricule?: string;
    role?: string;
    niveau_entree?: string;
    telephone?: string;
    direction?: string;
    region_id?: string;
    departement_id?: string;
    passwordResetToken?: string;
    passwordResetExpires?: Date; 

    tokens?: AuthToken[];
 
    comparePassword?: (password: string) => Promise < boolean > ; 
}

export type AuthToken = {
    accessToken: string,
    kind: string
};

const tableName:string = 'utilisateur';

class UserModel{ 
    
    async create(user:IUserModel): Promise<IUserModel> {
        const id:number[] = await knex(tableName).insert(user);

        return { ...user, id: id[0] };
    }

    async findOne(objet: any): Promise<IUserModel> {  
        return await knex.select().from(tableName).where(objet).first(); 
    }

    async findAll(): Promise<[IUserModel]> {
        return await knex.select().from(tableName).orderBy('prenoms');
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id ).del();
    }

    async update(user:IUserModel): Promise<IUserModel> {
        return await knex(tableName).where({ id: user.id }).update(user);
    }

    // Authentication information

    async comparePassword(user:IUserModel, xpassword:string): Promise < boolean > {    

        if (user && bcrypt.compareSync(xpassword, user.password)){
            return true;
        }

        return false;
    }

}

export default new UserModel(); 
 