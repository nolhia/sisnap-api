import * as Joi from 'joi';
import Validation from '../validation';
import { IUserModel } from './model';

/**
 * Validation des données des utilisateurs avant enregistrement dans la BD
 * @export
 * @class UserValidation
 * @extends Validation
 */
class UserValidation extends Validation {

    /**
     * Creates an instance of UserValidation.
     * @memberof UserValidation
     */
    constructor() {
        super();
    }

    /**
     * @param {IUserModel} params
     * @returns {Joi.ValidationResult<IUserModel >}
     * @memberof UserValidation
     */
    createUser(
        params: IUserModel
    ): Joi.ValidationResult<IUserModel> {
        const schema: Joi.Schema = Joi.object().keys({
            nom: Joi.string().required(),
            prenoms: Joi.string().required(),
            email: Joi.string().required(),
            password: Joi.string().required(),
            telephone: Joi.string().optional().allow(''),
            matricule: Joi.string().optional().allow(''),
            role: Joi.string(),
            niveau_entree: Joi.string(),
            region_id: Joi.string().optional().allow(''),
            departement_id: Joi.string().optional().allow(''),
            direction: Joi.string().optional().allow(''),
            status: Joi.string()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {IUserModel} params
     * @returns {Joi.ValidationResult<IUserModel >}
     * @memberof UserValidation
     */
    updateUser(
        params: IUserModel
    ): Joi.ValidationResult<IUserModel> {
        const schema: Joi.Schema = Joi.object().keys({
            id: Joi.string().required(),
            nom: Joi.string(),
            prenoms: Joi.string(),
            email: Joi.string(),
            password: Joi.string(),
            telephone: Joi.string().optional().allow(''),
            matricule: Joi.string().optional().allow(''),
            role: Joi.string(),
            niveau_entree: Joi.string(),
            region_id: Joi.string().optional().allow(''),
            departement_id: Joi.string().optional().allow(''),
            direction: Joi.string().optional().allow(''),
            status: Joi.string()
        });

        return Joi.validate(params, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof UserValidation
     */
    getUser(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }

    /**
     * @param {{ id: string }} body
     * @returns {Joi.ValidationResult<{ id: string }>}
     * @memberof UserValidation
     */
    removeUser(
        body: {
            id: string
        }
    ): Joi.ValidationResult<{
        id: string
    }> {
        const schema: Joi.Schema = Joi.object().keys({
            id: this.customJoi.objectId().required()
        });

        return Joi.validate(body, schema);
    }
}

export default new UserValidation();
