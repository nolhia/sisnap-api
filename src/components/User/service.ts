import * as Joi from 'joi';
import * as bcrypt from 'bcryptjs';
import UserModel, { IUserModel } from './model';
import UserValidation from './validation';
import { IUserService } from './interface'; 

/**
 * @export
 * @implements {IUserModelService}
 */
const UserService: IUserService = {
    /**
     * @returns {Promise < IUserModel[] >}
     * @memberof UserService
     */
    async findAll(): Promise < IUserModel[] > {
        try {
            return await UserModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} _id
     * @returns {Promise < IUserModel >}
     * @memberof UserService
     */
    async findOne(_id: string): Promise < IUserModel > {
        try {
            return await UserModel.findOne({
                id: _id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IUserModel} user
     * @returns {Promise < IUserModel >}
     * @memberof UserService
     */
    async insert(body: IUserModel): Promise < IUserModel > {
        try {
            const validate: Joi.ValidationResult < IUserModel > = UserValidation.createUser(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }
            body.password = bcrypt.hashSync(body.password, 10);
            const user: IUserModel = await UserModel.create(body);
            return user;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IUserModel} user
     * @returns {Promise < IUserModel >}
     * @memberof UserService
     */
    async update(body: IUserModel): Promise < IUserModel > {
        try {
            const validate: Joi.ValidationResult < IUserModel > = UserValidation.updateUser(body);

            if (validate.error) {
                throw new Error(validate.error.message);
            }
            if(body.password && body.password !== ''){
                body.password = bcrypt.hashSync(body.password, 10);
            }
            
            const user: IUserModel = await UserModel.update(body);

            return user;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IUserModel >}
     * @memberof UserService
     */
    async remove(id: string): Promise < IUserModel > {
        try { 
            const user: IUserModel = await UserModel.findOneAndRemove(id);

            return user;
        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default UserService;
