import { ICarteStatiqueModel } from './model';

/**
 * @export
 * @interface ICarteStatiqueService
 */
export interface ICarteStatiqueService {

    /**
     * @param {ICarteStatiqueModel} ICarteStatiqueModel
     * @returns {Promise<ICarteStatiqueModel>}
     * @memberof ICarteStatiqueService
     */
    insert(ICarteStatiqueModel: ICarteStatiqueModel): Promise<ICarteStatiqueModel>;

    /**
     * @param {ICarteStatiqueModel} ICarteStatiqueModel
     * @returns {Promise<ICarteStatiqueModel>}
     * @memberof ICarteStatiqueService
     */
    update(ICarteStatiqueModel: ICarteStatiqueModel): Promise<ICarteStatiqueModel>;

    /**
     * @param {string} id
     * @returns {Promise<ICarteStatiqueModel>}
     * @memberof ICarteStatiqueService
     */
    findOne(id: string): Promise<ICarteStatiqueModel>;

    /**
     * @returns {Promise<ICarteStatiqueModel[]>}
     * @memberof ICarteStatiqueService
     */
    findAll(): Promise<ICarteStatiqueModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<ICarteStatiqueModel>}
     * @memberof ICarteStatiqueService
     */
    remove(id: string): Promise<any>;

}
