import knex  from '../../config/connection/database';

/**
 * @export
 * @interface ICarteStatiqueModel
 */
export interface ICarteStatiqueModel {
    id?: number; 
}
  
const tableName:string = 'carte';

class CarteStatiqueModel{ 
    async create(objet:ICarteStatiqueModel): Promise<ICarteStatiqueModel> { 
        const id:number[] = await knex(tableName).insert(objet); 

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<ICarteStatiqueModel> {  
        return await knex.select().from(tableName).where(objet).first(); 
    }

    async findAll(): Promise<[ICarteStatiqueModel]> {
        return await knex.select().from(tableName).orderBy('titre');
    }

    async findOneAndRemove(id: any): Promise<any> { 
        return await knex(tableName).where('id', id).del();
    }

    async update(CarteStatique:ICarteStatiqueModel): Promise<ICarteStatiqueModel> {
        return await knex(tableName).where({ id: CarteStatique.id }).update(CarteStatique);
    }

    async upload(data:any): Promise<any> {

        return await knex(tableName).insert(data);  
    }
}

export default new CarteStatiqueModel();
