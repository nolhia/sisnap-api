import CarteStatiqueService from './service';
import { HttpError } from '../../config/error';
import { ICarteStatiqueModel } from './model';
import { NextFunction, Request, Response } from 'express';
import UtilsHelper from '../../helpers/UtilsHelper';
const fs = require('fs');
/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try { 
        const file = req.file;  
        // Convertir image en base64
        const buff = fs.readFileSync(file.path);
        const base64image = buff.toString('base64'); 
        const data = JSON.parse(req.body.data);
        //data.path = file.path+'.'+file.originalname.split('.').pop();  
        data.size = file.size; 
        data.filename = file.filename;
        data.image =  base64image;
        
        const CarteStatique: ICarteStatiqueModel = await CarteStatiqueService.insert(data);
        //Suprimer le ficher temporaire
        //fs.unlink(file.path);
        res.status(201).json(CarteStatique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const CarteStatique: ICarteStatiqueModel = await CarteStatiqueService.update(req.body);

        res.status(200).json(CarteStatique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}


/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const CarteStatique: ICarteStatiqueModel = await CarteStatiqueService.findOne(req.params.id);

        res.status(200).json(CarteStatique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const CarteStatiques: ICarteStatiqueModel[] = await CarteStatiqueService.findAll();

        res.status(200).json(CarteStatiques);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const CarteStatique: ICarteStatiqueModel = await CarteStatiqueService.remove(req.params.id);

        res.status(200).json(CarteStatique);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
