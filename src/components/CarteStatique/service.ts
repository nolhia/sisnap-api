import CarteStatiqueModel, { ICarteStatiqueModel } from './model'; 
import { ICarteStatiqueService } from './interface';

/**
 * @export
 * @implements {ICarteStatiqueService}
 */
const CarteStatiqueService: ICarteStatiqueService = {

    /**
     * @param {ICarteStatiqueModel} CarteStatique
     * @returns {Promise < ICarteStatiqueModel >}
     * @memberof CarteStatiqueService
     */
    async insert(body: ICarteStatiqueModel): Promise<ICarteStatiqueModel> {
        try { 
            const CarteStatique: ICarteStatiqueModel = await CarteStatiqueModel.create(body);

            return CarteStatique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {ICarteStatiqueModel} CarteStatique
     * @returns {Promise < ICarteStatiqueModel >}
     * @memberof CarteStatiqueService
     */
    async update(body: ICarteStatiqueModel): Promise<ICarteStatiqueModel> {
        try {
             
            const CarteStatique: ICarteStatiqueModel = await CarteStatiqueModel.update(body);

            return CarteStatique;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ICarteStatiqueModel >}
     * @memberof CarteStatiqueService
     */
    async findOne(id: string): Promise<ICarteStatiqueModel> {
        try {

            return await CarteStatiqueModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < ICarteStatiqueModel[] >}
     * @memberof CarteStatiqueService
     */
    async findAll(): Promise<ICarteStatiqueModel[]> {
        try {
            return await CarteStatiqueModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < ICarteStatiqueModel >}
     * @memberof CarteStatiqueService
     */
    async remove(id: string): Promise<ICarteStatiqueModel> {
        try { 
            return await CarteStatiqueModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    } 
};

export default CarteStatiqueService;
