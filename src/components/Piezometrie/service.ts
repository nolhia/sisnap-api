import PiezometrieModel, { IPiezometrieModel } from './model';
import { IPiezometrieService } from './interface';

/**
 * @export
 * @implements {IPiezometrieService}
 */
const PiezometrieService: IPiezometrieService = {

    /**
     * @param {IPiezometrieModel} Piezometrie
     * @returns {Promise < IPiezometrieModel >}
     * @memberof PiezometrieService
     */
    async insert(body: IPiezometrieModel): Promise<IPiezometrieModel> {
        try {
            const Piezometrie: IPiezometrieModel = await PiezometrieModel.create(body);

            return Piezometrie;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {IPiezometrieModel} Piezometrie
     * @returns {Promise < IPiezometrieModel >}
     * @memberof PiezometrieService
     */
    async update(body: IPiezometrieModel): Promise<IPiezometrieModel> {
        try {
            const Piezometrie: IPiezometrieModel = await PiezometrieModel.update(body);

            return Piezometrie;
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IPiezometrieModel >}
     * @memberof PiezometrieService
     */
    async findOne(id: string): Promise<IPiezometrieModel> {
        try {
            return await PiezometrieModel.findOne({
                id: id
            });
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @returns {Promise < IPiezometrieModel[] >}
     * @memberof PiezometrieService
     */
    async findAll(): Promise<IPiezometrieModel[]> {
        try {
            return await PiezometrieModel.findAll();
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {string} id
     * @returns {Promise < IPiezometrieModel >}
     * @memberof PiezometrieService
     */
    async remove(id: string): Promise<IPiezometrieModel> {
        try {
            return await PiezometrieModel.findOneAndRemove(id);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
     * @param {any} data
     * @returns {Promise < IPiezometrieModel >}
     * @memberof PiezometrieService
     */
    async upload(data: any): Promise<IPiezometrieModel> {
        try {
            return await PiezometrieModel.upload(data);
        } catch (error) {
            throw new Error(error.message);
        }
    },

    /**
    * @returns {Promise < any[] >}
    * @memberof PiezometrieService
    */
    async export(): Promise<any[]> {
        try {
            const data: any[] = await PiezometrieModel.export();

            return data;

        } catch (error) {
            throw new Error(error.message);
        }
    }
};

export default PiezometrieService;
