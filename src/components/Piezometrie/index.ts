import PiezometrieService from './service';
import StationService from '../Station/service';
import { HttpError } from '../../config/error';
import { IPiezometrieModel } from './model';
import { NextFunction, Request, Response } from 'express';
import UtilsHelper from '../../helpers/UtilsHelper';
const fs: any = require('fs');

/**
 * Ajouter une nouvelle piezometrie
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function create(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Piezometrie: IPiezometrieModel = await PiezometrieService.insert(req.body);

        res.status(201).json(Piezometrie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Mettre à jour une piezometrie
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function update(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Piezometrie: IPiezometrieModel = await PiezometrieService.update(req.body);

        res.status(200).json(Piezometrie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer une piezometrie à l'aide de son id
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findOne(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Piezometrie: IPiezometrieModel = await PiezometrieService.findOne(req.params.id);

        res.status(200).json(Piezometrie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Récupérer la liste de toutes les piézométries
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function findAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Piezometries: IPiezometrieModel[] = await PiezometrieService.findAll();

        res.status(200).json(Piezometries);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Supprimer une piézométrie à l'aide de son id
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function remove(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const Piezometrie: IPiezometrieModel = await PiezometrieService.remove(req.params.id);

        res.status(200).json(Piezometrie);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Traiter un fichier csv contenant une liste de piezométries à insérer dans la BD
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function upload(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const exceltojson: any = await UtilsHelper.convertExcelToJson(req);

        await exceltojson(
            {
                input: req.file.path,
                output: null,
                lowerCaseHeaders: true
            },
            async (err: any, result: any) => {
                if (err) {
                    res.status(400).json({ error_code: 1, err_desc: err, data: null });
                }
                // supprimer le fichier 
                fs.unlinkSync(req.file.path);

                // pour ligne de données on verifie si la station existe
                // 1 - Si oui on recupère leur id
                // 2 - Si non on les insères dans leur table respective et on récupère l'id de l'enregistrement
                try {
                    const promisedPiezometries: any = result.map(async (element: any) => {
                        const stationCode: any = element.station;
                        element.status = 'pending';

                        try {
                            // Traitement des stations
                            const station: any = await StationService.findOneByCode(stationCode);
                            if (station && station.id) {
                                element.station_suivi_id = station.id;
                            } else {
                                console.log('la station ' + stationCode + ' n\'existe pas pour l\'instant, elle sera créee');
                                const stationData: any = {
                                    'code': stationCode,
                                    'titre': stationCode,
                                    'latitude': 1,
                                    'longitude': 1,
                                    'altitude': 1,
                                    'type_station_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                    'type_ouvrage_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                    'nappe_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                };
                                const nouvelleStation: any = await StationService.insert(stationData);
                                if (nouvelleStation && nouvelleStation.id) {
                                    element.station_suivi_id = nouvelleStation.id;
                                }

                            }

                            delete element['station'];

                            return element;

                        } catch (error) {
                            console.log(error.message);
                            res.status(500).json(error.message);
                        }

                    });

                    const piezometries: any = await Promise.all(promisedPiezometries);

                    await PiezometrieService.upload(result);
                    res.status(200).json(piezometries);
                } catch (er) {
                    console.log(er);
                    res.status(400).json('Une erreure inatendue s\'est produite durant l\'opération. Veuillez vérifier votre fichier SVP.');
                }

            }
        );

    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * Traiter un fichier csv contenant une liste d'enregistreur automatique à insérer dans la BD
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function uploadEnregistreur(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const exceltojson: any = await UtilsHelper.convertExcelToJson(req);

        await exceltojson(
            {
                input: req.file.path,
                output: null,
                lowerCaseHeaders: true
            },
            async (err: any, result: any) => {
                if (err) {
                    res.status(400).json({ error_code: 1, err_desc: err, data: null });
                }
                // supprimer le fichier 
                fs.unlinkSync(req.file.path);

                try {
                    const promisedPiezometries: any = result.map(async (element: any) => {
                        element.station_suivi_id = 'dd054764-0c0b-11ea-8d71-362b9e155667'; // On attribue une station par defaut vu que les enregistreur automatique n'on pas normalement d'info sur la station
                        element.status = 'pending';
                        const stationCode: any = element.station;

                        try {
                            // Traitement des stations
                            const station: any = await StationService.findOneByCode(stationCode);
                            if (station && station.id) {
                                element.station_suivi_id = station.id;
                            } else {
                                console.log('la station ' + stationCode + ' n\'existe pas pour l\'instant, elle sera créee');
                                const stationData: any = {
                                    'code': stationCode,
                                    'titre': stationCode,
                                    'latitude': 1,
                                    'longitude': 1,
                                    'altitude': 1,
                                    'type_station_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                    'type_ouvrage_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                    'nappe_id': "d52d16b6-fcde-11e9-8f0b-362b9e155667",
                                };
                                const nouvelleStation: any = await StationService.insert(stationData);
                                if (nouvelleStation && nouvelleStation.id) {
                                    element.station_suivi_id = nouvelleStation.id;
                                }

                            }

                            if (element.heure_mesure === '') {
                                element.heure_mesure = null;
                            }

                            if (element.temperature === '') {
                                element.temperature = null;
                            }

                            if (element.ph === '') {
                                element.ph = null;
                            }

                            if (element.conductivite === '') {
                                element.conductivite = null;
                            }
                            if (element.niveau_statique_sol === '') {
                                element.niveau_statique_sol = null;
                            }
                            if (element.repere_sol === '') {
                                element.repere_sol = null;
                            }
                            if (element.repere_eau === '') {
                                element.repere_eau = null;
                            }

                            delete element['station'];

                            return element;

                        } catch (error) {
                            console.log(error.message);
                            res.status(500).json(error.message);
                        }



                    });

                    const piezometries: any = await Promise.all(promisedPiezometries);

                    await PiezometrieService.upload(result);
                    res.status(200).json(piezometries);
                } catch (er) {
                    console.log(er);
                    res.status(400).json('Une erreure inatendue s\'est produite durant l\'opération. Veuillez vérifier votre fichier SVP.');
                }

            }
        );

    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}

/**
 * préparer les données des piezometries existantes pour un export
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 * @returns {Promise < void >}
 */
export async function exportData(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
        const data: any[] = await PiezometrieService.export();

        res.status(200).json(data);
    } catch (error) {
        next(new HttpError(error.message.status, error.message));
    }
}
