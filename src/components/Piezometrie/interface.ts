import { IPiezometrieModel } from './model';

/**
 * @export
 * @interface IPiezometrieService
 */
export interface IPiezometrieService {

    /**
     * @param {IPiezometrieModel} IPiezometrieModel
     * @returns {Promise<IPiezometrieModel>}
     * @memberof IPiezometrieService
     */
    insert(IPiezometrieModel: IPiezometrieModel): Promise<IPiezometrieModel>;

    /**
     * @param {IPiezometrieModel} IPiezometrieModel
     * @returns {Promise<IPiezometrieModel>}
     * @memberof IPiezometrieService
     */
    update(IPiezometrieModel: IPiezometrieModel): Promise<IPiezometrieModel>;

    /**
     * @param {string} id
     * @returns {Promise<IPiezometrieModel>}
     * @memberof IPiezometrieService
     */
    findOne(id: string): Promise<IPiezometrieModel>;

    /**
     * @returns {Promise<IPiezometrieModel[]>}
     * @memberof IPiezometrieService
     */
    findAll(): Promise<IPiezometrieModel[]>;

    /**
     * @param {string} id
     * @returns {Promise<IPiezometrieModel>}
     * @memberof IPiezometrieService
     */
    remove(id: string): Promise<any>;

    /**
     * @param {any} data
     * @returns {Promise<any>}
     * @memberof IPiezometrieService
     */
    upload(data: any): Promise<any>;

    /**
     * @returns {Promise<any[]>}
     * @memberof IPiezometrieService
     */
    export(): Promise<any[]>;
}
