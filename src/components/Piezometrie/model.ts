import knex from '../../config/connection/database';

/**
 * @export
 * @interface IPiezometrieModel
 */
export interface IPiezometrieModel {
    id?: number;
}

const tableName: string = 'piezometrie';

class PiezometrieModel {
    async create(objet: IPiezometrieModel): Promise<IPiezometrieModel> {
        const id: number[] = await knex(tableName).insert(objet);

        return { ...objet, id: id[0] };
    }

    async findOne(objet: any): Promise<IPiezometrieModel> {
        return await knex.select().from(tableName).where(objet).first();
    }

    async findAll(): Promise<[IPiezometrieModel]> {
        return await knex.select('piezometrie.*', 'station.titre as station').from(tableName)
            .leftJoin('station', 'piezometrie.station_suivi_id', 'station.id');
    }

    async export(): Promise<[IPiezometrieModel]> {
        return await knex.select(
            'piezometrie.date_mesure', 'piezometrie.heure_mesure', 'piezometrie.temperature', 'piezometrie.repere_eau',
            'piezometrie.niveau_statique_sol', 'piezometrie.conductivite', 'piezometrie.ph', 'piezometrie.note', 'piezometrie.repere_sol',
            'piezometrie.status', 'piezometrie.niveau_dynamique_sol', 'piezometrie.pression', 'piezometrie.pulses',
            'station.titre as station',
            'region.titre as region',
            'enquete.titre as campagne').from(tableName)
            .leftJoin('station', 'piezometrie.station_suivi_id', 'station.id')
            .leftJoin('region', 'piezometrie.region_id', 'region.id')
            .leftJoin('enquete', 'piezometrie.enquete_id', 'enquete.id');
    }

    async findOneAndRemove(id: any): Promise<any> {
        return await knex(tableName).where('id', id).del();
    }

    async update(Piezometrie: IPiezometrieModel): Promise<IPiezometrieModel> {
        return await knex(tableName).where({ id: Piezometrie.id }).update(Piezometrie);
    }

    async upload(data: any): Promise<any> {
        return await knex(tableName).insert(data);
    }
}

export default new PiezometrieModel();
