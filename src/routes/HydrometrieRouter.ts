import { Router } from 'express';
import { HydrometrieComponent } from '../components';
import * as multer from 'multer';

const upload: any = multer({
    dest: 'uploads/imports/',
    fileFilter: (req, file, callback) => {
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(null, false);
            //return callback(new Error('Veuillez choisir un fichier excel'), false);
        }
        callback(null, true);
    }
});

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/collecte/hydrometrie
 * 
 * @swagger
 * /v1/collecte/hydrometrie:
 *   post:
 *      description: Cet endpoint permet de créer une nouvelle hydrometrie
 *      tags: ["hydrometrie"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: hydrometrie creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/hydrometrieSchema'
 *      responses:
 *        201:
 *          description: return created hydrometrie
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/hydrometrieSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', HydrometrieComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/collecte/hydrometrie
 * 
 * @swagger
 * /v1/collecte/hydrometrie:
 *   post:
 *      description: Cet endpoint permet de mettre à jour une hydrométrie existante
 *      tags: ["hydrometrie"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: hydrometrie update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/hydrometrieSchema'
 *      responses:
 *        201:
 *          description: return created hydrometrie
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/hydrometrieSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', HydrometrieComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/collecte/hydrometrie/:id
 * 
 * @swagger
 * /v1/collecte/hydrometrie/{id}:
 *  get:
 *    description: Cetb endpoint permet de récupérer une hydrometrie
 *    tags: ["hydrometrie"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: l'id de l'hydrometrie
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return hydrometrie by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/hydrometrieSchema'
 */
router.get('/:id', HydrometrieComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/collecte/hydrometrie
 * 
 * @swagger
 * /v1/collecte/hydrometrie:
 *   get:
 *     description: Récupérer la liste de toutes les hydrométries
 *     tags: ["hydrometrie"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: Tableau contenant la liste des hydrométries
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/hydrometrieSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', HydrometrieComponent.findAll);


/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/collecte/hydrometrie/:id
 * 
 * @swagger
 * /v1/collecte/hydrometrie/{id}:
 *  DELETE:
 *    description: Supprimer une hydrométrie à l'aide de son id
 *    tags: ["hydrometrie"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: l'id de l'hydrométrie
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted hydrométrie
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/hydrometrieSchema'
 */
router.delete('/:id', HydrometrieComponent.remove);

/**
 * Uploader un fichier contenant une liste d'hydrométries
 */
router.post('/upload', upload.single('file'), async (req, res, next) => {
    try {
        HydrometrieComponent.upload(req, res, next);
    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * Exporter la liste des hydrometrie qui sont dans la BD
 */
router.post('/export', HydrometrieComponent.exportData);

/**
 * @export {express.Router}
 */
export default router;
