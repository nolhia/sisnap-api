import { Router } from 'express';
import { ElementchimiqueComponent } from '../components';

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/parametre/elementchimiques
 * 
 * @swagger
 * /v1/parametre/elementchimiques:
 *   post:
 *      description: Cet endpoint permet de créer une nouveau parametre d'analyse(elementchimiques)
 *      tags: ["elementchimiques"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: elementchimiques creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/elementchimiquesSchema'
 *      responses:
 *        201:
 *          description: return created Element Chimique
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/elementchimiquesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', ElementchimiqueComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/parametre/elementchimiques
 * 
 * @swagger
 * /v1/parametre/elementchimiques:
 *   post:
 *      description: Cet endpoint permet de mettre à jour un element chimique existant
 *      tags: ["elementchimiques"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: element chimique update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/elementChimiqueSchema'
 *      responses:
 *        201:
 *          description: return created element chimique
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/elementChimiqueSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', ElementchimiqueComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/parametre/elementchimiques/:id
 * 
 * @swagger
 * /v1/parametre/elementchimiques/{id}:
 *  get:
 *    description: Cet endpoint permet de récupérer un element chimique par id
 *    tags: ["elementchimiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the element chimique id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return element chimique by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/elementChimiqueSchema'
 */
router.get('/:id', ElementchimiqueComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/parametre/elementchimiques
 * 
 * @swagger
 * /v1/parametre/elementchimiques:
 *   get:
 *     description: Cet endpoint permet de récupérer la liste de tous les elements chimiques
 *     tags: ["elementchimiques"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: Un tableau d'élément chimique
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/elementChimiqueSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', ElementchimiqueComponent.findAll);


/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/parametre/elementchimiques/:id
 * 
 * @swagger
 * /v1/parametre/elementchimiques/{id}:
 *  DELETE:
 *    description: Supprimer un element chimique
 *    tags: ["elementchimiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the element chimique id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted element chimique
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/elementChimiqueSchema'
 */
router.delete('/:id', ElementchimiqueComponent.remove);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/parametre/elementchimiques/analyseType/:analyseType
 * 
 * @swagger
 * /v1/parametre/elementchimiques/analyseType/{analyseType}:
 *  get:
 *    description: Récupérer un element chimique par type d'analyse
 *    tags: ["elementchimiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: analyseType
 *        description: the Type analyse id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return element chimique by type abalyse
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/ElementchimiquesSchema'
 */
router.get('/analyseType/:analyseType', ElementchimiqueComponent.findByAnalyse);


/**
 * @export {express.Router}
 */
export default router;
