import { Router } from 'express';
import { ZonegeographiqueComponent } from '../components';

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * GET method route
 * @example http://localhost:PORT/v1/zonegeographiques
 * 
 * @swagger
 * /v1/zonegeographiques:
 *   get:
 *     description: Get all stored zones geographique in Database
 *     tags: ["zonegeographiques"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: An array of zone geographiques
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/Zonegeographiques'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/all', ZonegeographiqueComponent.findAllZone);
router.get('/', ZonegeographiqueComponent.findRegions);
router.get('/departements/:id', ZonegeographiqueComponent.findDepartements);
router.get('/departements/communes/:id', ZonegeographiqueComponent.findCommunes);

/**
 * POST method route
 * @example http://localhost:PORT/v1/zonegeographiques
 * 
 * @swagger
 * /v1/zonegeographiques:
 *   post:
 *      description: Create new zone geographique
 *      tags: ["zonegeographiques"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: zone geographique creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ZonegeographiqueSchema'
 *            example:
 *              codet: xyz
 *              titre: Niamey ouest
 *      responses:
 *        201:
 *          description: return created zone geographique
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/ZonegeographiqueSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', ZonegeographiqueComponent.createRegion);
router.post('/departements', ZonegeographiqueComponent.createDepartement);
router.post('/departements/communes', ZonegeographiqueComponent.createCommune);
/**
 * PUT method route
 * @example http://localhost:PORT/v1/zonegeographiques
 * 
 * @swagger
 * /v1/zonegeographiques:
 *   post:
 *      description: Upate an existing zone geographique
 *      tags: ["zonegeographiques"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: zone geographique update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ZonegeographiqueSchema'
 *            example:
 *              codet: xyz
 *              titre: Niamey ouest
 *      responses:
 *        201:
 *          description: return created project
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/ZonegeographiqueSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', ZonegeographiqueComponent.updateRegion);
router.put('/departements', ZonegeographiqueComponent.updateDepartement);
router.put('/departements/communes', ZonegeographiqueComponent.updateCommune);
/**
 * GET method route 
 * @example http://localhost:PORT/v1/zonegeographiques/:id
 * 
 * @swagger
 * /v1/zonegeographiques/{id}:
 *  get:
 *    description: Get zone geographique by id
 *    tags: ["zonegeographiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the zone geographique id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return zone geographique by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/ZonegeographiqueSchema'
 */
router.get('/:id', ZonegeographiqueComponent.findOneRegion);
router.get('/departement/:id', ZonegeographiqueComponent.findOneDepartement);
router.get('/departements/commune/:id', ZonegeographiqueComponent.findOneCommune);


/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/parametre/zonegeographiques/:id
 * 
 * @swagger
 * /v1/parametre/zonegeographiques/{id}:
 *  DELETE:
 *    description: DELETE Type station by id
 *    tags: ["zonegeographiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted Type station
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/TypeStationSchema'
 */
router.delete('/:id', ZonegeographiqueComponent.removeRegion);
router.delete('/departements/:id', ZonegeographiqueComponent.removeDepartement);
router.delete('/departements/communes/:id', ZonegeographiqueComponent.removeCommune);
/**
 * @export {express.Router}
 */
export default router;
