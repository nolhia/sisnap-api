import { Router } from 'express';
import { RequeteComponent } from '../components';

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/requete/mesures
 * 
 * @swagger
 * /v1/requete/mesures:
 *   post:
 *      description: Create new analysechimiques
 *      tags: ["mesures"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: analysechimiques creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AnalysechimiquesSchema'
 *            example:
 *              date_prelevement: 2019-10-10
 *              heure_prelevement: 09:05
 *              date_analyse: 2019-10-10
 *              heure_analyse: 13:30
 *              element_chimique: {}
 *      responses:
 *        201:
 *          description: return created Analyse Chimique
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/AnalysechimiquesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/mesures', RequeteComponent.mesure); 

router.post('/graphique/piezometries', RequeteComponent.graphePiezometries); 
router.post('/graphique/pluviometries', RequeteComponent.graphePluviometries); 
router.post('/graphique/hydrometries', RequeteComponent.grapheHydrometries); 
/**
 * POST method route
 * @example http://localhost:PORT/v1/requete/analyses
 * 
 * @swagger
 * /v1/requete/analyses:
 *   post:
 *      description: Create new analysechimiques
 *      tags: ["analyses"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: analysechimiques creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AnalysechimiquesSchema'
 *            example:
 *              date_prelevement: 2019-10-10
 *              heure_prelevement: 09:05
 *              date_analyse: 2019-10-10
 *              heure_analyse: 13:30
 *              element_chimique: {}
 *      responses:
 *        201:
 *          description: return created Analyse Chimique
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/AnalysechimiquesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/analyses', RequeteComponent.analyse); 

/**
 * @export {express.Router}
 */
export default router;
