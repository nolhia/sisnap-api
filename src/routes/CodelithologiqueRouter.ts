import { Router } from 'express';
import { CodelithologiqueComponent } from '../components';
import * as multer from 'multer';

const upload = multer({
    dest: 'uploads/imports/',
    fileFilter: function (req, file, callback) { //file filter
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(null, false);
            //return callback(new Error('Veuillez choisir un fichier excel'), false);
        }
        callback(null, true);
    }
}); // multer configuration

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/parametre/codelithologiques
 * 
 * @swagger
 * /v1/parametre/codelithologiques:
 *   post:
 *      description: Cet endpoint permet de créer une nouvelle code lithologiques
 *      tags: ["codelithologiques"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: codelithologiques creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/codelithologiquesSchema'
 *      responses:
 *        201:
 *          description: return created code lithologique
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/codelithologiquesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', CodelithologiqueComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/parametre/codelithologiques
 * 
 * @swagger
 * /v1/parametre/codelithologiques:
 *   post:
 *      description: Cet endpoint permet de mettre à jour une code lithologique existante
 *      tags: ["codelithologiques"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: code lithologique update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/codelithologiqueSchema'
 *      responses:
 *        201:
 *          description: return update codelithologique
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/codelithologiqueSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', CodelithologiqueComponent.update);

/**
 * Uploader un fichier contenant une liste de code lithologique à importer
 */
router.post('/upload', upload.single('file'), async (req, res, next) => {
    try {
        CodelithologiqueComponent.upload(req, res, next);

    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * GET method route 
 * @example http://localhost:PORT/v1/parametre/codelithologiques/:id
 * 
 * @swagger
 * /v1/parametre/codelithologiques/{id}:
 *  get:
 *    description: Cet endpoint permet de récupérer une code lithologique
 *    tags: ["codelithologiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the code lithologiqueid
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return code lithologique by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/codelithologiqueSchema'
 */
router.get('/:id', CodelithologiqueComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/parametre/codelithologiques
 * 
 * @swagger
 * /v1/parametre/codelithologiques:
 *   get:
 *     description: Cet endpoint permet de récupérer la liste de toutes les codes lithologiques
 *     tags: ["codelithologiques"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: Tableau de codes lithologiques
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/codelithologiqueSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', CodelithologiqueComponent.findAll);


/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/parametre/codelithologiques/:id
 * 
 * @swagger
 * /v1/parametre/codelithologiques/{id}:
 *  DELETE:
 *    description: Cet endpoint permet de supprimer une code lithologique de la bd
 *    tags: ["codelithologiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the code lithologique id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted code lithologique
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/codelithologiqueSchema'
 */
router.delete('/:id', CodelithologiqueComponent.remove);


/**
 * @export {express.Router}
 */
export default router;
