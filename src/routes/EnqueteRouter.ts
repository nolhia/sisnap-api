import { Router } from 'express';
import { EnqueteComponent } from '../components';

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/parametre/enquetes
 * 
 * @swagger
 * /v1/parametre/enquetes:
 *   post:
 *      description: Cette endpoint permet de créer une nouvelle enquête
 *      tags: ["enquetes"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: enquetes creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/enquetesSchema'
 *      responses:
 *        201:
 *          description: return created Type enquête
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/enquetesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', EnqueteComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/parametre/enquetes
 * 
 * @swagger
 * /v1/parametre/enquetes:
 *   post:
 *      description: Cet endpoint permet de mettre à jour une enquête existante
 *      tags: ["enquetes"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: enquête update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/enquetesSchema'
 *      responses:
 *        201:
 *          description: return created enquete
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/enquetesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', EnqueteComponent.update);

/***
 * Cet endpoint permet de mettre à jour le status d'une enquête
 */
router.put('/validate', EnqueteComponent.updateStatus);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/parametre/enquetes/:id
 * 
 * @swagger
 * /v1/parametre/enquetes/{id}:
 *  get:
 *    description: Cet endpoint permet de récupérer une enquête
 *    tags: ["enquetes"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: id de l'enquête
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return enquête by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/enquetesSchema'
 */
router.get('/:id', EnqueteComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/parametre/enquetes
 * 
 * @swagger
 * /v1/parametre/enquetes:
 *   get:
 *     description: Cet endpoint permet de retourner la liste de toutes les enquêtes
 *     tags: ["enquetes"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: Tableau contenant la liste des enquêtes
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/enquetesSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', EnqueteComponent.findAll);


/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/parametre/enquetes/:id
 * 
 * @swagger
 * /v1/parametre/enquetes/{id}:
 *  DELETE:
 *    description: Cet endpoint permet de supprimer une enquête
 *    tags: ["enquetes"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: l'id de l'enquête à supprimer
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted enquête
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/enquetesSchema'
 */
router.delete('/:id', EnqueteComponent.remove);


/**
 * @export {express.Router}
 */
export default router;
