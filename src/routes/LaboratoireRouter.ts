import { Router } from 'express';
import { LaboratoireComponent } from '../components';

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/parametre/laboratoires
 * 
 * @swagger
 * /v1/parametre/laboratoires:
 *   post:
 *      description: Ajouter un nouveau laboratoire
 *      tags: ["laboratoires"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: laboratoires creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/laboratoiresSchema'
 *      responses:
 *        201:
 *          description: return created laboratoire
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/laboratoiresSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', LaboratoireComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/parametre/laboratoires
 * 
 * @swagger
 * /v1/parametre/laboratoires:
 *   post:
 *      description: Mettre à jour un laboratoire existant
 *      tags: ["laboratoires"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Laboratoire update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/laboratoiresSchema'
 *      responses:
 *        201:
 *          description: return created laboratoire
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/laboratoiresSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', LaboratoireComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/parametre/laboratoires/:id
 * 
 * @swagger
 * /v1/parametre/laboratoires/{id}:
 *  get:
 *    description: Récupérer un laboratoire à l'aide de son id
 *    tags: ["laboratoires"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: l'id du laboratoire
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return laboratoire by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/laboratoiresSchema'
 */
router.get('/:id', LaboratoireComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/parametre/laboratoires
 * 
 * @swagger
 * /v1/parametre/laboratoires:
 *   get:
 *     description: Récupérer la liste de tous les laboratoires
 *     tags: ["laboratoires"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: Tableau contenant la liste des laboratoires
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/laboratoiresSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', LaboratoireComponent.findAll);


/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/parametre/laboratoires/:id
 * 
 * @swagger
 * /v1/parametre/laboratoires/{id}:
 *  DELETE:
 *    description: Supprimer un laboraire à l'aide de son id
 *    tags: ["laboratoires"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the laboratoire id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted laboratoire
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/laboratoiresSchema'
 */
router.delete('/:id', LaboratoireComponent.remove);


/**
 * @export {express.Router}
 */
export default router;
