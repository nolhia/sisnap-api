import * as express from 'express';
import * as http from 'http';
import * as jwtConfig from '../config/middleware/jwtAuth';
import * as swaggerUi from 'swagger-ui-express';
import AuthRouter from './AuthRouter';
import UserRouter from './UserRouter';
import ZonegeographiqueRouter from './ZonegeographiqueRouter';
import TypeStationRouter from './TypeStationRouter';
import LaboratoireRouter from './LaboratoireRouter';
import StationRouter from './StationRouter';
import CoupegeologiqueRouter from './CoupegeologiqueRouter';
import NappeRouter from './NappeRouter';
import ElementchimiqueRouter from './ElementchimiqueRouter';
import TypeOuvrageRouter from './TypeOuvrageRouter';
import EnqueteRouter from './EnqueteRouter';
import PiezometrieRouter from './PiezometrieRouter';
import PluviometrieRouter from './PluviometrieRouter';
import HydrometrieRouter from './HydrometrieRouter';
import AnalysechimiqueRouter from './AnalysechimiqueRoutes';
import AnalyseisotopeRouter from './AnalyseisotopeRouter';
import AnalysepesticideRouter from './AnalysepesticideRouter';
import RequeteRouter from './RequeteRouter';
import AnalysebacteriologieRoute from './AnalysebacteriologieRoutes';
import CarteStatiqueRouter from './CarteStatiqueRouter';
import CodelithologiqueRouter from './CodelithologiqueRouter';

let swaggerDoc: Object;

try {
    swaggerDoc = require('../../swagger.json');
} catch (error) {
    console.log('***************************************************');
    console.log('  Seems like you doesn\`t have swagger.json file');
    console.log('  Please, run: ');
    console.log('  $ swagger-jsdoc -d swaggerDef.js -o swagger.json');
    console.log('***************************************************');
}

/**
 * @export
 * @param {express.Application} app
 */
export function init(app: express.Application): void {
    const router: express.Router = express.Router();

    /**
     * @description
     *  Forwards any requests to the /v1/users URI to our UserRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/users', UserRouter);

    /**
     * @description Forwards any requests to the /auth URI to our AuthRouter
     * @constructs
     */
    app.use('/api/v1/auth', AuthRouter);

    /**
     * @description
     *  Forwards any requests to the /v1/zonegeographiques URI to our ZonegeographiqueRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/parametre/regions', jwtConfig.isAuthenticated, ZonegeographiqueRouter);

    /**
     * @description
     *  Forwards any requests to the /v1/typestations URI to our TypeStationRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/typestations', jwtConfig.isAuthenticated, TypeStationRouter);

    /**
     * @description
     *  Forwards any requests to the /v1/parametre/laboratoires URI to our LaboratoireRouter
     *  Also, check if user authenticated
     * @constructs
     */

    app.use('/api/v1/parametre/laboratoires', jwtConfig.isAuthenticated, LaboratoireRouter);
    /**
     * @description
     *  Forwards any requests to the /v1/parametre/laboratoires URI to our LaboratoireRouter
     *  Also, check if user authenticated
     * @constructs
     */

    app.use('/api/v1/parametre/codelithologique', jwtConfig.isAuthenticated, CodelithologiqueRouter);
    /**
     * @description
     *  Forwards any requests to the /v1/parametre/codelithologique URI to our CodelithologiqueRouter
     *  Also, check if user authenticated
     * @constructs
     */

    app.use('/api/v1/parametre/enquetes', jwtConfig.isAuthenticated, EnqueteRouter);
    /**
     * @description
     *  Forwards any requests to the /v1/parametre/coupegeologiques URI to our LaboratoireRouter
     *  Also, check if user authenticated
     * @constructs
     */

    app.use('/api/v1/parametre/coupegeologiques', jwtConfig.isAuthenticated, CoupegeologiqueRouter);
    /**
     * @description
     *  Forwards any requests to the /v1/parametre/nappes URI to our NappeRouter
     *  Also, check if user authenticated
     * @constructs
     */

    app.use('/api/v1/parametre/nappes', jwtConfig.isAuthenticated, NappeRouter);
    /**
     * @description
     *  Forwards any requests to the /v1/parametre/elementchimiques URI to our NappeRouter
     *  Also, check if user authenticated
     * @constructs
     */

    app.use('/api/v1/parametre/elementchimiques', jwtConfig.isAuthenticated, ElementchimiqueRouter);
    /**
     * @description
     *  Forwards any requests to the /v1/stations URI to our StationRouter
     *  Also, check if user authenticated
     * @constructs
     */

    app.use('/api/v1/stations', jwtConfig.isAuthenticated, StationRouter);
    /**
     * @description
     *  Forwards any requests to the /v1/typeouvrages URI to our TypeOuvrageRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/typeouvrages', jwtConfig.isAuthenticated, TypeOuvrageRouter);

    /**
     * @description
     *  Forwards any requests to the /v1/collecte/piezometrie URI to our NappeRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/collecte/piezometrie', jwtConfig.isAuthenticated, PiezometrieRouter);
    
    /**
     * @description
     *  Forwards any requests to the /v1/collecte/hydrometrie URI to our NappeRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/collecte/hydrometrie', jwtConfig.isAuthenticated, HydrometrieRouter);

    /**
     * @description
     *  Forwards any requests to the /v1/collecte/pluviometrie URI to our NappeRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/collecte/pluviometrie', jwtConfig.isAuthenticated, PluviometrieRouter);

    /**
     * @description
     *  Forwards any requests to the /v1/analyse/chimiques URI to our NappeRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/collecte/analyse/chimiques', jwtConfig.isAuthenticated, AnalysechimiqueRouter);

    /**
     * @description
     *  Forwards any requests to the /v1/analyse/isotope URI to our AnalyseisotopeRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/collecte/analyse/isotope', jwtConfig.isAuthenticated, AnalyseisotopeRouter);

    /**
     * @description
     *  Forwards any requests to the /v1/analyse/pesticide URI to our AnalysepesticideRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/collecte/analyse/pesticide', jwtConfig.isAuthenticated, AnalysepesticideRouter);

    /**
     * @description
     *  Forwards any requests to the /v1/analyse/chimiques URI to our NappeRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/requete', jwtConfig.isAuthenticated, RequeteRouter);

    /**
     * @description
     *  Forwards any requests to the /v1/analyse/bacteriologie URI to our AnalysebacteriologieeRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/collecte/analyse/bacteriologie', jwtConfig.isAuthenticated, AnalysebacteriologieRoute);

     /**
     * @description
     *  Forwards any requests to the /v1/cartestatique URI to our NappeRouter
     *  Also, check if user authenticated
     * @constructs
     */
    app.use('/api/v1/cartes/statiques', jwtConfig.isAuthenticated, CarteStatiqueRouter);

    /**
     * @description
     *  If swagger.json file exists in root folder, shows swagger api description
     *  else send commands, how to get swagger.json file
     * @constructs
     */
    if (swaggerDoc) {
        app.use('/docs', swaggerUi.serve);
        app.get('/docs', swaggerUi.setup(swaggerDoc));
    } else {
        app.get('/docs', (req, res) => {
            res.send('<p>Seems like you doesn\'t have <code>swagger.json</code> file.</p>' +
                '<p>For generate doc file use: <code>swagger-jsdoc -d swaggerDef.js -o swagger.json</code> in terminal</p>' +
                '<p>Then, restart your application</p>');
        });
    }

    /** 
     * @description No results returned mean the object is not found
     * @constructs
     */
    app.use((req, res, next) => {
        res.status(404).send(http.STATUS_CODES[404]);
    });

    /**
     * @constructs all routes
     */
    app.use(router);
}
