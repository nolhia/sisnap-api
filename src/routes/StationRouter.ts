import { Router } from 'express';
import { StationComponent } from '../components';
import * as multer from 'multer';

const upload: any = multer({
    dest: 'uploads/imports/',
    fileFilter: (req, file, callback) => {
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(null, false);
            //return callback(new Error('Veuillez choisir un fichier excel'), false);
        }
        callback(null, true);
    }
});

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/stations
 * 
 * @swagger
 * /v1/stations:
 *   post:
 *      description: Create new Station
 *      tags: ["stations"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Station creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/StationSchema'
 *            example:
 *              codet: TS001
 *              titre: Station Niamey ouest
 *              latitude: 00900122;
 *              longitude: 123423209;
 *              altitude: 6563733;
 *              profondeur_totale: 038474;
 *              repere_sol: 78373;
 *              type_enregistrement: manuel;
 *              note: ceci est une note;
 *              zone_geographique: 556ab-eg56h0-th6774;
 *      responses:
 *        201:
 *          description: return created Station
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/StationSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', StationComponent.create);


/**
 * PUT method route
 * @example http://localhost:PORT/v1/stations
 * 
 * @swagger
 * /v1/stations:
 *   post:
 *      description: Upate an existing station
 *      tags: ["stations"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: station update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/StationSchema'
 *            example:
 *              codet: TS001
 *              titre: Station Niamey ouest
 *      responses:
 *        201:
 *          description: return updated station
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/StationSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', StationComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/stations/:id
 * 
 * @swagger
 * /v1/stations/{id}:
 *  get:
 *    description: Get station by id
 *    tags: ["stations"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return station by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/StationSchema'
 */
router.get('/:id', StationComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/stations
 * 
 * @swagger
 * /v1/stations:
 *   get:
 *     description: Get all stored stations in Database
 *     tags: ["stations"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: An array of stations
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/StationSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', StationComponent.findAll);

/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/stations/:id
 * 
 * @swagger
 * /v1/stations/{id}:
 *  DELETE:
 *    description: DELETE station by id
 *    tags: ["stations"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted station
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/StationSchema'
 */
router.delete('/:id', StationComponent.remove);

router.post('/upload', upload.single('file'), async (req, res, next) => {
    try {
        StationComponent.upload(req, res, next);
    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * Exporter la liste des stations de suivis qui sont dans la BD
 */
router.post('/export', StationComponent.exportData);

/**
 * @export {express.Router}
 */
export default router;
