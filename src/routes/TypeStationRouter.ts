import { Router } from 'express';
import { TypeStationComponent } from '../components';

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/typestations
 * 
 * @swagger
 * /v1/typestations:
 *   post:
 *      description: Create new Type Station
 *      tags: ["typestations"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Type Station creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/TypeStationSchema'
 *            example:
 *              codet: 01
 *              titre: Station Niamey ouest
 *      responses:
 *        201:
 *          description: return created Type Station
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/TypeStationSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', TypeStationComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/typestations
 * 
 * @swagger
 * /v1/typestations:
 *   post:
 *      description: Upate an existing Type station
 *      tags: ["typestations"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Type station update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/TypeStationSchema'
 *            example:
 *              codet: AB
 *              titre: Station Niamey ouest
 *      responses:
 *        201:
 *          description: return updated type station
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/TypeStationSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', TypeStationComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/typestations/:id
 * 
 * @swagger
 * /v1/typestations/{id}:
 *  get:
 *    description: Get Type station by id
 *    tags: ["typestations"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Type station by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/TypeStationSchema'
 */
router.get('/:id', TypeStationComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/typestations
 * 
 * @swagger
 * /v1/typestations:
 *   get:
 *     description: Get all stored Type stations in Database
 *     tags: ["typestations"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: An array of Type stations
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/TypeStationSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', TypeStationComponent.findAll);

/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/typestations/:id
 * 
 * @swagger
 * /v1/typestations/{id}:
 *  DELETE:
 *    description: DELETE Type station by id
 *    tags: ["typestations"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted Type station
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/TypeStationSchema'
 */
router.delete('/:id', TypeStationComponent.remove);


/**
 * @export {express.Router}
 */
export default router;
