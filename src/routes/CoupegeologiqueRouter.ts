import { Router } from 'express';
import { CoupegeologiqueComponent } from '../components';

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/parametre/coupegeologiques
 * 
 * @swagger
 * /v1/parametre/coupegeologiques:
 *   post:
 *      description: Cet endpoint permet d'ajouter une nouvelle coupe geologique dans la BD
 *      tags: ["coupegeologiques"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: coupegeologiques creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/coupegeologiquesSchema'
 *      responses:
 *        201:
 *          description: return created Type Coupe Geologique
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/coupegeologiqueSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', CoupegeologiqueComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/parametre/coupegeologiques
 * 
 * @swagger
 * /v1/parametre/coupegeologiques:
 *   post:
 *      description: Cet endpoint permet de mettre à jour une coupe geologique existante
 *      tags: ["coupegeologiques"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: update Coupe Geologique request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/coupegeologiqueSchema'
 *      responses:
 *        201:
 *          description: return created project
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/coupegeologiqueSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', CoupegeologiqueComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/parametre/coupegeologiques/:id
 * 
 * @swagger
 * /v1/parametre/coupegeologiques/{id}:
 *  get:
 *    description: Cet endpoint permet de récupérer une coupe geologique
 *    tags: ["coupegeologiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the coupe geologique id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return coupe geologique by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/coupegeologiqueSchema'
 */
router.get('/one/:id', CoupegeologiqueComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/parametre/coupegeologiques
 * 
 * @swagger
 * /v1/parametre/coupegeologiques:
 *   get:
 *     description: Cet endpoint permet de récupérer la liste de toutes les coupes géoloqiues
 *     tags: ["coupegeologiques"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: Un tableau de coupe geologique
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/CoupeGeologiqueSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', CoupegeologiqueComponent.findAll);

/**
 * Cet endpoint permet de récupérer la liste de toutes les code lithologiques existante dans la BD
 */
router.get('/codeslithologiques', CoupegeologiqueComponent.getCodesLithologiques);

/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/parametre/coupegeologiques/:id
 * 
 * @swagger
 * /v1/parametre/coupegeologiques/{id}:
 *  DELETE:
 *    description: Cet endpoint permet de supprimer une coupe geoloqique
 *    tags: ["coupegeologiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: l'id de la coupe geologique
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted coupe geologique
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/CoupeGeologiqueSchema'
 */
router.delete('/:id', CoupegeologiqueComponent.remove);


/**
 * @export {express.Router}
 */
export default router;
