import { Router } from 'express';
import { AnalyseisotopeComponent } from '../components';
import * as multer from 'multer';

const upload: any = multer({
    dest: 'uploads/imports/',
    fileFilter: (req, file, callback) => {
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(null, false);
            //return callback(new Error('Veuillez choisir un fichier excel'), false);
        }
        callback(null, true);
    }
});


/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/analyse/isotope
 * 
 * @swagger
 * /v1/analyse/isotope:
 *   post:
 *      description: Cet endpoint permet de créer une nouvelle analyse isotope
 *      tags: ["analyseisotope"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: AnalyseIsotopes creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AnalyseIsotopesSchema'
 *            example:
 *              date_prelevement: 2019-10-10
 *              heure_prelevement: 09:05
 *              date_analyse: 2019-10-10
 *              heure_analyse: 13:30
 *              element_chimique: {}
 *      responses:
 *        201:
 *          description: return created analyse isotope
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/AnalyseIsotopesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', AnalyseisotopeComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/analyse/isotope
 * 
 * @swagger
 * /v1/analyse/isotope:
 *   post:
 *      description: cet endpoint permet de mettre à jour une analyse isotope existante
 *      tags: ["analyseisotope"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: analyse isotope update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AnalyseIsotopesSchema'
 *            example:
 *              date_prelevement: 2019-10-10
 *              heure_prelevement: 09:05
 *              date_analyse: 2019-10-10
 *              heure_analyse: 13:30
 *              element_chimique: {}
 *      responses:
 *        201:
 *          description: return created AnalyseIsotope
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/AnalyseIsotopesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', AnalyseisotopeComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/analyse/isotope/:id
 * 
 * @swagger
 * /v1/analyse/isotope/{id}:
 *  get:
 *    description: cet endpoint permet de recupérer une analyse isotope par id
 *    tags: ["analyseisotope"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the analyse isotope id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Type station by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/AnalyseIsotopesSchema'
 */
router.get('/:id', AnalyseisotopeComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/analyse/isotope
 * 
 * @swagger
 * /v1/analyse/AnalyseIsotopes:
 *   get:
 *     description: Cet endpoint permet de récupérer la liste de toutes les analyses isotopes
 *     tags: ["analyseisotope"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: An array of analyse isotopes
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/AnalyseIsotopesSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', AnalyseisotopeComponent.findAll);

/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/analyse/isotope/:id
 * 
 * @swagger
 * /v1/analyse/isotope/{id}:
 *  DELETE:
 *    description: Cet endpoint permet de supprimer une analyse isotope
 *    tags: ["analyseisotope"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: The Analyse Isotope id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted analyse isotope
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/AnalyseIsotopSchema'
 */
router.delete('/:id', AnalyseisotopeComponent.remove);

/**
 * Uploader un fichier contenant une liste d'analyses isotopes à importer
 */
router.post('/upload', upload.single('file'), async (req, res, next) => {
    try {
        AnalyseisotopeComponent.upload(req, res, next);
    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * Exporter la liste des analyses isotopes qui sont dans la BD
 */
router.post('/export', AnalyseisotopeComponent.exportData);

/**
 * @export {express.Router}
 */
export default router;
