import { Router } from 'express';
import { PiezometrieComponent } from '../components';
import * as multer from 'multer';

const upload: any = multer({
    dest: 'uploads/imports/',
    fileFilter: (req, file, callback) => {
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(null, false);
            //return callback(new Error('Veuillez choisir un fichier excel'), false);
        }
        callback(null, true);
    }
});

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/collecte/piezometrie
 * 
 * @swagger
 * /v1/collecte/piezometrie:
 *   post:
 *      description: Create new piezometrie
 *      tags: ["piezometrie"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: piezometrie creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/piezometrieSchema'
 *            example:
 *              codet: TS001
 *              titre: Station Niamey ouest
 *      responses:
 *        201:
 *          description: return created Type Station
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/piezometrieSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', PiezometrieComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/collecte/piezometrie
 * 
 * @swagger
 * /v1/collecte/piezometrie:
 *   post:
 *      description: Upate an existing Type station
 *      tags: ["piezometrie"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Type station update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/TypeStationSchema'
 *            example:
 *              codet: TS001
 *              titre: Station Niamey ouest
 *      responses:
 *        201:
 *          description: return created project
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/TypeStationSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', PiezometrieComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/collecte/piezometrie/:id
 * 
 * @swagger
 * /v1/collecte/piezometrie/{id}:
 *  get:
 *    description: Get Type station by id
 *    tags: ["piezometrie"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Type station by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/TypeStationSchema'
 */
router.get('/:id', PiezometrieComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/collecte/piezometrie
 * 
 * @swagger
 * /v1/collecte/piezometrie:
 *   get:
 *     description: Get all stored Type stations in Database
 *     tags: ["piezometrie"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: An array of Type stations
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/TypeStationSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', PiezometrieComponent.findAll);


/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/collecte/piezometrie/:id
 * 
 * @swagger
 * /v1/collecte/piezometrie/{id}:
 *  DELETE:
 *    description: DELETE Type station by id
 *    tags: ["piezometrie"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted Type station
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/TypeStationSchema'
 */
router.delete('/:id', PiezometrieComponent.remove);

/**
 * Uploader un fichier contenant une liste de piezometrie
 */
router.post('/upload', upload.single('file'), async (req, res, next) => {
    try {
        PiezometrieComponent.upload(req, res, next);
    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * Uploader un fichier contenant une liste de piezometrie d'enregistreur automatique
 */
router.post('/upload/enregistreur', upload.single('file'), async (req, res, next) => {
    try {
        PiezometrieComponent.uploadEnregistreur(req, res, next);
    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * Exporter la liste des Piezometries qui sont dans la BD
 */
router.post('/export', PiezometrieComponent.exportData);

/**
 * @export {express.Router}
 */
export default router;
