import { Router } from 'express';
import { CarteStatiqueComponent } from '../components';
import * as multer from 'multer';

const upload = multer({ dest: 'uploads/cartes/'}); // multer configuration

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/cartes/statiques
 * 
 * @swagger
 * /v1/cartes/statiques:
 *   post:
 *      description: Create new cartestatique
 *      tags: ["cartestatique"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: cartestatique creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/cartestatiqueSchema'
 *            example:
 *              codet: TS001
 *              titre: Station Niamey ouest
 *      responses:
 *        201:
 *          description: return created Type Station
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/cartestatiqueSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', upload.single('file'), async (req, res, next) => {
    try {  
        CarteStatiqueComponent.create(req, res, next); 
    } catch (err) {
        res.sendStatus(400);
    }
})

/**
 * PUT method route
 * @example http://localhost:PORT/v1/cartes/statiques
 * 
 * @swagger
 * /v1/cartes/statiques:
 *   post:
 *      description: Upate an existing Type station
 *      tags: ["cartestatique"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Type station update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/CarteStatiqueSchema'
 *            example:
 *              codet: TS001
 *              titre: Station Niamey ouest
 *      responses:
 *        201:
 *          description: return created project
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/CarteStatiqueSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', CarteStatiqueComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/cartestatique/:id
 * 
 * @swagger
 * /v1/cartes/statiques/{id}:
 *  get:
 *    description: Get Type station by id
 *    tags: ["cartestatique"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Type station by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/CarteStatiqueSchema'
 */
router.get('/:id', CarteStatiqueComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/cartestatique
 * 
 * @swagger
 * /v1/cartes/statiques:
 *   get:
 *     description: Get all stored Type stations in Database
 *     tags: ["cartestatique"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: An array of Type stations
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/CarteStatiqueSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', CarteStatiqueComponent.findAll);


/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/cartestatique/:id
 * 
 * @swagger
 * /v1/cartes/statiques/{id}:
 *  DELETE:
 *    description: DELETE Type station by id
 *    tags: ["cartestatique"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted Type station
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/CarteStatiqueSchema'
 */
router.delete('/:id', CarteStatiqueComponent.remove);


/**
 * @export {express.Router}
 */
export default router;
