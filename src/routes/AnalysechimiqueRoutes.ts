import { Router } from 'express';
import { AnalysechimiqueComponent } from '../components';
import * as multer from 'multer';

const upload: any = multer({
    dest: 'uploads/imports/',
    fileFilter: (req, file, callback) => {
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(null, false);
            //return callback(new Error('Veuillez choisir un fichier excel'), false);
        }

        callback(null, true);
    }
});

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/collecte/analyse/chimiques
 * 
 * @swagger
 * /v1/analyse/chimiques:
 *   post:
 *      description: Cet endpoint permet de créer une nouvelle analyse chimique
 *      tags: ["analysechimiques"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: analysechimiques creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AnalysechimiquesSchema'
 *            example:
 *              date_prelevement: 2019-10-10
 *              heure_prelevement: 09:05
 *              date_analyse: 2019-10-10
 *              heure_analyse: 13:30
 *              element_chimique: {}
 *      responses:
 *        201:
 *          description: return created Analyse Chimique
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/AnalysechimiquesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', AnalysechimiqueComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/collecte/analyse/chimiques
 * 
 * @swagger
 * /v1/analyse/chimiques:
 *   post:
 *      description: Cet endpoint permet de mettre à jour une analyse chimique existante
 *      tags: ["analysechimiques"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: analyse chimique update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AnalysechimiquesSchema'
 *            example:
 *              date_prelevement: 2019-10-10
 *              heure_prelevement: 09:05
 *              date_analyse: 2019-10-10
 *              heure_analyse: 13:30
 *              element_chimique: {}
 *      responses:
 *        201:
 *          description: return created analysechimique
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/AnalysechimiquesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', AnalysechimiqueComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/collecte/analyse/chimiques/:id
 * 
 * @swagger
 * /v1/analyse/chimiques/{id}:
 *  get:
 *    description: recupérer une analyse chimiques par id
 *    tags: ["analysechimiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: l'id de l'analyse chimique
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Analyse chimiques by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/AnalysechimiquesSchema'
 */
router.get('/:id', AnalysechimiqueComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/collecte/analyse/chimiques
 * 
 * @swagger
 * /v1/analyse/analysechimiques:
 *   get:
 *     description: Cet endpoint permet de récupérer la liste de toutes les analyses chimiques
 *     tags: ["analysechimiques"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: An array of analyse chimiques
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/AnalysechimiquesSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', AnalysechimiqueComponent.findAll);

/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/collecte/analyse/chimiques/:id
 * 
 * @swagger
 * /v1/analyse/chimiques/{id}:
 *  DELETE:
 *    description: Cet enpoint permet de supprimer une analyse chimique
 *    tags: ["analysechimiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: The Analyse chimique id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted Analyse chimique
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/AnalysechimiquesSchema'
 */
router.delete('/:id', AnalysechimiqueComponent.remove);

/**
 * Uploader un fichier contenant une liste d'analyses chimiques à importer
 */
router.post('/upload', upload.single('file'), async (req, res, next) => {
    try {
        AnalysechimiqueComponent.upload(req, res, next);
    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * Exporter la liste des analyses chimiques qui sont dans la BD
 */
router.post('/export', AnalysechimiqueComponent.exportData);


/**
 * GET method route 
 * @example http://localhost:PORT/v1/collecte/analyse/chimiques/report/:id
 * 
 * @swagger
 * /v1/analyse/chimiques/report/{id}:
 *  get:
 *    description: recupérer une analyse chimiques par id
 *    tags: ["analysechimiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: l'id de l'analyse chimique
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Analyse chimiques by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/AnalysechimiquesSchema'
 */
router.get('/report/:id', AnalysechimiqueComponent.report);

/**
 * @export {express.Router}
 */
export default router;
