import { Router } from 'express';
import { AnalysebacteriologieComponent } from '../components';
import * as multer from 'multer';

const upload: any = multer({
    dest: 'uploads/imports/',
    fileFilter: (req, file, callback) => {
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(null, false);
            //return callback(new Error('Veuillez choisir un fichier excel'), false);
        }
        callback(null, true);
    }
});

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/analyse/bacteriologie
 * 
 * @swagger
 * /v1/analyse/bacteriologie:
 *   post:
 *      description: Cet endpoint permet de créer une analyse bacteriologie
 *      tags: ["analysebacteriologie"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: analysebacteriologie creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AnalysebacteriologieSchema'
 *            example:
 *              date_prelevement: 2019-10-10
 *              heure_prelevement: 09:05
 *              date_analyse: 2019-10-10
 *              heure_analyse: 13:30
 *              element_chimique: {}
 *      responses:
 *        201:
 *          description: return created Analyse Bacteriologie
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/AnalysebacteriologieSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', AnalysebacteriologieComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/analyse/bacteriologie
 * 
 * @swagger
 * /v1/analyse/bacteriologie:
 *   post:
 *      description: Cet endpoint permet de mettre à jour une analysebacteriologie
 *      tags: ["analysebacteriologie"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: analyse bacteriologie update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AnalysebacteriologieSchema'
 *            example:
 *              date_prelevement: 2019-10-10
 *              heure_prelevement: 09:05
 *              date_analyse: 2019-10-10
 *              heure_analyse: 13:30
 *              element_chimique: {}
 *      responses:
 *        201:
 *          description: return updated analysebacteriologie
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/AnalysebacteriologieSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', AnalysebacteriologieComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/analyse/bacteriologie/:id
 * 
 * @swagger
 * /v1/analyse/bacteriologie/{id}:
 *  get:
 *    description: recupérer une analyse bacteriologie par id
 *    tags: ["analysebacteriologie"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: l'id de l'analyse bacteriologie
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Analyse bacteriologie by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/AnalysebacteriologieSchema'
 */
router.get('/:id', AnalysebacteriologieComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/analyse/bacteriologie
 * 
 * @swagger
 * /v1/analyse/analysebacteriologie:
 *   get:
 *     description: Cet endpoint permet de récupérer la liste de toutes les analyses bacteriologiques
 *     tags: ["analysebacteriologie"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: Tableau d'analyses bacteriologiques
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/AnalysebacteriologieSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', AnalysebacteriologieComponent.findAll);

/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/analyse/bacteriologie/:id
 * 
 * @swagger
 * /v1/analyse/bacteriologie/{id}:
 *  DELETE:
 *    description: Supprimer une Analyse bacteriologique
 *    tags: ["analysebacteriologie"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: L'id de Analyse bacteriologie
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: retourne l'analyse bacteriologique supprimée
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/AnalysebacteriologieSchema'
 */
router.delete('/:id', AnalysebacteriologieComponent.remove);

/**
 * Cet endpoint permet d'uploader un fichier contenant une liste d'analyses bacteriologiques à importer
 */
router.post('/upload', upload.single('file'), async (req, res, next) => {
    try {
        AnalysebacteriologieComponent.upload(req, res, next);
    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * Exporter la liste des analyses bacteriologiques qui sont dans la BD
 */
router.post('/export', AnalysebacteriologieComponent.exportData);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/collecte/analyse/bacteriologie/report/:id
 * 
 * @swagger
 * /v1/analyse/bacteriologie/report/{id}:
 *  get:
 *    description: recupérer une analyse bacteriologique par id
 *    tags: ["analysebacteriologiques"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: l'id de l'analyse bacteriologique
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Analyse bacteriologique by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/AnalysebacteriologiqueSchema'
 */
router.get('/report/:id', AnalysebacteriologieComponent.report);

/**
 * @export {express.Router}
 */
export default router;

