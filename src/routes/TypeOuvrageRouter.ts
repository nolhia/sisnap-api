import { Router } from 'express';
import { TypeOuvrageComponent } from '../components';

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/typeouvrages
 * 
 * @swagger
 * /v1/typeouvrages:
 *   post:
 *      description: Create new Type Ouvrage
 *      tags: ["typeouvrage"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Type Ouvrage creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/TypeOuvrageSchema'
 *            example:
 *              codet: 01
 *              titre: Ouvrage 01
 *      responses:
 *        201:
 *          description: return created Type Ouvrage
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/TypeOuvrageSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', TypeOuvrageComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/typeouvrages
 * 
 * @swagger
 * /v1/typeouvrages:
 *   post:
 *      description: Upate an existing Type ouvrage
 *      tags: ["typeouvrages"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Type ouvrage update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/TypeOuvrageSchema'
 *            example:
 *              codet: 01
 *              titre: Ouvrage 01 update
 *      responses:
 *        201:
 *          description: return updated type ouvrage
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/TypeOuvrageSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', TypeOuvrageComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/typeouvrages/:id
 * 
 * @swagger
 * /v1/typeouvrages/{id}:
 *  get:
 *    description: Get Type ouvrage by id
 *    tags: ["typeouvrages"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type ouvrage id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Type ouvrage by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/TypeOuvrageSchema'
 */
router.get('/:id', TypeOuvrageComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/typeouvrages
 * 
 * @swagger
 * /v1/typeouvrages:
 *   get:
 *     description: Get all stored Type ouvrages in Database
 *     tags: ["typeouvrages"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: An array of Type ouvrages
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/TypeOuvrageSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', TypeOuvrageComponent.findAll);

/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/typeouvrages/:id
 * 
 * @swagger
 * /v1/typeouvrages/{id}:
 *  DELETE:
 *    description: DELETE Type ouvrage by id
 *    tags: ["typeouvrages"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type ouvrage id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted Type ouvrage
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/TypeOuvrageSchema'
 */
router.delete('/:id', TypeOuvrageComponent.remove);

/**
 * @export {express.Router}
 */
export default router;
