import { Router } from 'express';
import { NappeComponent } from '../components';
import * as multer from 'multer';

const upload = multer({
    dest: 'uploads/imports/',
    fileFilter: function (req, file, callback) { //file filter
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(null, false);
            //return callback(new Error('Veuillez choisir un fichier excel'), false);
        }
        callback(null, true);
    }
}); // multer configuration

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/parametre/nappes
 * 
 * @swagger
 * /v1/parametre/nappes:
 *   post:
 *      description: Ajouter une nouvelle nappe
 *      tags: ["nappes"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: nappes creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/nappesSchema'
 *      responses:
 *        201:
 *          description: return created nappe
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/nappesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', NappeComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/parametre/nappes
 * 
 * @swagger
 * /v1/parametre/nappes:
 *   post:
 *      description: Mettre à jour une nappe existante
 *      tags: ["nappes"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Nappe update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/nappesSchema'
 *      responses:
 *        201:
 *          description: return created Nappe
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/nappesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', NappeComponent.update);
// router.post('/upload', NappeComponent.upload);

router.post('/upload', upload.single('file'), async (req, res, next) => {
    try {
        NappeComponent.upload(req, res, next);

    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * GET method route 
 * @example http://localhost:PORT/v1/parametre/nappes/:id
 * 
 * @swagger
 * /v1/parametre/nappes/{id}:
 *  get:
 *    description: Récupérer une nappe à l'aide de son id
 *    tags: ["nappes"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the nappe id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return nappe by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/nappesSchema'
 */
router.get('/:id', NappeComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/parametre/nappes
 * 
 * @swagger
 * /v1/parametre/nappes:
 *   get:
 *     description: Récupérer la liste de toutes les nappes de la base des données
 *     tags: ["nappes"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: Un Tableau contenant la liste de tous les nappes
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/nappesSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', NappeComponent.findAll);


/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/parametre/nappes/:id
 * 
 * @swagger
 * /v1/parametre/nappes/{id}:
 *  DELETE:
 *    description: Supprimer une nappe à l'aide de son Id
 *    tags: ["nappes"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the nappe id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted nappe
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/nappesSchema'
 */
router.delete('/:id', NappeComponent.remove);


/**
 * @export {express.Router}
 */
export default router;
