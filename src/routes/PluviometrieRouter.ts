import { Router } from 'express';
import { PluviometrieComponent } from '../components';
import * as multer from 'multer';

const upload: any = multer({
    dest: 'uploads/imports/',
    fileFilter: (req, file, callback) => {
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(null, false);
            //return callback(new Error('Veuillez choisir un fichier excel'), false);
        }
        callback(null, true);
    }
});


/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/collecte/pluviometrie
 * 
 * @swagger
 * /v1/collecte/pluviometrie:
 *   post:
 *      description: Create new pluviometrie
 *      tags: ["pluviometrie"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: pluviometrie creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/pluviometrieSchema'
 *            example:
 *              codet: TS001
 *              titre: Station Niamey ouest
 *      responses:
 *        201:
 *          description: return created Type Station
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/pluviometrieSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', PluviometrieComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/collecte/pluviometrie
 * 
 * @swagger
 * /v1/collecte/pluviometrie:
 *   post:
 *      description: Upate an existing Type station
 *      tags: ["pluviometrie"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Type station update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/TypeStationSchema'
 *            example:
 *              codet: TS001
 *              titre: Station Niamey ouest
 *      responses:
 *        201:
 *          description: return created project
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/TypeStationSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', PluviometrieComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/collecte/pluviometrie/:id
 * 
 * @swagger
 * /v1/collecte/pluviometrie/{id}:
 *  get:
 *    description: Get Type station by id
 *    tags: ["pluviometrie"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Type station by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/TypeStationSchema'
 */
router.get('/:id', PluviometrieComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/collecte/pluviometrie
 * 
 * @swagger
 * /v1/collecte/pluviometrie:
 *   get:
 *     description: Get all stored Type stations in Database
 *     tags: ["pluviometrie"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: An array of Type stations
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/TypeStationSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', PluviometrieComponent.findAll);


/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/collecte/pluviometrie/:id
 * 
 * @swagger
 * /v1/collecte/pluviometrie/{id}:
 *  DELETE:
 *    description: DELETE Type station by id
 *    tags: ["pluviometrie"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the Type station id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted Type station
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/TypeStationSchema'
 */
router.delete('/:id', PluviometrieComponent.remove);

/**
 * Uploader un fichier contenant une liste d'analyses chimique
 */
router.post('/upload', upload.single('file'), async (req, res, next) => {
    try {
        PluviometrieComponent.upload(req, res, next);
    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * Exporter la liste des hydrometrie qui sont dans la BD
 */
router.post('/export', PluviometrieComponent.exportData);

/**
 * @export {express.Router}
 */
export default router;
