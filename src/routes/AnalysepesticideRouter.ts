import { Router } from 'express';
import { AnalysepesticideComponent } from '../components';
import * as multer from 'multer';

const upload: any = multer({
    dest: 'uploads/imports/',
    fileFilter: (req, file, callback) => {
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(null, false);
            //return callback(new Error('Veuillez choisir un fichier excel'), false);
        }
        callback(null, true);
    }
});

/**
 * @constant {express.Router}
 */
const router: Router = Router();

/**
 * POST method route
 * @example http://localhost:PORT/v1/analyse/pesticide
 * 
 * @swagger
 * /v1/analyse/pesticide:
 *   post:
 *      description: Cet endpoint permet de créer une nouvelle Analyse pesticide
 *      tags: ["analysepesticide"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: Analysepesticides creation request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AnalysepesticidesSchema'
 *            example:
 *              date_prelevement: 2019-10-10
 *              heure_prelevement: 09:05
 *              date_analyse: 2019-10-10
 *              heure_analyse: 13:30
 *              element_chimique: {}
 *      responses:
 *        201:
 *          description: return created analyse pesticide
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/AnalysepesticidesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.post('/', AnalysepesticideComponent.create);

/**
 * PUT method route
 * @example http://localhost:PORT/v1/analyse/pesticide
 * 
 * @swagger
 * /v1/analyse/pesticide:
 *   post:
 *      description: Cet endpoint permet de mettre à jour une analyse pesticide existante
 *      tags: ["analysepesticide"]
 *      security:
 *       - ApiKeyAuth: []
 *      requestBody:
 *        description: analyse pesticide update request body
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/AnalysepesticidesSchema'
 *            example:
 *              date_prelevement: 2019-10-10
 *              heure_prelevement: 09:05
 *              date_analyse: 2019-10-10
 *              heure_analyse: 13:30
 *              element_chimique: {}
 *      responses:
 *        201:
 *          description: return created Analysepesticide
 *          content:
 *            application/json:
 *              schema:
 *                oneOf:
 *                  - $ref: '#/components/schemas/AnalysepesticidesSchema'
 *        default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.put('/', AnalysepesticideComponent.update);

/**
 * GET method route 
 * @example http://localhost:PORT/v1/analyse/pesticide/:id
 * 
 * @swagger
 * /v1/analyse/pesticide/{id}:
 *  get:
 *    description: Cet endpoint permet de récuper une analyse pesticide
 *    tags: ["analysepesticide"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: the analyse pesticide id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return Type station by id
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/AnalysepesticidesSchema'
 */
router.get('/:id', AnalysepesticideComponent.findOne);

/**
 * GET method route
 * @example http://localhost:PORT/v1/analyse/pesticide
 * 
 * @swagger
 * /v1/analyse/Analysepesticides:
 *   get:
 *     description: Cet endpoint permet de récuper la liste de toutes les analyses pesticides
 *     tags: ["analysepesticide"]
 *     security:
 *      - ApiKeyAuth: []
 *     responses:
 *       200:
 *         description: An array of analyse pesticides
 *         content:
 *           application/json:
 *             schema:
 *               oneOf:
 *                - $ref: '#/components/schemas/AnalysepesticidesSchema'
 *       default:
 *          description: unexpected error
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Error'
 */
router.get('/', AnalysepesticideComponent.findAll);

/**
 * DELETE method route 
 * @example http://localhost:PORT/v1/analyse/pesticide/:id
 * 
 * @swagger
 * /v1/analyse/pesticide/{id}:
 *  DELETE:
 *    description: Cet endpoint permet de supprimer une analyse pesticide
 *    tags: ["analysepesticide"]
 *    security:
 *      - ApiKeyAuth: []
 *    parameters:
 *      - in: path
 *        name: id
 *        description: The Analyse pesticide id
 *        required: true
 *        schema:
 *          type: string
 *    responses:
 *      200:
 *        description: return The deleted analyse pesticide
 *        content:
 *          application/json:
 *            schema:
 *              oneOf:
 *                - $ref: '#/components/schemas/AnalysepesticidesSchema'
 */
router.delete('/:id', AnalysepesticideComponent.remove);

/**
 * Uploader un fichier contenant une liste d'analyses pesticides à importer
 */
router.post('/upload', upload.single('file'), async (req, res, next) => {
    try {
        AnalysepesticideComponent.upload(req, res, next);
    } catch (err) {
        res.sendStatus(400);
    }
});

/**
 * Exporter la liste des analyses pesticides qui sont dans la BD
 */
router.post('/export', AnalysepesticideComponent.exportData);


/**
 * @export {express.Router}
 */
export default router;
